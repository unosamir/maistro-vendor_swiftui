//
//  SignupSuccessPopupView.swift
//  MaiStro-Vendor
//
//  Created by samir on 06/09/19.
//  Copyright © 2019 samir. All rights reserved.
//

import UIKit

class SuccessPopupView: LoadableView {
    
    //MARK: IBOutlets
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    @IBOutlet var lblText: UILabel!
    
    //MARK: Variables
    public var handler : (() -> Void)?
    
    //MARK: View Life cylce
    
    init(frame: CGRect,title:String,subTitle:String,textMsg:String) {
        
        super.init(frame: frame)
        
        lblTitle.text = title
        lblSubTitle.text = subTitle
        lblText.text = textMsg
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
   
    @IBAction func btnCloseAction(_ sender: UIButton) {
        if handler != nil {
            handler!()
        }
    }
    
}
