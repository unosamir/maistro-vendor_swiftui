//
//  NoDataFoundView.swift
//  MaiStro-Vendor
//
//  Created by samir on 29/08/19.
//  Copyright © 2019 samir. All rights reserved.
//

import UIKit

class NoDataFoundView: UIView {

    @IBOutlet var imgNoData: UIImageView!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var width: NSLayoutConstraint!
    @IBOutlet var height: NSLayoutConstraint!
    
    var message = ""
    var image : UIImage?
    
    override func awakeFromNib() {
        
    }
    
    override func layoutSubviews() {
        
        lblMessage.text = message
        imgNoData.image = image
        
        if image == nil{
            
            width.constant = 0
            height.constant = 0
        }
    }
}

