//
//  WebViewVC.swift
//  UNOApp OMA
//
//  Created by Ashish on 24/07/18.
//  Copyright © 2018 Umakant. All rights reserved.
//

import UIKit

class NotificationWebViewVC: UIViewController{
    
    //MARK: IBOutlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var webMessage: WKWebView!
    
    //MARK: Variable
    var link: String = ""
    var strTitle:String = ""
    var isfromPresent: Bool = false
    
    // MARK:- View life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webMessage.navigationDelegate = self
        
        if let url = URL(string: link.removeWhiteSpacesFromString()) {
            webMessage.load(URLRequest(url: url))
        }else{
            
            //CommonMethods.showAlertMessage("Link is not valid")
        }
        
        self.lblTitle.text = String(format: "%@", self.strTitle)
    }
    
    // MARK:- Back button Action
    
    @IBAction func btnBackPress(_ sender: UIButton) {
        //CommonMethods.hideLoader()
        if self.isfromPresent {
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//Webview Delegates
extension NotificationWebViewVC : WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
//        loader.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
//        loader.stopAnimating()
    }
}
