//
//  MessageProfileVC.swift
//  oma3.0
//
//  Created by Chirag Vegad on 11/06/18.
//  Copyright © 2018 Umakant. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import MessageUI
import MMPlayerView

class MessageProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate{
    
    //MARK: IBOutlet
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var tblMessageDetail: UITableView!
    
    //MARK: Variables
    var msgData : ClsInboxNotification!
    var cellCount = 2
    lazy var mmPlayerLayer: MMPlayerLayer = {
        let l = MMPlayerLayer()
        l.cacheType = .memory(count: 5)
        l.coverFitType = .fitToPlayerView
        l.videoGravity = AVLayerVideoGravity.resizeAspectFill
        l.replace(cover: CoverA.instantiateFromNib() as! UIView & MMPlayerCoverViewProtocol)
        return l
    }()
    
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        cellCount = msgData.notificationInfo.notificationData.media.source == "" ? 2 : 3
        //#CHANGE_REQUIRE
        NotificationManager.readSingleNotification(userID: "USER_ID", notificationID: "MESSAGE_UUID") { (success, error) in
        }
        self.title = "Message"
        self.setnameofButton()
        if let nav = self.navigationController {
            nav.isNavigationBarHidden = false
            if #available(iOS 13, *){
                nav.navigationBar.prefersLargeTitles = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let nav = self.navigationController {
            nav.isNavigationBarHidden = true
        }
        
        switch mmPlayerLayer.currentPlayStatus {
        case .playing:
            mmPlayerLayer.player?.pause()
        default:
            break
            // print("ERROR")
        }
        
    }
    
    //MARK: Private Methods
    // MARK:- Set Bottom Button Title
    func setnameofButton() {
        self.btnAction.isHidden = msgData.notificationInfo.type.lowercased() == "push"
        self.btnAction.setTitle(msgData.notificationInfo.notificationData.action.name, for: .normal)
    }
    
    // MARK:- Back button Action
    
    @IBAction func btnBackPress(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Event Button click
    
    @IBAction func btnCheckOutTapped(_ sender: UIButton) {
        
        if self.msgData.notificationInfo.notificationData.action.action == SDK_NOTIFICATION_TYPE_GOTO_LINK {
            
            if self.msgData.notificationInfo.notificationData.action.value != nil && self.msgData.notificationInfo.notificationData.action.value.count != 0 {
                
                if !self.msgData.notificationInfo.notificationData.action.value.hasPrefix("http") || !self.msgData.notificationInfo.notificationData.action.value.hasPrefix("https") {
                    
                    self.msgData.notificationInfo.notificationData.action.value = "https://" + self.msgData.notificationInfo.notificationData.action.value
                }
            }
            self.performSegue(withIdentifier: "loadWebview", sender: nil)
        }
            
        else if self.msgData.notificationInfo.notificationData.action.action == SDK_NOTIFICATION_TYPE_CALL {
            
            if self.msgData.notificationInfo.notificationData.action.value != nil && self.msgData.notificationInfo.notificationData.action.value.count != 0 {
                
                let phone = String( format: "tel://%@", getCleanNumber(phone:  self.msgData.notificationInfo.notificationData.action.value))
                let url: NSURL = NSURL(string: phone)!
                
                UIApplication.shared.openURL(url as URL)
            }
        }
            
        else if self.msgData.notificationInfo.notificationData.action.action == SDK_NOTIFICATION_TYPE_EMAIL {
            
            let emailBodyDX: [String: AnyObject] = UserDefaultsMethods.getCustomObject(key: "") as! [String: AnyObject]
            
            
            
            if emailBodyDX["app_action_emails"] != nil {
                
                let actionArray: [AnyObject] = emailBodyDX["app_action_emails"] as! [AnyObject]
                
                if actionArray.count > 0 {
                    
                    let emailBodyDX: [String: AnyObject] = actionArray[0] as! [String: AnyObject]
                    
                    let mailer = MFMailComposeViewController()
                    
                    mailer.mailComposeDelegate = self
                    mailer.setSubject(emailBodyDX["subject"] as! String)
                    mailer.setMessageBody(emailBodyDX["body"] as! String, isHTML: true)
                    mailer.setToRecipients([emailBodyDX["email"] as! String])
                    if MFMailComposeViewController.canSendMail() {
                        
                        present(mailer, animated: true)
                        { () -> Void in
                        }
                    }
                }
            }
        }
            
        else if self.msgData.notificationInfo.notificationData.action.action == SDK_NOTIFICATION_TYPE_RATE_US {
            
            let rateUs = UserDefaultsMethods.getString(key: krateusURL)
            
            if rateUs != nil && rateUs?.count != 0 {
                
                if let url = URL(string: rateUs!)
                {
                    UIApplication.shared.openURL(url)
                }
            }
        }
            
        else if self.msgData.notificationInfo.notificationData.action.action == SDK_NOTIFICATION_TYPE_CLOSE
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK:- Prepare for Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "loadWebview"
        {
            let webVC = segue.destination as! NotificationWebViewVC
            webVC.link = self.msgData.notificationInfo.notificationData.action.value
            webVC.isfromPresent = false
            webVC.strTitle = (self.btnAction.currentTitle)!
        }
    }
    
    // MARK:- Tableview Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageTitleCell", for: indexPath) as! MessageTitleCell
            cell.lblTitle.text = (self.msgData.notificationInfo.title)
            cell.lblDateAuth.text = ""// (CommonMethods.convertDateFormater(msgData.createdAt)).uppercased() // ("FROM CEO")  + "\n" +
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageTextCell", for: indexPath) as! MessageTextCell
            cell.lblText.text = (self.msgData.notificationInfo.body)
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row == 2{
            if(msgData.notificationInfo.notificationData.media.type == "image"){
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageImageCell", for: indexPath) as! MessageImageCell
                
                cell.spinner.isHidden = false
                cell.spinner.startAnimating()
                cell.spinner.hidesWhenStopped = true
                cell.imgMessage.contentMode = .scaleAspectFit
                
                cell.imgMessage.sd_setImage(with: URL(string: msgData.notificationInfo.notificationData.media.source), placeholderImage:nil, options: .refreshCached, progress: nil)
                { image, _, _, _ in
                    cell.spinner.stopAnimating()
                    cell.spinner.isHidden = true
                }
                cell.selectionStyle = .none
                return cell
            }
            else if(msgData.notificationInfo.notificationData.media.type == "video"){
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageImageCell", for: indexPath) as! MessageImageCell
                if let url = URL(string: msgData.notificationInfo.notificationData.media.source){
                    
                    mmPlayerLayer.playView = cell.imgMessage
                    
                    // set url prepare to load
//                    mmPlayerLayer.set(url: url, state: { (status) in
//                        switch status {
//                        case .failed(let err):
//                            let alert = UIAlertController(title: "err", message: err.description, preferredStyle: .alert)
//                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                            self.present(alert, animated: true, completion: nil)
//                        default: break
//                        }
//                    })
                    mmPlayerLayer.set(url: url)
                }
                mmPlayerLayer.autoPlay = false
                mmPlayerLayer.resume()
                cell.selectionStyle = .none
                return cell
            }
            else if(msgData.notificationInfo.notificationData.media.type == ""){
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageDocCell", for: indexPath) as! MessageDocCell
                self.addShadow(bgView: cell.vwFile)
                self.addShadow(bgView: cell.vwImage)
                cell.vwImage.isHidden = true
                cell.lblCounter.text = "1 ATTCHMENTS"
                cell.selectionStyle = .none
                return cell
            }
        }
        else if indexPath.row == 3{
            if(msgData.notificationInfo.notificationData.media.type != "" ){
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageDocCell", for: indexPath) as! MessageDocCell
                self.addShadow(bgView: cell.vwFile)
                self.addShadow(bgView: cell.vwImage)
                cell.lblCounter.text = "2 ATTCHMENTS"
                cell.selectionStyle = .none
                return cell
            }
            else if(msgData.notificationInfo.notificationData.media.type != ""){
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageImageCell", for: indexPath) as! MessageImageCell
                
                cell.spinner.isHidden = false
                cell.spinner.startAnimating()
                cell.spinner.hidesWhenStopped = true
                cell.imgMessage.contentMode = .scaleAspectFit
                
                cell.imgMessage.sd_setImage(with: URL(string: msgData.notificationInfo.notificationData.media.source), placeholderImage:  nil, options: .refreshCached, progress: nil)
                { image, _, _, _ in
                    cell.spinner.stopAnimating()
                    cell.spinner.isHidden = true
                }
                cell.selectionStyle = .none
                return cell
            }
            else if(msgData.notificationInfo.notificationData.media.type != ""){
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageDocCell", for: indexPath) as! MessageDocCell
                self.addShadow(bgView: cell.vwFile)
                self.addShadow(bgView: cell.vwImage)
                cell.vwImage.isHidden = true
                cell.lblCounter.text = "1 ATTCHMENTS"
                cell.selectionStyle = .none
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row < 2){
            return UITableView.automaticDimension
        }
        else if(indexPath.row == 2){
            if(msgData.notificationInfo.notificationData.media.source != ""){
                if(msgData.notificationInfo.notificationData.media.type == "video"){
                    return 209
                }else if(msgData.notificationInfo.notificationData.media.type == "image"){
                    return UIScreen.main.bounds.size.height * 0.4
                }
                else{
                    return UITableView.automaticDimension
                }
            }
            else{
                return 175
            }
        }
        else if(indexPath.row == 3){
            if(msgData.notificationInfo.notificationData.media.source != "")
            {
                return UITableView.automaticDimension
            }
            else{
                return 175
            }
        }
        return UITableView.automaticDimension
    }
    
    // MARK: - Mail Composer Delegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if error == nil
        {
            dismiss(animated: true) { () -> Void in }
        }
        else
        {
            showAlertMessage((error?.localizedDescription)!)
        }
    }
    
}
