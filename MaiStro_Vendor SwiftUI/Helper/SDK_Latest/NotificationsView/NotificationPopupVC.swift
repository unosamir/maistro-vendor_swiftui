//
//  NotificationPopupVC.swift
//  UNOApp OMA
//
//  Created by Ashish on 16/01/19.
//  Copyright © 2019 Umakant. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer
import MessageUI
import UIKit
import MMPlayerView

let SDK_NOTIFICATION_TYPE_CLOSE: String = "close"
let SDK_NOTIFICATION_TYPE_GOTO_LINK: String = "web_link"
let SDK_NOTIFICATION_TYPE_CAMERA: String = "camera"
let SDK_NOTIFICATION_TYPE_RATE_US: String = "rate_us"
let SDK_NOTIFICATION_TYPE_CALL: String = "call"
let SDK_NOTIFICATION_TYPE_EMAIL: String = "email"

let krateusURL = "rate_us"
//let kAlertTitle = "ICEAPP"
let kEmailBody = "email_body"


class NotificationPopupVC: UIViewController,MFMailComposeViewControllerDelegate {
    
    //MARK: IBOutlet
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var msgbodyHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwMedia: UIView!
    @IBOutlet weak var imgMessage: UIImageView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnAction: UIButton!
    
    //MARK: Variables
 //   var objNotification: ClsICENotificationPayload!
    var objNotification: ClsNotificationRootClass!
  //  var objNotificationData : Cls
    lazy var mmPlayerLayer: MMPlayerLayer = {
        let l = MMPlayerLayer()
        l.cacheType = .memory(count: 5)
        l.coverFitType = .fitToPlayerView
        l.videoGravity = AVLayerVideoGravity.resizeAspectFill
        l.replace(cover: CoverA.instantiateFromNib() as! UIView & MMPlayerCoverViewProtocol)
        return l
    }()
    
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        switch mmPlayerLayer.currentPlayStatus {
        case .playing:
            mmPlayerLayer.player?.pause()
        default:
            break
            // print("ERROR")
        }
        
    }
    
    //MARK: Private Methods
    func initialConfig(){
        
        var Title: String = kAppName
        
        if self.objNotification.aps.alert.title != "" {
            if self.objNotification.aps.alert.title.count != 0
            {
                Title = self.objNotification.aps.alert.title
            }
        }
        self.lblTitle.text = Title
        self.lblTitle.layoutSubviews()
        self.view.layoutIfNeeded()
        
        self.txtMessage.textContainer.lineFragmentPadding = 0
        self.txtMessage.textContainerInset = .zero
        
        if self.objNotification.aps.alert.body != "" {
            self.txtMessage.text = self.objNotification.aps.alert.body
        }
        if self.txtMessage.contentSize.height <= 100 {
            self.msgbodyHeight.constant = self.txtMessage.contentSize.height
        }else{
            self.msgbodyHeight.constant = 100
        }
        self.imgMessage.isHidden = true
        self.activityIndicator.isHidden = true
        if self.objNotification.media.type == "video" {
            if let url = URL(string: self.objNotification.media.source){
                mmPlayerLayer.playView = self.vwMedia
                
//                mmPlayerLayer.set(url: url, state: { (status) in
//                    switch status {
//                    case .failed(let err):
//                        let alert = UIAlertController(title: "err", message: err.description, preferredStyle: .alert)
//                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
//                    default: break
//                    }
//                })
                mmPlayerLayer.set(url: url)
            }
            mmPlayerLayer.autoPlay = true
            mmPlayerLayer.resume()
        }
        else if objNotification.media.type == "image" {
            
            self.imgHeight.constant = UIScreen.main.bounds.size.height * 0.4
            self.activityIndicator.startAnimating()
            self.activityIndicator.hidesWhenStopped = true
            self.activityIndicator.isHidden = false
            self.imgMessage.isHidden = false
            self.imgMessage.sd_setImage(with: URL(string: self.objNotification.media.source), placeholderImage: nil, options: .refreshCached, progress: nil)
            { image, _, _, _ in
                if image != nil
                {
                    let newimage: UIImage = Helper.imageWithImage(sourceImage: image!, scaledToWidth: self.view.frame.size.width-48)
                    if newimage.size.height < UIScreen.main.bounds.size.height * 0.4 {
                        self.imgHeight.constant = newimage.size.height
                    }
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                }
            }
        }
        self.setnameofButton()
    }
    
    // MARK:- Set Button Title
    func setnameofButton() {
//        let actionArray:[AnyObject] = UserDefaultsMethods.getMutableArray(key: kSDKActionArray)! as [AnyObject]
//        let namePredicate = NSPredicate(format: "action_type == %@",self.objNotification.action.action)
//        let arrfilter = (actionArray).filter { namePredicate.evaluate(with: $0) } as Array
//        if arrfilter.count > 0
//        {
//            self.btnAction.setTitle((arrfilter[0] as! NSDictionary).value(forKey: "title") as? String, for: .normal)
//        }
        self.btnAction.setTitle(self.objNotification.action.name, for: .normal)
    }
    
    // MARK:- Button Action
    @IBAction func btnCheckOutTapped(_ sender: UIButton) {
        
        if self.objNotification.action.action == SDK_NOTIFICATION_TYPE_GOTO_LINK {
            
            if self.objNotification.action.value.count != 0 {
                if !self.objNotification.action.value.hasPrefix("http") || !self.objNotification.action.value.hasPrefix("https") {
                    self.objNotification.action.value = "https://" + self.objNotification.action.value
                }
            }
//            let webView = self.storyboard?.instantiateViewController(withIdentifier: "WebViewVC") as? NotificationWebViewVC
//            webView?.link = self.objNotification.action.value
//            webView?.isfromPresent = true
//            webView?.strTitle = (self.btnAction.currentTitle)!
//            self.present(webView!, animated: false)
            
            if self.objNotification.action.value != nil && self.objNotification.action.value.count != 0 {
                if let url = URL(string: self.objNotification.action.value)
                {
                    UIApplication.shared.openURL(url)
                }
            }
            self.dismiss(animated: true, completion: nil)
            
        }
            
        else if self.objNotification.action.action == SDK_NOTIFICATION_TYPE_CALL {
            
            if self.objNotification.action.value != nil && self.objNotification.action.value.count != 0 {
                
                let phone = String( format: "tel://%@", getCleanNumber(phone:  self.objNotification.action.value))
                let url: NSURL = NSURL(string: phone)!
                
                UIApplication.shared.openURL(url as URL)
            }
            self.dismiss(animated: true, completion: nil)
        }
            
        else if self.objNotification.action.action == SDK_NOTIFICATION_TYPE_EMAIL {
            
          //  let emailBodyDX: [String: AnyObject] = UserDefaultsMethods.getCustomObject(key: kEmailBody) as! [String: AnyObject]
            
           // if emailBodyDX["app_action_emails"] != nil {
                
             //   let actionArray: [AnyObject] = emailBodyDX["app_action_emails"] as! [AnyObject]
                
               // if actionArray.count > 0 {
                    
                  //  let emailBodyDX: [String: AnyObject] = actionArray[0] as! [String: AnyObject]
                    
                    let mailer = MFMailComposeViewController()
                    
                    mailer.mailComposeDelegate = self
                  //  mailer.setSubject(emailBodyDX["subject"] as! String)
                  //  mailer.setMessageBody(emailBodyDX["body"] as! String, isHTML: true)
                    mailer.setToRecipients([self.objNotification.action.value])
                    if MFMailComposeViewController.canSendMail() {
                        
                        present(mailer, animated: true)
                        { () -> Void in
                        }
                    }
               // }
           // }
            
        }
            
        else if self.objNotification.action.action == SDK_NOTIFICATION_TYPE_RATE_US {
            
            let rateUs = "https://unoapp.com"//UserDefaultsMethods.getString(key: krateusURL)
            if rateUs != nil && rateUs.count != 0 {
                if let url = URL(string: rateUs)
                {
                    UIApplication.shared.openURL(url)
                }
            }
            self.dismiss(animated: true, completion: nil)
        }else if self.objNotification.action.action == SDK_NOTIFICATION_TYPE_CLOSE {
            
            self.dismiss(animated: true, completion: nil)
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK:- Dismiss Button Action
    
    @IBAction func dismissbuttonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Mail Composer Delegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
      //  if error == nil {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
           self.dismiss(animated: true) { () -> Void in }
        }
        
//        }
//        else {
//
//            showAlertMessage((error?.localizedDescription)!)
//        }
    }
    
}
