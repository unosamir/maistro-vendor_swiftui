//
//  InboxCell.swift
//  oma3.0
//
//  Created by Chirag Vegad on 11/06/18.
//  Copyright © 2018 Umakant. All rights reserved.
//

import UIKit

let GREENCOLOR = UIColor(red: 20.0 / 255.0, green: 185.0 / 255.0, blue: 214.0 / 255.0, alpha: 1.0)

class InboxCell: UICollectionViewCell,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblMessages: UITableView!
    
    var arrData : [ClsNotificationData]!
    var filter : Int!
    var filterText : String!
    
    var pageNumber = 1
    var totalPage = 1
    
    var shedowOpacity : Float = 0.2
//    var delegate : NavigationDelegate!
    var arrMainData : [[String:Any]]!
    
    var refreshControl: UIRefreshControl!
    
    // MARK:- Awake Nib
    
    override func awakeFromNib() {
        self.tblMessages.delegate = self
        self.tblMessages.dataSource = self
        
    }
    
    // MARK:- Tableview Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(arrData.count != 0){
            if(filter == 1){
                let unreadData = self.getFilterData(filter: 2, data: arrData) as [ClsNotificationData]
                if(unreadData.count != 0){
                    let readData = self.getFilterData(filter: 3, data: arrData) as [ClsNotificationData]
                    if(readData.count != 0) {
                        return 2
                    }
                    else{
                        return 1
                    }
                }
            }
            return 1
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(arrData.count != 0){
            if(filter == 1){
                let unreadData = self.getFilterData(filter: 2, data: arrData) as [ClsNotificationData]
                if(unreadData.count != 0){
                    let readData = self.getFilterData(filter: 3, data: arrData) as [ClsNotificationData]
                    if(readData.count != 0){
                        if(section == 0){
                            return unreadData.count
                        }
                        else if (section == 1){
                            let readData = self.getFilterData(filter: 3, data: arrData) as [ClsNotificationData]
                            return readData.count
                        }
                    }
                    else{
                        if(section == 0){
                            return unreadData.count
                        }
                    }
                }
                else{
                    return arrData.count
                }
            }
            return arrData.count
        }
        else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(self.filter == 1){
            let unreadData = self.getFilterData(filter: 2, data: arrData) as [ClsNotificationData]
            if(unreadData.count != 0){
                
                if(section == 0){
                    
                    let header : MsgHeaderCell = tableView.dequeueReusableCell(withIdentifier: "MsgHeaderCell") as! MsgHeaderCell
                    let modifierCount = NSMutableAttributedString(string:"NEW MESSAGES ")
                    let attributes = [NSAttributedString.Key.foregroundColor : GREENCOLOR]
                    let countString = NSMutableAttributedString(string:String(format:"(%d)" + "     ",unreadData.count), attributes:attributes)
                    modifierCount.append(countString)
                    header.lblHeaderTitle.attributedText = modifierCount
                    
                    return header
                }
            }
        }
        return UIView()
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if(self.filter == 1){
            let unreadData = self.getFilterData(filter: 2, data: arrData) as [ClsNotificationData]
            if(unreadData.count != 0){
                if(section == 0){
                    let header : MsgSapratorCell = tableView.dequeueReusableCell(withIdentifier: "MsgSapratorCell") as! MsgSapratorCell
                    return header
                }
            }
        }
        return UIView()
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationMessageCell", for: indexPath) as! NotificationMessageCell
        if(self.filter == 1){
            let unreadData = self.getFilterData(filter: 2, data: arrData) as [ClsNotificationData]
            if(unreadData.count != 0){
                let readData = self.getFilterData(filter: 3, data: arrData) as [ClsNotificationData]
                if(readData.count != 0){
                    if(indexPath.section == 0){
                        let msgData = unreadData[indexPath.row]
                        cell.lblMainTitle.text = (msgData.notificationRequest.notification.title)
                        cell.lblSubTitle.text = (msgData.notificationRequest.notification.body)
                        cell.lblUpperTitle.text = "" //FROM CEO"
                        cell.lblDateTime.text = convertDateFormater(msgData.createdAt).uppercased()
                        cell.selectionStyle = .none
                        self.getviewShedow(cell.vwBack, opacity: shedowOpacity)
                        
                    }
                    else if(indexPath.section == 1){
                        let readData = self.getFilterData(filter: 3, data: arrData) as [ClsNotificationData]
                        let msgData = readData[indexPath.row]
                        
                        cell.lblMainTitle.text = (msgData.notificationRequest.notification.title)
                        cell.lblSubTitle.text = (msgData.notificationRequest.notification.body)
                        cell.lblUpperTitle.text = "" //FROM CEO"
                        cell.lblDateTime.text = convertDateFormater(msgData.createdAt).uppercased()
                        cell.selectionStyle = .none
                        self.getviewShedow(cell.vwBack, opacity: shedowOpacity)
                        
                    }
                }
                else{
                    if(indexPath.section == 0){
                        let msgData = unreadData[indexPath.row]
                        
                        
                        cell.lblMainTitle.text = (msgData.notificationRequest.notification.title)
                        cell.lblSubTitle.text = (msgData.notificationRequest.notification.body)
                        cell.lblUpperTitle.text = "" //FROM CEO"
                        cell.lblDateTime.text = convertDateFormater(msgData.createdAt).uppercased()
                        cell.selectionStyle = .none
                        self.getviewShedow(cell.vwBack, opacity: shedowOpacity)
                        
                    }
                }
            }
            else{
                let msgData = arrData[indexPath.row]
                
                cell.lblMainTitle.text = (msgData.notificationRequest.notification.title)
                cell.lblSubTitle.text = (msgData.notificationRequest.notification.body)
                cell.lblUpperTitle.text = "" //FROM CEO"
                cell.lblDateTime.text = convertDateFormater(msgData.createdAt).uppercased()
                cell.selectionStyle = .none
                self.getviewShedow(cell.vwBack, opacity: shedowOpacity)
            }
        }
        else{
            let msgData = arrData[indexPath.row]
            cell.lblMainTitle.text = (msgData.notificationRequest.notification.title)
            cell.lblSubTitle.text = (msgData.notificationRequest.notification.body)
            cell.lblUpperTitle.text = "" //FROM CEO"
            cell.lblDateTime.text = convertDateFormater(msgData.createdAt).uppercased()
            cell.selectionStyle = .none
            self.getviewShedow(cell.vwBack, opacity: shedowOpacity)
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(self.filter == 1){
            if(section == 0){
                let unreadData = self.getFilterData(filter: 2, data: arrData) as [ClsNotificationData]
                if(unreadData.count != 0){
                    return 34
                }
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(self.filter == 1){
            if(section == 0){
                let unreadData = self.getFilterData(filter: 2, data: arrData) as [ClsNotificationData]
                if(unreadData.count != 0){
                    return 34
                }
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(self.filter == 1){
            let unreadData = self.getFilterData(filter: 2, data: arrData) as [ClsNotificationData]
            if(unreadData.count != 0){
                let readData = self.getFilterData(filter: 3, data: arrData) as [ClsNotificationData]
                if(readData.count != 0){
                    if(indexPath.section == 0){
                        let msgData = unreadData[indexPath.row]
                    }
                    else if(indexPath.section == 1){
                        
                        let msgData = readData[indexPath.row]
                    }
                }
                else{
                    let msgData = unreadData[indexPath.row]
                }
            }
            else{
                let msgData = arrData[indexPath.row]
            }
        }
        else{
            let msgData = arrData[indexPath.row]
        }
        
    }
    
    // MARK:- Add Shadow to Tableview Cell
    
    func getviewShedow(_ view: UIView, opacity: Float) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOpacity = opacity
        view.layer.shadowRadius = 1
        view.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
    }
    
    // MARK:- Filter Data for Message type
    
    func getFilterData(filter: Int,data:[ClsNotificationData]) -> [ClsNotificationData]
    {
        var arrMsg = [ClsNotificationData]()
        if(filter == 1) {
            arrMsg = data
        }
        else if (filter == 2){
            arrMsg = data.filter({$0.readAt == ""})
        }
        else if (filter == 3){
            arrMsg = data.filter({$0.readAt != ""})
        }
        return arrMsg.count == 0 ? [ClsNotificationData]() : arrMsg
    }
    
    // MARK: - Other Functions
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        
        dateFormatter.setLocale()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.calendar = NSCalendar.current
        
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMMM d yyyy, h:mm a"
        return  dateFormatter.string(from: date!)
        
    }
}


