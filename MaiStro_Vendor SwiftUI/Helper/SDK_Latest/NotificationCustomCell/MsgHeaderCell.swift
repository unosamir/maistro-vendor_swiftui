//
//  HeaderCell.swift
//  oma3.0
//
//  Created by Chirag Vegad on 11/06/18.
//  Copyright © 2018 Umakant. All rights reserved.
//

import UIKit

class MsgHeaderCell: UITableViewCell {

    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
