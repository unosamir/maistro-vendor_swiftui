//
//  MessageImageCell.swift
//  oma3.0
//
//  Created by Chirag Vegad on 12/06/18.
//  Copyright © 2018 Umakant. All rights reserved.
//

import UIKit
import SDWebImage

class MessageImageCell: UITableViewCell {

    @IBOutlet weak var imgMessage: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
