//
//  MessageVideoCell.swift
//  oma3.0
//
//  Created by Chirag Vegad on 12/06/18.
//  Copyright © 2018 Umakant. All rights reserved.
//

import UIKit

class MessageDocCell: UITableViewCell {

    @IBOutlet weak var vwFile: UIView!
    @IBOutlet weak var vwImage: UIView!
    
    @IBOutlet weak var lblFile: UILabel!
    @IBOutlet weak var lblImage: UILabel!
    
    @IBOutlet weak var lblCounter: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
