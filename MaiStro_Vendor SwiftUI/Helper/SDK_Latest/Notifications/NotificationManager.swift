//
//  NotificationManager.swift
//  RestManager
//
//  Created by Meghdoot on 23/08/19.
//  Copyright © 2019 Appcoda. All rights reserved.
//

import UIKit
import Gloss

 let ERROR_PARSE_PROBLEM : String = "420: Unfortunately, we were not able to process your request. Please try again later."




class NotificationManager: NSObject {
    static let rest = RestManager()
    static func createUser(params: [String: String] = [:], completionHandler: @escaping (Bool,CustomError?) -> Void) {
        guard let url = URL(string: Settings.api.createUser) else { return }
        
        rest.requestHttpHeaders.add(value: "application/json", forKey: "Content-Type")
        rest.requestHttpHeaders.add(value: Settings.token, forKey: "api-token")
        
        rest.httpBodyParameters.setValues(params: params)
        
        rest.makeRequest(toURL: url, withHttpMethod: .POST) { (results) in
            guard let response = results.response else { return }
            if (200...299).contains(response.httpStatusCode) {
                guard let data = results.data, let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else { return }
                return completionHandler(true,nil)
            }else{
                let error = CustomError(title: results.error?.localizedDescription, description: ERROR_NOT_EXPECTED, code: 500)
                completionHandler(false,error)
            }
        }
    }
    
    static func updateUser(userID userid : String,params: [String: String] = [:], completionHandler: @escaping (Bool,CustomError?) -> Void) {
        let strURL = String(format: Settings.api.updateUser, userid)
        guard let url = URL(string: strURL) else { return }
        
        rest.requestHttpHeaders.add(value: "application/x-www-form-urlencoded", forKey: "Content-Type")
        rest.requestHttpHeaders.add(value: Settings.token, forKey: "api-token")
        
        rest.httpBodyParameters.setValues(params: params)
        
        rest.makeRequest(toURL: url, withHttpMethod: .PATCH) { (results) in
            guard let response = results.response else { return }
            if (200...299).contains(response.httpStatusCode) {
                return completionHandler(true,nil)
            }else{
                let error = CustomError(title: results.error?.localizedDescription, description: ERROR_NOT_EXPECTED, code: 500)
                completionHandler(false,error)
            }
        }
    }
    
    static func getInboxNotification(userID userid : String, page : Int ,params: [String: String] = [:], completionHandler: @escaping (ClsInboxMsg,CustomError?) -> Void) {
        let strURL = String(format: Settings.api.getInboxNotification, userid, page)
        guard let url = URL(string: strURL) else { return }
        
        rest.requestHttpHeaders.add(value: "application/x-www-form-urlencoded", forKey: "Content-Type")
        rest.requestHttpHeaders.add(value: Settings.token, forKey: "api-token")
        
        rest.httpBodyParameters.setValues(params: params)
        
        rest.makeRequest(toURL: url, withHttpMethod: .GET) { (results) in
            guard let response = results.response else { return }
            if (200...299).contains(response.httpStatusCode) {
                guard let data = results.data, let json = ((try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]) as [String : AnyObject]??) else { return }
                if let objJson = json, let objNotification : ClsInboxMsg = "payload" <~~ objJson  {
                    completionHandler(objNotification,nil)
                }else{
                    let error = CustomError(title: ERROR_PARSE_PROBLEM, description: ERROR_NOT_EXPECTED, code: 420)
                    completionHandler(ClsInboxMsg(),error)
                }
            }else{
                let error = CustomError(title: results.error?.localizedDescription, description: ERROR_NOT_EXPECTED, code: 500)
                completionHandler(ClsInboxMsg(),error)
            }
        }
    }
    
    static func getUnreadCount(userID userid : String,params: [String: String] = [:], completionHandler: @escaping (Int,CustomError?) -> Void) {
        let strURL = String(format: Settings.api.getUnreadCount, userid) + "?type=push&is_promotional=false"
        guard let url = URL(string: strURL) else { return }
        
        rest.requestHttpHeaders.add(value: "application/x-www-form-urlencoded", forKey: "Content-Type")
        rest.requestHttpHeaders.add(value: Settings.token, forKey: "api-token")
        
        rest.httpBodyParameters.setValues(params: params)
        
        rest.makeRequest(toURL: url, withHttpMethod: .GET) { (results) in
            guard let response = results.response else { return }
            if (200...299).contains(response.httpStatusCode) {
                guard let data = results.data, let json = ((try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]) as [String : AnyObject]??) else { return }
                if let objJson = json, let objNotification : ClsUnreadCount = "payload" <~~ objJson  {
                    NotificationCenter.default.post(name: Notification.Name.UnreadCount, object: ObjectNotify(count: objNotification.unread))
                    completionHandler(objNotification.unread,nil)
                }else{
                    let error = CustomError(title: ERROR_PARSE_PROBLEM, description: ERROR_NOT_EXPECTED, code: 420)
                    completionHandler(0,error)
                }
            }else{
                let error = CustomError(title: results.error?.localizedDescription, description: ERROR_NOT_EXPECTED, code: 500)
                completionHandler(0,error)
            }
        }
    }
    
    static func readSingleNotification(userID userid : String, notificationID notificationid : String, params: [String: String] = [:], completionHandler: @escaping (Bool,CustomError?) -> Void) {
        let strURL = String(format: Settings.api.readSingleNotification, userid, notificationid)
        guard let url = URL(string: strURL) else { return }
        
        rest.requestHttpHeaders.add(value: "application/json", forKey: "Content-Type")
        rest.requestHttpHeaders.add(value: Settings.token, forKey: "api-token")
        
        rest.httpBodyParameters.setValues(params: ["read": "true"])
        
        rest.makeRequest(toURL: url, withHttpMethod: .PATCH) { (results) in
            guard let response = results.response else { return }
            if (200...299).contains(response.httpStatusCode) {
                self.getUnreadCount(userID: userid, completionHandler: { (_, _) in }) // to refresh count
                return completionHandler(true,nil)
            }else{
                let error = CustomError(title: results.error?.localizedDescription, description: ERROR_NOT_EXPECTED, code: 500)
                completionHandler(false,error)
            }
        }
    }
    
    static func readMultipleNotification(userID userid : String, completionHandler: @escaping (Bool,CustomError?) -> Void) {
        let strURL = String(format: Settings.api.readMultipleNotification, userid)
        guard let url = URL(string: strURL) else { return }
        
        rest.requestHttpHeaders.add(value: "application/json", forKey: "Content-Type")
        rest.requestHttpHeaders.add(value: Settings.token, forKey: "api-token")
        
        rest.httpBodyParameters.setValues(params: ["read": "true"])
        
        rest.makeRequest(toURL: url, withHttpMethod: .PATCH) { (results) in
            guard let response = results.response else { return }
            if (200...299).contains(response.httpStatusCode) {
                return completionHandler(true,nil)
            }else{
                let error = CustomError(title: results.error?.localizedDescription, description: ERROR_NOT_EXPECTED, code: 500)
                completionHandler(false,error)
            }
        }
    }
    
    
    static func deleteNotification(userID userid : String, notificationID notificationid : String, params: [String: String] = [:], completionHandler: ((Bool,CustomError?) -> Void)? = nil) {
        let strURL = String(format: Settings.api.deleteNotification, userid, notificationid)
        guard let url = URL(string: strURL) else { return }
        
        rest.requestHttpHeaders.add(value: "application/json", forKey: "Content-Type")
        rest.requestHttpHeaders.add(value: Settings.token, forKey: "api-token")
        
        rest.httpBodyParameters.setValues(params: ["read": "true"])
        
        rest.makeRequest(toURL: url, withHttpMethod: .DELETE) { (results) in
            guard let response = results.response else {
                completionHandler?(false,nil)
                return
            }
            if (200...299).contains(response.httpStatusCode) {
                guard let data = results.data, let json = ((try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]) as [String : AnyObject]??) else { return }
                self.getUnreadCount(userID: userid, completionHandler: { (_, _) in })
                completionHandler?(true,nil)
            }else{
                guard let data = results.data, let json = ((try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]) as [String : AnyObject]??) else { return }
                print(json)
                 let error = CustomError(title: results.error?.localizedDescription, description: ERROR_NOT_EXPECTED, code: 500)
                completionHandler?(false,error)
            }
        }
    }
    
    static func setNotificationStatus(isEnableNotification: Bool,isEnablePromotionalNotification: Bool ,userID : String, params: [String: String] = [:], completionHandler: @escaping (Bool,CustomError?) -> Void) {
        let strURL = String(format: Settings.api.setNotificationStatus, userID , self.getUUID())
        guard let url = URL(string: strURL) else { return }
        
        rest.requestHttpHeaders.add(value: "application/json", forKey: "Content-Type")
        rest.requestHttpHeaders.add(value: Settings.token, forKey: "api-token")
        
        rest.httpBodyParameters.setValues(params: ["notification_enabled": isEnableNotification ? "true" : "false","promotional_enabled": isEnablePromotionalNotification ? "true" : "false"])
        
        rest.makeRequest(toURL: url, withHttpMethod: .PATCH) { (results) in
            guard let response = results.response else { return }
            if (200...299).contains(response.httpStatusCode) {
                return completionHandler(true,nil)
            }else{
                let error = CustomError(title: results.error?.localizedDescription, description: ERROR_NOT_EXPECTED, code: 500)
                completionHandler(false,error)
            }
        }
    }
    
    static func setDeviceStatus(isLoggedIn: Bool ,userID : String, deviceID : String, params: [String: String] = [:], completionHandler: @escaping (Bool,CustomError?) -> Void) {
        let strURL = String(format: Settings.api.setDeviceStatus, userID , deviceID)
        guard let url = URL(string: strURL) else { return }
        
        rest.requestHttpHeaders.add(value: "application/json", forKey: "Content-Type")
        rest.requestHttpHeaders.add(value: Settings.token, forKey: "api-token")
        
        rest.httpBodyParameters.setValues(params: ["active": isLoggedIn ? "true" : "false"])
        
        rest.makeRequest(toURL: url, withHttpMethod: .PATCH) { (results) in
            guard let response = results.response else { return }
            if (200...299).contains(response.httpStatusCode) {
                return completionHandler(true,nil)
            }else{
                let error = CustomError(title: results.error?.localizedDescription, description: ERROR_NOT_EXPECTED, code: 500)
                completionHandler(false,error)
            }
        }
    }
    
   
    static func getDeviceStatus(userID userid : String, completionHandler: @escaping (ClsDeviceStatusPayload,CustomError?) -> Void)
    {
        let strURL = String(format: Settings.api.getDeviceStatus, userid,self.getUUID())
        guard let url = URL(string: strURL) else { return }
        
        rest.requestHttpHeaders.add(value: "application/x-www-form-urlencoded", forKey: "Content-Type")
        rest.requestHttpHeaders.add(value: Settings.token, forKey: "api-token")
        
        rest.httpBodyParameters.setValues(params: [:])
        
        rest.makeRequest(toURL: url, withHttpMethod: .GET) { (results) in
            guard let response = results.response else { return }
            if (200...299).contains(response.httpStatusCode) {
                guard let data = results.data, let json = ((try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]) as [String : AnyObject]??) else { return }
                if let objJson = json, let objNotification : ClsDeviceStatusPayload = "payload" <~~ objJson  {
                    completionHandler(objNotification,nil)
                }else{
                    let error = CustomError(title: ERROR_PARSE_PROBLEM, description: ERROR_NOT_EXPECTED, code: 420)
                    completionHandler(ClsDeviceStatusPayload(),error)
                }
            }else{
                let error = CustomError(title: results.error?.localizedDescription, description: ERROR_NOT_EXPECTED, code: 500)
                completionHandler(ClsDeviceStatusPayload(),error)
            }
        }
    }

    static func getUUID() -> String{
        let keychain = KeychainSwift()
        if let uuid = keychain.get(Settings.token) {
            return uuid
        }else{
            if let identifier =  UIDevice.current.identifierForVendor {
                keychain.set(identifier.uuidString, forKey: Settings.token)
                return identifier.uuidString
            }
        }
        return ""
    }
    
}
