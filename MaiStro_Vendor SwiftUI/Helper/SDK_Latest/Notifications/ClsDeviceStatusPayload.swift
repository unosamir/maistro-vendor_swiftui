//
//  ClsDeviceStatusPayload.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 4, 2019

import Foundation 
import Gloss

//MARK: - ClsDeviceStatusPayload
public class ClsDeviceStatusPayload: Glossy {
    public var active : Bool!
    public var applicationId : Int!
    public var createdAt : String!
    public var deletedAt : AnyObject!
    public var deviceId : String!
    public var id : Int!
    public var notificationEnabled : Bool!
    public var notificationToken : String!
    public var platform : String!
    public var platformVersion : String!
    public var promotionalEnabled : Bool!
    public var updatedAt : String!
    public var userId : Int!

	//MARK: Default Initializer 
	init()
	{
        active = false
        applicationId = 0
        createdAt = ""
        deletedAt = nil
                deviceId = ""
        id = 0
        notificationEnabled = false
        notificationToken = ""
        platform = ""
        platformVersion = ""
        promotionalEnabled = false
        updatedAt = ""
        userId = 0
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let active : Bool = "active" <~~ json {
            self.active = active
        }else{
            self.active = false
        }
        if let applicationId : Int = "application_id" <~~ json {
            self.applicationId = applicationId
        }else{
            self.applicationId = 0
        }
        if let createdAt : String = "created_at" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let deletedAt : AnyObject = "deleted_at" <~~ json {
            self.deletedAt = deletedAt
        }else{
        }
        if let deviceId : String = "device_id" <~~ json {
            self.deviceId = deviceId
        }else{
            self.deviceId = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let notificationEnabled : Bool = "notification_enabled" <~~ json {
            self.notificationEnabled = notificationEnabled
        }else{
            self.notificationEnabled = false
        }
        if let notificationToken : String = "notification_token" <~~ json {
            self.notificationToken = notificationToken
        }else{
            self.notificationToken = ""
        }
        if let platform : String = "platform" <~~ json {
            self.platform = platform
        }else{
            self.platform = ""
        }
        if let platformVersion : String = "platform_version" <~~ json {
            self.platformVersion = platformVersion
        }else{
            self.platformVersion = ""
        }
        if let promotionalEnabled : Bool = "promotional_enabled" <~~ json {
            self.promotionalEnabled = promotionalEnabled
        }else{
            self.promotionalEnabled = false
        }
        if let updatedAt : String = "updated_at" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        if let userId : Int = "user_id" <~~ json {
            self.userId = userId
        }else{
            self.userId = 0
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "active" ~~> active,
        "application_id" ~~> applicationId,
        "created_at" ~~> createdAt,
        "deleted_at" ~~> deletedAt,
        "device_id" ~~> deviceId,
        "id" ~~> id,
        "notification_enabled" ~~> notificationEnabled,
        "notification_token" ~~> notificationToken,
        "platform" ~~> platform,
        "platform_version" ~~> platformVersion,
        "promotional_enabled" ~~> promotionalEnabled,
        "updated_at" ~~> updatedAt,
        "user_id" ~~> userId,
		])
	}

}
