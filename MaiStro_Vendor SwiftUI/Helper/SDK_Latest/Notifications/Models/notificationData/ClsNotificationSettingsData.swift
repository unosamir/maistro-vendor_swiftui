//
//	ClsNotificationSettingsData.swift
//
//	Create by Apple on 18/1/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ClsNotificationSettingsData : NSObject, NSCoding{

	var actionValue : String!
	var callToAction : String!
	var companyId : String!
	var mediaId : String!
	var mediaPath : String!
	var message : String!
	var mimeType : String!
	var payload : Payload!
	var pushMessage : String!
	var sendWhen : String!
	var title : String!
	var userId : String!
	var videoFrom : String!
	var zActionidFk : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		actionValue = dictionary["action_value"] as? String
		callToAction = dictionary["call_to_action"] as? String
		companyId = dictionary["companyId"] as? String
		mediaId = dictionary["mediaId"] as? String
		mediaPath = dictionary["mediaPath"] as? String
		message = dictionary["message"] as? String
		mimeType = dictionary["mime_type"] as? String
		if let payloadData = dictionary["payload"] as? NSDictionary{
			payload = Payload(fromDictionary: payloadData)
		}
		pushMessage = dictionary["push_message"] as? String
		sendWhen = dictionary["send_when"] as? String
		title = dictionary["title"] as? String
		userId = dictionary["userId"] as? String
		videoFrom = dictionary["video_from"] as? String
		zActionidFk = dictionary["z_actionid_fk"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if actionValue != nil{
			dictionary["action_value"] = actionValue
		}
		if callToAction != nil{
			dictionary["call_to_action"] = callToAction
		}
		if companyId != nil{
			dictionary["companyId"] = companyId
		}
		if mediaId != nil{
			dictionary["mediaId"] = mediaId
		}
		if mediaPath != nil{
			dictionary["mediaPath"] = mediaPath
		}
		if message != nil{
			dictionary["message"] = message
		}
		if mimeType != nil{
			dictionary["mime_type"] = mimeType
		}
		if payload != nil{
			dictionary["payload"] = payload.toDictionary()
		}
		if pushMessage != nil{
			dictionary["push_message"] = pushMessage
		}
		if sendWhen != nil{
			dictionary["send_when"] = sendWhen
		}
		if title != nil{
			dictionary["title"] = title
		}
		if userId != nil{
			dictionary["userId"] = userId
		}
		if videoFrom != nil{
			dictionary["video_from"] = videoFrom
		}
		if zActionidFk != nil{
			dictionary["z_actionid_fk"] = zActionidFk
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         actionValue = aDecoder.decodeObject(forKey: "action_value") as? String
         callToAction = aDecoder.decodeObject(forKey: "call_to_action") as? String
         companyId = aDecoder.decodeObject(forKey: "companyId") as? String
         mediaId = aDecoder.decodeObject(forKey: "mediaId") as? String
         mediaPath = aDecoder.decodeObject(forKey: "mediaPath") as? String
         message = aDecoder.decodeObject(forKey: "message") as? String
         mimeType = aDecoder.decodeObject(forKey: "mime_type") as? String
         payload = aDecoder.decodeObject(forKey: "payload") as? Payload
         pushMessage = aDecoder.decodeObject(forKey: "push_message") as? String
         sendWhen = aDecoder.decodeObject(forKey: "send_when") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         userId = aDecoder.decodeObject(forKey: "userId") as? String
         videoFrom = aDecoder.decodeObject(forKey: "video_from") as? String
         zActionidFk = aDecoder.decodeObject(forKey: "z_actionid_fk") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if actionValue != nil{
			aCoder.encode(actionValue, forKey: "action_value")
		}
		if callToAction != nil{
			aCoder.encode(callToAction, forKey: "call_to_action")
		}
		if companyId != nil{
			aCoder.encode(companyId, forKey: "companyId")
		}
		if mediaId != nil{
			aCoder.encode(mediaId, forKey: "mediaId")
		}
		if mediaPath != nil{
			aCoder.encode(mediaPath, forKey: "mediaPath")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if mimeType != nil{
			aCoder.encode(mimeType, forKey: "mime_type")
		}
		if payload != nil{
			aCoder.encode(payload, forKey: "payload")
		}
		if pushMessage != nil{
			aCoder.encode(pushMessage, forKey: "push_message")
		}
		if sendWhen != nil{
			aCoder.encode(sendWhen, forKey: "send_when")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "userId")
		}
		if videoFrom != nil{
			aCoder.encode(videoFrom, forKey: "video_from")
		}
		if zActionidFk != nil{
			aCoder.encode(zActionidFk, forKey: "z_actionid_fk")
		}

	}

}
