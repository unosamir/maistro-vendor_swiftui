//
//	SDKNotification.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SDKNotification : NSObject, NSCoding{

	var badge : Int!
	var body : String!
	var icon : String!
	var sound : String!
	var title : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		badge = dictionary["badge"] as? Int
		body = dictionary["body"] as? String
		icon = dictionary["icon"] as? String
		sound = dictionary["sound"] as? String
		title = dictionary["title"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if badge != nil{
			dictionary["badge"] = badge
		}
		if body != nil{
			dictionary["body"] = body
		}
		if icon != nil{
			dictionary["icon"] = icon
		}
		if sound != nil{
			dictionary["sound"] = sound
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         badge = aDecoder.decodeObject(forKey: "badge") as? Int
         body = aDecoder.decodeObject(forKey: "body") as? String
         icon = aDecoder.decodeObject(forKey: "icon") as? String
         sound = aDecoder.decodeObject(forKey: "sound") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if badge != nil{
			aCoder.encode(badge, forKey: "badge")
		}
		if body != nil{
			aCoder.encode(body, forKey: "body")
		}
		if icon != nil{
			aCoder.encode(icon, forKey: "icon")
		}
		if sound != nil{
			aCoder.encode(sound, forKey: "sound")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}