//
//	ClsNotificationData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ClsNotificationData : NSObject, NSCoding{

	var actionType : String!
	var actionValue : String!
	var appActionId : Int!
	var createdAt : String!
	var mediaId : Int!
	var mediaPath : String!
	var mimeType : String!
	var notificationId : Int!
	var notificationMetaId : Int!
	var notificationRequest : SDKNotificationData!
	var notificationType : String!
	var pushToken : String!
	var readAt : String!
	var userDeviceId : Int!
	var userId : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		actionType = dictionary["action_type"] as? String
		actionValue = dictionary["action_value"] as? String
		appActionId = dictionary["app_action_id"] as? Int
		createdAt = dictionary["created_at"] as? String
		mediaId = dictionary["media_id"] as? Int
		mediaPath = dictionary["media_path"] as? String
		mimeType = dictionary["mime_type"] as? String
		notificationId = dictionary["notification_id"] as? Int
		notificationMetaId = dictionary["notification_meta_id"] as? Int
        if let notificationRequestData = Helper.convertToDictionary(text: dictionary["notification_request"] as! String){
            notificationRequest = SDKNotificationData(fromDictionary: notificationRequestData)
        }
		notificationType = dictionary["notification_type"] as? String
		pushToken = dictionary["push_token"] as? String
		readAt = dictionary["read_at"] as? String
		userDeviceId = dictionary["user_device_id"] as? Int
		userId = dictionary["user_id"] as? Int
	}
    
	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if actionType != nil{
			dictionary["action_type"] = actionType
		}
		if actionValue != nil{
			dictionary["action_value"] = actionValue
		}
		if appActionId != nil{
			dictionary["app_action_id"] = appActionId
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if mediaId != nil{
			dictionary["media_id"] = mediaId
		}
		if mediaPath != nil{
			dictionary["media_path"] = mediaPath
		}
		if mimeType != nil{
			dictionary["mime_type"] = mimeType
		}
		if notificationId != nil{
			dictionary["notification_id"] = notificationId
		}
		if notificationMetaId != nil{
			dictionary["notification_meta_id"] = notificationMetaId
		}
		if notificationRequest != nil{
			dictionary["notification_request"] = notificationRequest
		}
		if notificationType != nil{
			dictionary["notification_type"] = notificationType
		}
		if pushToken != nil{
			dictionary["push_token"] = pushToken
		}
		if readAt != nil{
			dictionary["read_at"] = readAt
		}
		if userDeviceId != nil{
			dictionary["user_device_id"] = userDeviceId
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         actionType = aDecoder.decodeObject(forKey: "action_type") as? String
         actionValue = aDecoder.decodeObject(forKey: "action_value") as? String
         appActionId = aDecoder.decodeObject(forKey: "app_action_id") as? Int
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         mediaId = aDecoder.decodeObject(forKey: "media_id") as? Int
         mediaPath = aDecoder.decodeObject(forKey: "media_path") as? String
         mimeType = aDecoder.decodeObject(forKey: "mime_type") as? String
         notificationId = aDecoder.decodeObject(forKey: "notification_id") as? Int
         notificationMetaId = aDecoder.decodeObject(forKey: "notification_meta_id") as? Int
         notificationRequest = aDecoder.decodeObject(forKey: "notification_request") as? SDKNotificationData
         notificationType = aDecoder.decodeObject(forKey: "notification_type") as? String
         pushToken = aDecoder.decodeObject(forKey: "push_token") as? String
         readAt = aDecoder.decodeObject(forKey: "read_at") as? String
         userDeviceId = aDecoder.decodeObject(forKey: "user_device_id") as? Int
         userId = aDecoder.decodeObject(forKey: "user_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if actionType != nil{
			aCoder.encode(actionType, forKey: "action_type")
		}
		if actionValue != nil{
			aCoder.encode(actionValue, forKey: "action_value")
		}
		if appActionId != nil{
			aCoder.encode(appActionId, forKey: "app_action_id")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if mediaId != nil{
			aCoder.encode(mediaId, forKey: "media_id")
		}
		if mediaPath != nil{
			aCoder.encode(mediaPath, forKey: "media_path")
		}
		if mimeType != nil{
			aCoder.encode(mimeType, forKey: "mime_type")
		}
		if notificationId != nil{
			aCoder.encode(notificationId, forKey: "notification_id")
		}
		if notificationMetaId != nil{
			aCoder.encode(notificationMetaId, forKey: "notification_meta_id")
		}
		if notificationRequest != nil{
			aCoder.encode(notificationRequest, forKey: "notification_request")
		}
		if notificationType != nil{
			aCoder.encode(notificationType, forKey: "notification_type")
		}
		if pushToken != nil{
			aCoder.encode(pushToken, forKey: "push_token")
		}
		if readAt != nil{
			aCoder.encode(readAt, forKey: "read_at")
		}
		if userDeviceId != nil{
			aCoder.encode(userDeviceId, forKey: "user_device_id")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}
