//
//	SDKNotificationData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SDKNotificationData : NSObject, NSCoding{

	var data : SDKData!
	var notification : SDKNotification!
	var priority : String!
	var registrationIds : [String]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		if let dataData = dictionary["data"] as? [String:Any]{
			data = SDKData(fromDictionary: dataData)
		}
		if let notificationData = dictionary["notification"] as? [String:Any]{
			notification = SDKNotification(fromDictionary: notificationData)
		}
		priority = dictionary["priority"] as? String
		registrationIds = dictionary["registration_ids"] as? [String]
	}
    
	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if data != nil{
			dictionary["data"] = data.toDictionary()
		}
		if notification != nil{
			dictionary["notification"] = notification.toDictionary()
		}
		if priority != nil{
			dictionary["priority"] = priority
		}
		if registrationIds != nil{
			dictionary["registration_ids"] = registrationIds
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         data = aDecoder.decodeObject(forKey: "data") as? SDKData
         notification = aDecoder.decodeObject(forKey: "notification") as? SDKNotification
         priority = aDecoder.decodeObject(forKey: "priority") as? String
         registrationIds = aDecoder.decodeObject(forKey: "registration_ids") as? [String]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if notification != nil{
			aCoder.encode(notification, forKey: "notification")
		}
		if priority != nil{
			aCoder.encode(priority, forKey: "priority")
		}
		if registrationIds != nil{
			aCoder.encode(registrationIds, forKey: "registration_ids")
		}

	}

}
