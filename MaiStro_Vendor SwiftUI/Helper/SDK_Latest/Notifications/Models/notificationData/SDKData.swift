//
//	SDKData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SDKData : NSObject, NSCoding{

	var badge : Int!
	var eventId : Int!
	var isSdkNotification : String!
	var payload : SDKPayload!
	var pushId : String!
	var type : String!
	var userId : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		badge = dictionary["badge"] as? Int
		eventId = dictionary["eventId"] as? Int
		isSdkNotification = dictionary["isSdkNotification"] as? String
		if let payloadData = dictionary["payload"] as? [String:Any]{
			payload = SDKPayload(fromDictionary: payloadData)
		}
		pushId = dictionary["pushId"] as? String
		type = dictionary["type"] as? String
		userId = dictionary["userId"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if badge != nil{
			dictionary["badge"] = badge
		}
		if eventId != nil{
			dictionary["eventId"] = eventId
		}
		if isSdkNotification != nil{
			dictionary["isSdkNotification"] = isSdkNotification
		}
		if payload != nil{
			dictionary["payload"] = payload.toDictionary()
		}
		if pushId != nil{
			dictionary["pushId"] = pushId
		}
		if type != nil{
			dictionary["type"] = type
		}
		if userId != nil{
			dictionary["userId"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         badge = aDecoder.decodeObject(forKey: "badge") as? Int
         eventId = aDecoder.decodeObject(forKey: "eventId") as? Int
         isSdkNotification = aDecoder.decodeObject(forKey: "isSdkNotification") as? String
         payload = aDecoder.decodeObject(forKey: "payload") as? SDKPayload
         pushId = aDecoder.decodeObject(forKey: "pushId") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String
         userId = aDecoder.decodeObject(forKey: "userId") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if badge != nil{
			aCoder.encode(badge, forKey: "badge")
		}
		if eventId != nil{
			aCoder.encode(eventId, forKey: "eventId")
		}
		if isSdkNotification != nil{
			aCoder.encode(isSdkNotification, forKey: "isSdkNotification")
		}
		if payload != nil{
			aCoder.encode(payload, forKey: "payload")
		}
		if pushId != nil{
			aCoder.encode(pushId, forKey: "pushId")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "userId")
		}

	}

}