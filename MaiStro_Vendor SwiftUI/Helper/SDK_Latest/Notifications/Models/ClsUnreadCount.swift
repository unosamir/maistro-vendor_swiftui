//
//  ClsUnreadCount.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 2, 2019

import Foundation 
import Gloss

//MARK: - ClsUnreadCount
public class ClsUnreadCount: Glossy {
    public var unread : Int!

	//MARK: Default Initializer 
	init()
	{
        unread = 0
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let unread : Int = "unread" <~~ json {
            self.unread = unread
        }else{
            self.unread = 0
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "unread" ~~> unread,
		])
	}

}
