//
//  ClsICENotificationPayload.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 20, 2019

import Foundation 
import Gloss

//MARK: - ClsICENotificationPayload
public class ClsICENotificationPayload: Glossy {
    public var aps : ClsICENotificationAp!
    public var data : ClsICENotificationData!
    public var gcmmessageId : String!
    public var googlecae : String!

	//MARK: Default Initializer 
	init()
	{
        aps = ClsICENotificationAp()
        data = ClsICENotificationData()
        gcmmessageId = ""
        googlecae = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let aps : ClsICENotificationAp = "aps" <~~ json {
            self.aps = aps
        }else{
            self.aps = ClsICENotificationAp()
        }
        if let data : String = "data" <~~ json, data != "" {
            let jsonObj = convertToDictionary(text: data)
            self.data = ClsICENotificationData(json: jsonObj!)
        }else{
            self.data = ClsICENotificationData()
        }
        if let gcmmessageId : String = "gcm.message_id" <~~ json {
            self.gcmmessageId = gcmmessageId
        }else{
            self.gcmmessageId = ""
        }
        if let googlecae : String = "google.c.a.e" <~~ json {
            self.googlecae = googlecae
        }else{
            self.googlecae = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "aps" ~~> aps,
        "data" ~~> data,
        "gcm.message_id" ~~> gcmmessageId,
        "google.c.a.e" ~~> googlecae,
		])
	}
    
    func convertToDictionary(text: String) -> [String: AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

}
