//
//  ClsICENotificationAp.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 20, 2019

import Foundation 
import Gloss

//MARK: - ClsICENotificationAp
public class ClsICENotificationAp: Glossy {
    public var alert : ClsICENotificationAlert!
    public var badge : Int!

	//MARK: Default Initializer 
	init()
	{
        alert = ClsICENotificationAlert()
        badge = 0
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let alert : ClsICENotificationAlert = "alert" <~~ json {
            self.alert = alert
        }else{
            self.alert = ClsICENotificationAlert()
        }
        if let badge : Int = "badge" <~~ json {
            self.badge = badge
        }else{
            self.badge = 0
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "alert" ~~> alert,
        "badge" ~~> badge,
		])
	}

}
