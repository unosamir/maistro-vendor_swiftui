//
//  ClsICENotificationData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 20, 2019

import Foundation 
import Gloss

//MARK: - ClsICENotificationData
public class ClsICENotificationData: Glossy {
    public var action : ClsICENotificationAction!
    public var body : String!
    public var media : ClsICENotificationMedia!
    public var subTitle : String!
    public var title : String!
    public var type : String!
    public var uuid : String!

	//MARK: Default Initializer 
	init()
	{
        action = ClsICENotificationAction()
        body = ""
        media = ClsICENotificationMedia()
        subTitle = ""
        title = ""
        type = ""
        uuid = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let action : ClsICENotificationAction = "action" <~~ json {
            self.action = action
        }else{
            self.action = ClsICENotificationAction()
        }
        if let body : String = "body" <~~ json {
            self.body = body
        }else{
            self.body = ""
        }
        if let media : ClsICENotificationMedia = "media" <~~ json {
            self.media = media
        }else{
            self.media = ClsICENotificationMedia()
        }
        if let subTitle : String = "sub_title" <~~ json {
            self.subTitle = subTitle
        }else{
            self.subTitle = ""
        }
        if let title : String = "title" <~~ json {
            self.title = title
        }else{
            self.title = ""
        }
        if let type : String = "type" <~~ json {
            self.type = type
        }else{
            self.type = ""
        }
        if let uuid : String = "uuid" <~~ json {
            self.uuid = uuid
        }else{
            self.uuid = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "action" ~~> action,
        "body" ~~> body,
        "media" ~~> media,
        "sub_title" ~~> subTitle,
        "title" ~~> title,
        "type" ~~> type,
        "uuid" ~~> uuid,
		])
	}

}
