//
//  ClsInboxNotificationInfo.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 21, 2019

import Foundation 
import Gloss

//MARK: - ClsInboxNotificationInfo
public class ClsInboxNotificationInfo: Glossy {
    public var body : String!
    public var notificationData : ClsInboxNotificationData!
    public var persist : Bool!
    public var showBadge : Bool!
    public var title : String!
    public var type : String!
    public var uuid : String!

	//MARK: Default Initializer 
	init()
	{
        body = ""
        notificationData = ClsInboxNotificationData()
        persist = false
        showBadge = false
        title = ""
        type = ""
        uuid = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let body : String = "body" <~~ json {
            self.body = body
        }else{
            self.body = ""
        }
        if let notificationData : ClsInboxNotificationData = "notification_data" <~~ json {
            self.notificationData = notificationData
        }else{
            self.notificationData = ClsInboxNotificationData()
        }
        if let persist : Bool = "persist" <~~ json {
            self.persist = persist
        }else{
            self.persist = false
        }
        if let showBadge : Bool = "show_badge" <~~ json {
            self.showBadge = showBadge
        }else{
            self.showBadge = false
        }
        if let title : String = "title" <~~ json {
            self.title = title
        }else{
            self.title = ""
        }
        if let type : String = "type" <~~ json {
            self.type = type
        }else{
            self.type = ""
        }
        if let uuid : String = "uuid" <~~ json {
            self.uuid = uuid
        }else{
            self.uuid = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "body" ~~> body,
        "notification_data" ~~> notificationData,
        "persist" ~~> persist,
        "show_badge" ~~> showBadge,
        "title" ~~> title,
        "type" ~~> type,
        "uuid" ~~> uuid,
		])
	}

}
