//
//  ClsInboxNotificationData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 21, 2019

import Foundation 
import Gloss

//MARK: - ClsInboxNotificationData
public class ClsInboxNotificationData: Glossy {
    public var action : ClsInboxAction!
    public var data : ClsInboxData!
    public var media : ClsInboxMedia!

	//MARK: Default Initializer 
	init()
	{
        action = ClsInboxAction()
        data = ClsInboxData()
        media = ClsInboxMedia()
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let action : ClsInboxAction = "action" <~~ json {
            self.action = action
        }else{
            self.action = ClsInboxAction()
        }
        if let data : ClsInboxData = "data" <~~ json {
            self.data = data
        }else{
            self.data = ClsInboxData()
        }
        if let media : ClsInboxMedia = "media" <~~ json {
            self.media = media
        }else{
            self.media = ClsInboxMedia()
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "action" ~~> action,
        "data" ~~> data,
        "media" ~~> media,
		])
	}

}
