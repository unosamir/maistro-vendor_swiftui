//
//  ClsInboxData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 21, 2019

import Foundation 
import Gloss

//MARK: - ClsInboxData
public class ClsInboxData: Glossy {
    public var body : String!
    public var subTitle : String!
    public var title : String!

	//MARK: Default Initializer 
	init()
	{
        body = ""
        subTitle = ""
        title = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let body : String = "body" <~~ json {
            self.body = body
        }else{
            self.body = ""
        }
        if let subTitle : String = "sub_title" <~~ json {
            self.subTitle = subTitle
        }else{
            self.subTitle = ""
        }
        if let title : String = "title" <~~ json {
            self.title = title
        }else{
            self.title = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "body" ~~> body,
        "sub_title" ~~> subTitle,
        "title" ~~> title,
		])
	}

}
