//
//  ClsInboxMsg.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 21, 2019

import Foundation 
import Gloss

//MARK: - ClsInboxMsg
public class ClsInboxMsg: Glossy {
    public var notifications : [ClsInboxNotification]!
    public var page : Int!
    public var perPage : Int!
    public var totalPages : Int!
    public var totalResults : Int!
    public var unread : Int!

	//MARK: Default Initializer 
	init()
	{
        notifications = []
        page = 0
        perPage = 0
        totalPages = 0
        totalResults = 0
        unread = 0
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let notifications : [ClsInboxNotification] = "notifications" <~~ json {
            self.notifications = notifications
        }else{
            self.notifications = []
        }
        if let page : Int = "page" <~~ json {
            self.page = page
        }else{
            self.page = 0
        }
        if let perPage : Int = "per_page" <~~ json {
            self.perPage = perPage
        }else{
            self.perPage = 0
        }
        if let totalPages : Int = "total_pages" <~~ json {
            self.totalPages = totalPages
        }else{
            self.totalPages = 0
        }
        if let totalResults : Int = "total_results" <~~ json {
            self.totalResults = totalResults
        }else{
            self.totalResults = 0
        }
        if let unread : Int = "unread" <~~ json {
            self.unread = unread
        }else{
            self.unread = 0
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "notifications" ~~> notifications,
        "page" ~~> page,
        "per_page" ~~> perPage,
        "total_pages" ~~> totalPages,
        "total_results" ~~> totalResults,
        "unread" ~~> unread,
		])
	}

}
