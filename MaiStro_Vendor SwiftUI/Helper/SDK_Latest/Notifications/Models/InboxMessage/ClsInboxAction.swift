//
//  ClsInboxAction.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 21, 2019

import Foundation 
import Gloss

//MARK: - ClsInboxAction
public class ClsInboxAction: Glossy {
    public var name : String!
    public var value : String!
    public var action : String!
    
    //MARK: Default Initializer
    init()
    {
        name = ""
        value = ""
        action = ""
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let name : String = "name" <~~ json {
            self.name = name
        }else{
            self.name = ""
        }
        if let action : String = "action" <~~ json {
            self.action = name
        }else{
            self.action = ""
        }
        if let value : String = "value" <~~ json {
            self.value = value
        }else{
            self.value = ""
        }
        
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> name,
            "value" ~~> value,
            "action" ~~> action
            ])
    }

}
