//
//  ClsInboxNotification.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 21, 2019

import Foundation 
import Gloss

//MARK: - ClsInboxNotification
public class ClsInboxNotification: Glossy {
    public var createdAt : String!
    public var id : Int!
    public var isRead : Bool!
    public var notificationInfo : ClsInboxNotificationInfo!
    public var updatedAt : String!

	//MARK: Default Initializer 
	init()
	{
        createdAt = ""
        id = 0
        isRead = false
        notificationInfo = ClsInboxNotificationInfo()
        updatedAt = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let createdAt : String = "created_at" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let isRead : Bool = "is_read" <~~ json {
            self.isRead = isRead
        }else{
            self.isRead = false
        }
        if let notificationInfo : ClsInboxNotificationInfo = "notification" <~~ json {
            self.notificationInfo = notificationInfo
        }else{
            self.notificationInfo = ClsInboxNotificationInfo()
        }
        if let updatedAt : String = "updated_at" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "created_at" ~~> createdAt,
        "id" ~~> id,
        "is_read" ~~> isRead,
        "notification" ~~> notificationInfo,
        "updated_at" ~~> updatedAt,
		])
	}

}
