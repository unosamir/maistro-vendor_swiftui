//
//  Settings.swift
//  RestManager
//
//  Created by Meghdoot on 23/08/19.
//  Copyright © 2019 Appcoda. All rights reserved.
//

import UIKit

struct Settings {
    static let endpoint : String = KSDK_BASE_URL
    static let token : String = KSDK_API_TOKEN
    //"d4561a0e7158fb30cf36db59d9d5f8ed4654464b"
    
    struct api {
        static var createUser : String = String(format: "%@%@", endpoint, "/api/v1/users")
        static var updateUser : String = String(format: "%@%@", endpoint, "/api/v1/users/%@")
        static var getInboxNotification : String = String(format: "%@%@", endpoint, "/api/v1/users/%@/notifications/inbox?page=%d&limit=10&type=push&is_promotional=false")
        static var readMultipleNotification : String = String(format: "%@%@", endpoint, "/api/v1/users/%@/notifications")
        static var readSingleNotification : String = String(format: "%@%@", endpoint, "/api/v1/users/%@/notifications/%@")
        static var deleteNotification : String = String(format: "%@%@", endpoint, "/api/v1/users/%@/notifications/%@")
        static var getUnreadCount : String = String(format: "%@%@", endpoint, "/api/v1/users/%@/notifications/unread")
        static var setNotificationStatus : String = String(format: "%@%@", endpoint, "/api/v1/users/%@/devices/%@")
        static var setDeviceStatus : String = String(format: "%@%@", endpoint, "/api/v1/users/%@/devices/%@/status")
        static var getDeviceStatus : String = String(format: "%@%@", endpoint, "/api/v1/users/%@/device/%@")
    }
}
