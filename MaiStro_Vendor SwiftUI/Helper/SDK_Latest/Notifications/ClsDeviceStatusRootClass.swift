//
//  ClsDeviceStatusRootClass.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 4, 2019

import Foundation 
import Gloss

//MARK: - ClsDeviceStatusRootClass
public class ClsDeviceStatusRootClass: Glossy {
    public var apiVer : String!
    public var msg : String!
    public var payload : ClsDeviceStatusPayload!

	//MARK: Default Initializer 
	init()
	{
        apiVer = ""
        msg = ""
        payload = ClsDeviceStatusPayload()
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let apiVer : String = "api_ver" <~~ json {
            self.apiVer = apiVer
        }else{
            self.apiVer = ""
        }
        if let msg : String = "msg" <~~ json {
            self.msg = msg
        }else{
            self.msg = ""
        }
        if let payload : ClsDeviceStatusPayload = "payload" <~~ json {
            self.payload = payload
        }else{
            self.payload = ClsDeviceStatusPayload()
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "api_ver" ~~> apiVer,
        "msg" ~~> msg,
        "payload" ~~> payload,
		])
	}

}
