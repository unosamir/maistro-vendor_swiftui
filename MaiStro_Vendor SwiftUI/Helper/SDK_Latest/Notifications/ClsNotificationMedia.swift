//
//  ClsNotificationMedia.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 2, 2019

import Foundation 
import Gloss

//MARK: - ClsNotificationMedia
public class ClsNotificationMedia: Glossy {
    public var source : String!
    public var type : String!

	//MARK: Default Initializer 
	init()
	{
        source = ""
        type = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let source : String = "source" <~~ json {
            self.source = source
        }else{
            self.source = ""
        }
        if let type : String = "type" <~~ json {
            self.type = type
        }else{
            self.type = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "source" ~~> source,
        "type" ~~> type,
		])
	}

}
