//
//  ClsNotificationAp.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 2, 2019

import Foundation 
import Gloss

//MARK: - ClsNotificationAp
public class ClsNotificationAp: Glossy {
    public var alert : ClsNotificationAlert!
    public var badge : Int!
    public var sound : String!

	//MARK: Default Initializer 
	init()
	{
        alert = ClsNotificationAlert()
        badge = 0
        sound = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let alert : ClsNotificationAlert = "alert" <~~ json {
            self.alert = alert
        }else{
            self.alert = ClsNotificationAlert()
        }
        if let badge : Int = "badge" <~~ json {
            self.badge = badge
        }else{
            self.badge = 0
        }
        if let sound : String = "sound" <~~ json {
            self.sound = sound
        }else{
            self.sound = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "alert" ~~> alert,
        "badge" ~~> badge,
        "sound" ~~> sound,
		])
	}

    func convertToDictionary(text: String) -> [String: AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
