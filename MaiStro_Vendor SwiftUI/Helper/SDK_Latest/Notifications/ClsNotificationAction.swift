//
//  ClsNotificationAction.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 2, 2019

import Foundation 
import Gloss

//MARK: - ClsNotificationAction
public class ClsNotificationAction: Glossy {
    public var action : String!
    public var name : String!
    public var value : String!

	//MARK: Default Initializer 
	init()
	{
        action = ""
        name = ""
        value = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let action : String = "action" <~~ json {
            self.action = action
        }else{
            self.action = ""
        }
        if let name : String = "name" <~~ json {
            self.name = name
        }else{
            self.name = ""
        }
        if let value : String = "value" <~~ json {
            self.value = value
        }else{
            self.value = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "action" ~~> action,
        "name" ~~> name,
        "value" ~~> value,
		])
	}

}
