//
//  ClsNotificationRootClass.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 2, 2019

import Foundation 
import Gloss

//MARK: - ClsNotificationRootClass
public class ClsNotificationRootClass: Glossy {
    public var action : ClsNotificationAction!
    public var aps : ClsNotificationAp!
    public var body : String!
    public var gcmmessageId : String!
    public var googlecae : String!
    public var media : ClsNotificationMedia!
    public var subTitle : String!
    public var title : String!
    public var type : String!
    public var uuid : String!
    

	//MARK: Default Initializer 
	init()
	{
        action = ClsNotificationAction()
        aps = ClsNotificationAp()
        body = ""
        gcmmessageId = ""
        googlecae = ""
        media = ClsNotificationMedia()
        subTitle = ""
        title = ""
        type = ""
        uuid = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let action : String = "action" <~~ json, action != "" {
            let jsonObj = convertToDictionary(text: action)
            self.action = ClsNotificationAction(json: jsonObj!)
        }else{
            self.action = ClsNotificationAction()
        }
        if let aps : ClsNotificationAp = "aps" <~~ json {
            self.aps = aps
        }else{
            self.aps = ClsNotificationAp()
        }
        if let body : String = "body" <~~ json {
            self.body = body
        }else{
            self.body = ""
        }
        if let gcmmessageId : String = "gcm.message_id" <~~ json {
            self.gcmmessageId = gcmmessageId
        }else{
            self.gcmmessageId = ""
        }
        if let googlecae : String = "google.c.a.e" <~~ json {
            self.googlecae = googlecae
        }else{
            self.googlecae = ""
        }
        if let media : String = "media" <~~ json, media != "" {
            let jsonObj = convertToDictionary(text: media)
            self.media = ClsNotificationMedia(json: jsonObj!)
        }else{
            self.media = ClsNotificationMedia()
        }
        if let subTitle : String = "sub_title" <~~ json {
            self.subTitle = subTitle
        }else{
            self.subTitle = ""
        }
        if let title : String = "title" <~~ json {
            self.title = title
        }else{
            self.title = ""
        }
        if let type : String = "type" <~~ json {
            self.type = type
        }else{
            self.type = ""
        }
        if let uuid : String = "uuid" <~~ json {
            self.uuid = uuid
        }else{
            self.uuid = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "action" ~~> action,
        "aps" ~~> aps,
        "body" ~~> body,
        "gcm.message_id" ~~> gcmmessageId,
        "google.c.a.e" ~~> googlecae,
        "media" ~~> media,
        "sub_title" ~~> subTitle,
        "title" ~~> title,
        "type" ~~> type,
        "uuid" ~~> uuid,
		])
	}
    
    func convertToDictionary(text: String) -> [String: AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

}
