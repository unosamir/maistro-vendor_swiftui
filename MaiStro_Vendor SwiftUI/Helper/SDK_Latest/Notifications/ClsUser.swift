//
//  ClsUser.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on August 19, 2019

import Foundation 
import Gloss

//MARK: - ClsUser
public class ClsUser: Glossy {
    public var applicationId : Int!
    public var createdAt : String!
    public var email : String!
    public var firstName : String!
    public var id : Int!
    public var lastName : String!
    public var notificationEnabled : Bool!
    public var notificationToken : String!
    public var phone : String!
    public var refId : String!
    public var updatedAt : String!

	//MARK: Default Initializer 
	init()
	{
        applicationId = 0
        createdAt = ""
        email = ""
        firstName = ""
        id = 0
        lastName = ""
        notificationEnabled = false
        notificationToken = ""
        phone = ""
        refId = ""
        updatedAt = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let applicationId : Int = "application_id" <~~ json {
            self.applicationId = applicationId
        }else{
            self.applicationId = 0
        }
        if let createdAt : String = "created_at" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let email : String = "email" <~~ json {
            self.email = email
        }else{
            self.email = ""
        }
        if let firstName : String = "first_name" <~~ json {
            self.firstName = firstName
        }else{
            self.firstName = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let lastName : String = "last_name" <~~ json {
            self.lastName = lastName
        }else{
            self.lastName = ""
        }
        if let notificationEnabled : Bool = "notification_enabled" <~~ json {
            self.notificationEnabled = notificationEnabled
        }else{
            self.notificationEnabled = false
        }
        if let notificationToken : String = "notification_token" <~~ json {
            self.notificationToken = notificationToken
        }else{
            self.notificationToken = ""
        }
        if let phone : String = "phone" <~~ json {
            self.phone = phone
        }else{
            self.phone = ""
        }
        if let refId : String = "ref_id" <~~ json {
            self.refId = refId
        }else{
            self.refId = ""
        }
        if let updatedAt : String = "updated_at" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "application_id" ~~> applicationId,
        "created_at" ~~> createdAt,
        "email" ~~> email,
        "first_name" ~~> firstName,
        "id" ~~> id,
        "last_name" ~~> lastName,
        "notification_enabled" ~~> notificationEnabled,
        "notification_token" ~~> notificationToken,
        "phone" ~~> phone,
        "ref_id" ~~> refId,
        "updated_at" ~~> updatedAt,
		])
	}

}
