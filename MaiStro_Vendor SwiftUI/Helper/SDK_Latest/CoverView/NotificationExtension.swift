
//
//  NotificationExtension.swift
//  RestManager
//
//  Created by Meghdoot on 02/09/19.
//  Copyright © 2019 Appcoda. All rights reserved.
//

import UIKit

class Helper : NSObject {
    class func convertToDictionary(text: String) -> [String: AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func imageWithImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
        let oldWidth = sourceImage.size.width
        let scaleFactor = scaledToWidth / oldWidth
        
        let newHeight = sourceImage.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

extension UIViewController {
    
    func addShadow(bgView : UIView){
        bgView.layer.shadowOpacity = 0.2
        bgView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
        bgView.layer.shadowRadius = 1
        bgView.layer.shadowColor = UIColor.darkGray.cgColor
    }
    
    func showAlertMessage(_ alertMesasge: String) {
        let alert = UIAlertController(title: "Notification", message: alertMesasge, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
        })
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func getCleanNumber( phone : String)-> String {
        var phone = phone
        phone = phone.replacingOccurrences(of: "-", with: "")
        phone = phone.replacingOccurrences(of: " ", with: "")
        phone = phone.replacingOccurrences(of: "(", with: "")
        phone = phone.replacingOccurrences(of: ")", with: "")
        return phone
    }
}

//class Helper : NSObject {
//    class func convertToDictionary(text: String) -> [String: AnyObject]? {
//        if let data = text.data(using: .utf8) {
//            do {
//                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
//            } catch {
//                print(error.localizedDescription)
//            }
//        }
//        return nil
//    }
//
//    class func imageWithImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
//        let oldWidth = sourceImage.size.width
//        let scaleFactor = scaledToWidth / oldWidth
//
//        let newHeight = sourceImage.size.height * scaleFactor
//        let newWidth = oldWidth * scaleFactor
//
//        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
//        sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
//        let newImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        return newImage!
//    }
//}
