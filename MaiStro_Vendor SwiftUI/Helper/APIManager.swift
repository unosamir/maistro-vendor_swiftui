//
//  APIManager.swift
//  MaiStro-Vendor
//
//  Created by samir on 11/10/19.
//  Copyright © 2019 samir. All rights reserved.
//

import UIKit


class APIManager: NSObject {
    
    //MARK: Shared Instance
    static let shared = APIManager()
    
    // A network reachability instance
    let networkReachability = NetworkReachabilityManager()
    
    //MARK: - Constant
    struct Constants {
        
        static let RESOURCE_TOKEN = "ff11d85c5d130b3a61d9f1581903e7e53efa326d"
        
        struct URLs {
            
            private static let API_VER        = "/api/v1/"
            
            //MARK: Upload Image
            static let UPLOAD_IMAGE      = "https://resources2.dev.api.unoapp.io/api/v1/resources/upload"
            
            //MARK:Auth
            static let REGISTER_USER            = "\(API_VER)admins/create"
            static let LOGIN_USER               = "\(API_VER)admins/login"
            static let FORGOT_PWD               = "\(API_VER)admins/forgot_password"
            static let CHANGE_PWD               = "\(API_VER)admins/change_password"
            static let BUSINES_PROFILE          = "\(API_VER)admins/set_profile"
            static let VERIFY_LOGIN             = "\(API_VER)admins/verify_login"
            static let DASHBOARD_DATA           = "\(API_VER)admins/dashboard"
            static let CHANGE_SETTING           = "\(API_VER)admins/settings"
            
            static let CHANGE_REQUEST           = "\(API_VER)orderItemMaintenance/change_request"
            static let GET_CHANGE_REQUEST       = "\(API_VER)orderItemMaintenance/"
            static let APPROVE_CHANGE_REQUEST   = "\(API_VER)orderItemMaintenance/"

            //MARK:FAQ
            static let GET_FAQ       = "/api/app/faqs"

            //MARK: Service
            struct Services {
                
                //Request new item
                static let REQUEST_NEW_ITEM          = "\(API_VER)itemRequests"
                
                //Storage
                static let GET_STORAGE_SERVICE          = "\(API_VER)storageService/list"
                static let GET_SERVICE_ITEMS            = "\(API_VER)items"
                static let ADD_EDIT_DELETE_STORAGE_SERVICE     = "\(API_VER)storageService"
                
                //Maintenance
                static let GET_MAINTENANCE_SERVICE        = "\(API_VER)maintenanceService/list"
                static let ADD_EDIT_MAINTENANCE_SERVICE   = "\(API_VER)maintenanceService"
                static let DELETE_MULTIPLE_MAINTENANCE_SERVICE   = "\(API_VER)maintenanceService/deleteMultiple"
                static let RESTORE_ARCHIVED_SERVICE       = "\(API_VER)maintenanceService/restore_archived"
                static let GET_CHANGE_REQUEST_MAINTENANCE_SERVICE       = "\(API_VER)maintenanceService"
                
                //Transport
                static let GET_ADD_EDIT_TRANSPORT_SERVICE         = "\(API_VER)TransportationService"
                
                //Categories
                static let GET_CATEGORIES         = "\(API_VER)categories"
            }
            
            //MARK: Location
            struct Location {
                
                static let UPDATE_LOCATION_ORDER_CONFIG           = "\(API_VER)locationOrderConfig/"
                static let GET_LOCATION_HOURS                     = "\(API_VER)location/"
                static let CREATE_LOCATION_HOURS                  = "\(API_VER)locationHours/create"
                static let UPDATE_LOCATION_HOURS                  = "\(API_VER)locationHours/update"
                
                static let SET_LOCATION_HOLIDAY_HOURS                  = "\(API_VER)locationHolidayHours/set"
                static let DELETE_LOCATION_HOLIDAY_HOURS                  = "\(API_VER)locationHolidayHours/"
                
                static let SET_ACTIVE_STATUS                  = "\(API_VER)location/"
               
                static let PAYMENT_DETAILS                    = "\(API_VER)location/"
            }
            
            //MARK: Orders
            struct Orders {
                
                static let GET_STATUS_WISE_ORDERS           = "\(API_VER)orderItems"
                static let UPDATE_SERVIICE_STATUS           = "\(API_VER)orderItems/update_service_status"
                static let RECEIVE_ORDER                    = "\(API_VER)orderItems/receive_order"
                static let COMPLETE_ORDER                   = "\(API_VER)orderItems/complete_order"
                static let REPORT_PROBLEM                   = "\(API_VER)orderProblems/create"
                
                static let GET_ORDER_DETAILS                = "\(API_VER)orderItems/"
                static let EMAIL_RECEIPT                    = "\(API_VER)userOrders/"
            }
            
            //MARK: Earnings & Reports
            struct EarningsReports {
                           
                static let GET_EARNING_REPORTS    = "\(API_VER)/admins/order_list"
                        
            }
     
        }
        
        struct ResponseKey {
            
            static let code                         = "status_code"
            static let data                         = "data"
            static let message                      = "msg"
            static let list                         = "list"
        }
        
        struct ResponseCode {
            
            static let kArrSuccessCode              = [200,201]
            static let kErrorCode                   = 400
            static let kUnAuthorizeCode             = 401
            static let kNotFound                    = 404
        }
        
        struct MESSAGE {
            static let CREATE_THREAD                = "/api/v1/threads"
            static let GET_THREADS                  = "/api/v1/threads?admin_id=%@"
            
            static let CREATE_MESSAGES              = "/api/v1/messages"
            static let GET_MESSAGES_BY_THREADID     = "/api/v1/messages?thread_id=%@"
            static let GET_MESSAGES_BY_VENDORID     = "/api/v1/messages?admin_id=%@&user_id=%@"
        }
        
    }
    
    // Initialize
    private override init() {
        super.init()
        networkReachability?.startListening()
        setIndicatorViewDefaults()
    }
    
    // MARK: - Indicator view
    private func setIndicatorViewDefaults() {
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballTrianglePath
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 40, height: 40)
        NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_FONT = UIFont.boldSystemFont(ofSize: 17)
    }
    
    /// show indicator before network request
    func showIndicator(_ message:String = "", stopAfter: Double = 0.0) {
        
        let activityData = ActivityData(message: message)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        if stopAfter > 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + stopAfter) {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
    
    /// Stop Indicator Manually
    func stopIndicator() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    func isNetworkAvailable() -> Bool {
        guard (networkReachability?.isReachable)! else {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            return false
        }
        
        return true
    }
    
    var authorizationHeaders: HTTPHeaders {
        
        get{
            var headers: HTTPHeaders = [:]
            
            headers["Content-Type"] = "application/json"
            if let token = ApplicationData.user.token,token.count > 0{
                
                headers["auth-token"] = token
            }
            return headers
        }
    }
    
    // MARK: Request Promise
    func makeRequest(endPoint: String,method: Alamofire.HTTPMethod = .get, parameters: Parameters? = nil,encoding: ParameterEncoding, headers: [String:String]? = nil,isShowLoader:Bool = true,fullURL:String? = nil) -> Promise<[String:Any]> {
        return Promise { res in
            
            //show indicator
            if isShowLoader{
                //hide keyboard
                UIViewController.current().view.endEditing(true)
                self.showIndicator()
            }
            
            //Check Network
            if !self.isNetworkAvailable() {
                
                //hide indicator
                if isShowLoader{
                    
                    self.stopIndicator()
                }
                res.reject(CustomError.init(title: kAppName, description: NO_INTERNET_CONNECTION, code: Constants.ResponseCode.kErrorCode))
                return
            }
            
            //Check if full url is nil then make url else use full url
            var finalURLString = ""
            if fullURL == nil{
                
                let appInfo = "version=\(APP_VERSION)&platform=\(APP_PLATFORM)"
                if endPoint.contains("?"){
                    finalURLString = "\(SERVER)\(endPoint)&\(appInfo)"
                }else{
                    finalURLString = "\(SERVER)\(endPoint)?\(appInfo)"
                }
            }else{
                
                finalURLString = fullURL!
            }
            
            guard let finalURL = URL(string: finalURLString) else{
                
                //hide indicator
                if isShowLoader{
                    
                    self.stopIndicator()
                }
                res.reject(CustomError.init(title: kAppName, description: ERROR_NOT_EXPECTED, code: Constants.ResponseCode.kErrorCode))
                return
            }
            
            //Headers
            var finalHeader = HTTPHeaders()
            if let head = headers{
                var dictHead = head
                dictHead.merge(authorizationHeaders){(current, _) in current}
                finalHeader = dictHead
                finalHeader.removeValue(forKey: "auth-token")
            }else{
                
                finalHeader = authorizationHeaders
            }
            
            // Network request
            Alamofire.request(finalURL, method: method, parameters: parameters, encoding: encoding, headers: finalHeader).responseJSON { (response: DataResponse<Any>) in
                
                //hide indicator
                if isShowLoader{
                    
                    self.stopIndicator()
                }
                
                // check result is success
                guard response.result.isSuccess else {
                    res.reject(CustomError.init(title: kAppName, description: response.error?.localizedDescription ?? NULL_RESPONSE, code: Constants.ResponseCode.kErrorCode))
                    return
                }
                
                //response payload
                if let responseObject = response.result.value as? [String: Any]{
                    
                    //print response
                    //print(responseObject.printJson())
                    
                    //status code
                    let status_code = response.response?.statusCode ?? 0
                    
                    //message
                    var msg = responseObject[Constants.ResponseKey.message] as? String ?? ""
                    
                    //check if code is error then get msg from payload
                    if status_code == Constants.ResponseCode.kErrorCode{
                        
                        if let objPayload = responseObject["payload"] as? [String:Any],let arrDetail = objPayload["details"] as? [[String:Any]],let payloadMsg = arrDetail.first?["message"] as? String{
                            
                            msg = payloadMsg
                        }
                    }

                    //check token expired logout user if true
                    guard status_code != Constants.ResponseCode.kUnAuthorizeCode else {
                        
                        res.reject(CustomError.init(title: kAppName, description: msg, code: status_code))
                        //ApplicationData.shared.logoutUser(isShowConfirmation: false)
                        return
                    }
                    
                    //check status code
                    if Constants.ResponseCode.kArrSuccessCode.contains(status_code){
                        
                         res.fulfill(responseObject)
                    }else{
                        
                        res.reject(CustomError.init(title: kAppName, description: msg, code: status_code))
                    }
                }else{
                    
                    var msgMain = ""
                    if let msg = response.error?.localizedDescription,msg.count > 0{
                        
                        msgMain = msg
                    }else{
                        
                        msgMain = NULL_RESPONSE
                    }
                    
                    res.reject(CustomError.init(title: kAppName, description: msgMain, code: Constants.ResponseCode.kErrorCode))
                }
                
            }
        }
    }
    
    //Promise with dynamic return types
    //    func makeRequest<T>(endPoint:String,method: HTTPMethod = .get, parameters: Parameters? = nil,encoding:ParameterEncoding, headers: HTTPHeaders? = nil) -> Promise<T> {
    //        return Promise<T> { resolve, reject in
    //
    //            //Check Network
    //            if !self.isNetworkAvailable() {
    //
    //                reject(CustomError.init(title: "title", description: "desc", code: 401))
    //            }
    //
    //            //Final URL
    //            let finalURLString: String = "\(AppConstant.SERVER)\(endPoint)"
    //            guard let finalURL = URL(string: finalURLString) else{
    //                reject(CustomError.init(title: "", description: "", code: 401))
    //                return
    //            }
    //
    //            // Network request
    //            Alamofire.request(finalURL, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response: DataResponse<Any>) in
    //
    //                // check result is success
    //                guard response.result.isSuccess else {
    //                    reject(CustomError.init(title: "", description: "", code: 401))
    //                    return
    //                }
    //
    //                if let responseObject = response.result.value as? T{
    //
    //                    resolve(responseObject)
    //                }else{
    //
    //                    reject(CustomError.init(title: "Invalid type", description: "", code: 401))
    //                }
    //
    //            }
    //        }
    //
    //    }
    
    //MARK: Image Upload API
    
    func callAPIForImageUpload(arrImage:[UIImage],completion: @escaping (_ arrURL: [String]) -> Void){
        
        var arrPromises = [Promise<String>]()
        for img in arrImage{
            let imgData = img.jpegData(compressionQuality: 0.1)
            arrPromises.append(self.makeRequestForImageUpload(image: UIImage.init(data: imgData!)!))
        }
        
        APIManager.shared.showIndicator("Uploading Images", stopAfter: 0.0)
        firstly {
            
            when(resolved: arrPromises)
            
            }.done { arrResult in
                
                var arrURL = [String]()
                for res in arrResult{
                    
                    switch(res){
                    case .fulfilled(let value) :
                        arrURL.append(value)
                        break
                    default : break
                    }
                }
                APIManager.shared.stopIndicator()
                completion(arrURL)
            }.catch { error in
                
                Utilities.showAlertView(title: kAppName, message: error.localizedDescription)
                APIManager.shared.stopIndicator()
        }
        
    }
    
    fileprivate func makeRequestForImageUpload(image:UIImage) -> Promise<String> {
        return Promise { pmResult in
            
            var param = [String:Any]()
            //            param["business_id"] = 1313
            //            param["folder_id"] = 489
            //            param["folder"] = "5d4be1ddfe7d582ebadcf30f" // Commented By HD Reference By DK Given By CV observer MD
            
            param["description"] = kAppName
            param["content_type"] = "image/jpeg"
            param["name"] = String(format: "iOS_IMG_%@", Date.init() as CVarArg).replacingOccurrences(of: " ", with: "")
            
            var header = [String:String]()
            header["api-token"] = APIManager.Constants.RESOURCE_TOKEN
            
            APIManager.shared.makeRequest(endPoint: "", method: .post, parameters: param, encoding: JSONEncoding.default, headers: header,isShowLoader: false,fullURL: APIManager.Constants.URLs.UPLOAD_IMAGE).done { (res) in
                
                DispatchQueue.main.async {
                    
                    print(res)
                    
                    if let dictPayload = res["payload"] as? [String:Any]{
                        
                        //if let url = dictPayload["pre_signed_url"] as? String{
                        if let url = dictPayload["upload_url"] as? String{
                            let imgUrl = URL.init(string:url)
                            var absoluteURL = imgUrl?.absoluteString.replacingOccurrences(of: (imgUrl?.query)!, with: "")
                            absoluteURL = absoluteURL?.replacingOccurrences(of: "?", with: "")
                            
                            
                            //Image upload
                            let header = ["Content-Type":"image/jpeg"]
                            let imgData = image.jpegData(compressionQuality: 0.75)
                            if let image = imgData{
                                
                                Alamofire.upload(image, to: url, method: .put, headers: header).response { (res) in
                                    
                                    if res.response?.statusCode == 200 {
                                        
                                        pmResult.fulfill(absoluteURL ?? "")
                                    }else{
                                        
                                        let errorTemp = NSError(domain:res.error?.localizedDescription ?? "Something went wrong please try again later", code: res.response?.statusCode ?? 401, userInfo:nil)
                                        pmResult.reject(errorTemp)
                                    }
                                    
                                }
                            }
                            
                            
                        }
                        
                    }
                    
                }
            }
                
                .catch { (err) in
                    
//                    Utilities.showAlertView(title: kAppName, message: err.localizedDescription)
                    let errorTemp = NSError(domain:err.localizedDescription, code: 401, userInfo:nil)
                    pmResult.reject(errorTemp)
            }
            
            
        }
        
    }
    
}


protocol OurErrorProtocol: LocalizedError {
    
    var title: String? { get }
    var code: Int { get }
}

struct CustomError: OurErrorProtocol {
    
    var title: String?
    var code: Int
    var errorDescription: String? { return _description }
    var failureReason: String? { return _description }
    
    private var _description: String
    
    init(title: String?, description: String, code: Int) {
        self.title = title ?? "Error"
        self._description = description
        self.code = code
    }
}

