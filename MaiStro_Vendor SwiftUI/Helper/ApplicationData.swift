//
//  ApplicationData.swift
//  MeditabSwiftArchitecture
//
//  Created by Rushi on 23/05/17.
//  Copyright © 2017 Meditab. All rights reserved.
//

import UIKit

/** This singleton class is used to store application level data. */
class ApplicationData: NSObject {

    // A Singleton instance
    static let shared = ApplicationData()
  
//    // Checks if user is logged in
//    static var isUserLoggedIn: Bool {
//        get {
//            return Defaults.bool(forKey: AppConstants.UserDefaultKey.isUserLoggedIn)
//        }
//    }
//
//    /// Default headers to be passed in CenterAPI requests
//    var authorizationHeaders: HTTPHeaders {
//
//        get{
//            var headers: HTTPHeaders = [:]
//
//            if ApplicationData.isUserLoggedIn{
//
//                headers[NetworkClient.Constants.HeaderKey.Authorization] = "JWT \(ApplicationData.user.token ?? "")"
//            }
//            return headers
//        }
//    }
//
//
    // returns logged in user's information
    static var user: UserDetailModel {
        get {
            return UserDefaultsMethods.getCustomObject(key: AppConstants.UserDefaultKeys.UserInfoModelKey) as? UserDetailModel ?? UserDetailModel()
        }
    }

    
    // returns Current Time Zone
    
    static var localTimeZoneName: String { return TimeZone.current.identifier }

    //retun device Name
    static var deviceName: String {
        get {
            return UIDevice.current.localizedModel
        }
    }

    //retun device version
    static var deviceVersion: String {
        get {
            return UIDevice.current.systemVersion
        }
    }
    
    static var deviceId: String {
        get {
            return UIDevice.current.identifierForVendor!.uuidString
        }
    }
    
    static var ipAddress : String {

        get {

            var address : String?

            // Get list of all interfaces on the local machine:
            var ifaddr : UnsafeMutablePointer<ifaddrs>?
            guard getifaddrs(&ifaddr) == 0 else { return "" }
            guard let firstAddr = ifaddr else { return "" }

            // For each interface ...
            for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
                let interface = ifptr.pointee

                // Check for IPv4 or IPv6 interface:
                let addrFamily = interface.ifa_addr.pointee.sa_family

                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {

                    // Check interface name:
                    let name = String(cString: interface.ifa_name)

                    if  name == "en0" || name == "pdp_ip0"  {

                        // Convert interface address to a human readable string:
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                    &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
            return address ?? ""
        }
    }



    //return UUID
    func getUUID() -> String
    {
        return UUID().uuidString
    }

    //MARK: Maintain Usesr

    //MARK: Private Methods

    func loginUser(){
      //  kAppDelegate.joinSocketChannel()
        
        //kAppDelegate.loadUnreadCount()
        
//        let vc = UIStoryboard(name: "Dashboard", bundle: .main).instantiateViewController(withIdentifier: "TabbarVC") as! TabbarVC
//        kAppDelegate.window?.rootViewController = vc
//        kAppDelegate.window?.makeKeyAndVisible()
//
//        UIView.transition(with: kAppDelegate.window!,
//                          duration: 0.5,
//        options: .transitionCrossDissolve,
//        animations: nil,
//        completion: nil)
    }
    
//    func logoutUser(isShowConfirmation:Bool)
//    {
//
//        if isShowConfirmation{
//            
//            Utilities.showAlertWithTwoButtonAction(title: kAppName, message: StringConstants.MessageConstant.LogoutMsg, attributedMessage: nil, buttonTitle1: StringConstants.ButtonConstant.kNo, buttonTitle2: StringConstants.ButtonConstant.kYes, onButton1Click: {
//                
//            }) {
//                
//                self.logout()
//            }
//        }else{
//            
//            self.logout()
//        }
//       
//    }
    
//    private func logout(){
//
//        kAppDelegate.unreadcount = 0
//        NotificationCenter.default.post(name: Notification.Name.UnreadCount, object: ObjectNotify(count: 0))
//
//        var firebaseToken =  ""
//        if let token = UserDefaultsMethods.getString(key: KFireBaseToken){
//            firebaseToken = token
//        }
//
//        CommonMethods.logoutFromNotificationSdk()
//        UserDefaultsMethods.removeAllKeyFromDefault()
//        if firebaseToken != ""{
//            UserDefaultsMethods.setString(value: firebaseToken, key: KFireBaseToken)
//        }
//
//        let vc = UIStoryboard(name: "Login", bundle: .main).instantiateViewController(withIdentifier: "SelectSignUpVC") as! SelectSignUpVC
//        let nav = AppNavigationController(rootViewController: vc)
//        kAppDelegate.window?.rootViewController = nav
//        kAppDelegate.window?.makeKeyAndVisible()
//
//        UIView.transition(with: kAppDelegate.window!,
//                          duration: 0.5,
//        options: .transitionCrossDissolve,
//        animations: nil,
//        completion: nil)
//    }

}


