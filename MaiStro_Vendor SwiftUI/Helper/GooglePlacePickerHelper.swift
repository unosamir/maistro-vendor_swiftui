//
//  GooglePlacePickerHelper.swift
//  MaiStro-Vendor
//
//  Created by samir on 06/11/19.
//  Copyright © 2019 samir. All rights reserved.
//

import UIKit

class GooglePlacePickerHelper: NSObject {

    //MARK: Variables
    static let shared = GooglePlacePickerHelper()
    private var presentationController : UIViewController?
    private var completion : ((GooglePlaceDataModel) -> ())?
    
    //MARK: Imagepicker controller
    func openGooglePlacePicker(for controller: UIViewController, completion: @escaping ((GooglePlaceDataModel) -> ())){
        
        self.presentationController = controller
        self.completion = completion
        let placePickerController = GMSAutocompleteViewController()
        let filter = GMSAutocompleteFilter()
        filter.country = "CA"  //appropriate country code
        placePickerController.autocompleteFilter = filter
        placePickerController.delegate = self
        self.presentationController?.present(placePickerController, animated: true, completion: nil)
    }
}

//MARK: Place Picker delegates
extension GooglePlacePickerHelper: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if place.coordinate.latitude == 0.0 || place.coordinate.longitude == 0.0{
            
            Utilities.showAlertView(title: kAppName, message: "Please select valid address")
        }else{
            
            self.presentationController?.dismiss(animated: true) {
                
                var model = GooglePlaceDataModel()
                model.full_address = place.formattedAddress ?? ""
                model.latitude = place.coordinate.latitude
                model.longitude = place.coordinate.longitude
                
                if !model.full_address.contains(place.name ?? ""){
                    
                    model.full_address = "\(place.name ?? ""), \(place.formattedAddress ?? "")"
                }
                model.street1 = place.name ?? ""
                
                model = self.getPlaceAttributes(place: place, model: model)
                
                self.completion?(model)
            }
        }
        
    }

    private func getPlaceAttributes(place:GMSPlace,model:GooglePlaceDataModel) -> GooglePlaceDataModel{
        
        if let componnent = place.addressComponents{
            
//            var arrStreet1 = [String]()
//            var arrStreet2 = [String]()
            
            for addressComponent in componnent {
                for type in (addressComponent.types){
                    
                    switch(type){
                    case "locality":
                        model.city = addressComponent.name
                        
                    case "country":
                        model.country = addressComponent.name
                        
                    case "administrative_area_level_1":
                        model.state = addressComponent.name
                    
                    case "postal_code":
                        model.postal_code = addressComponent.name
                        
//                    case "street_number","route","neighborhood","sublocality_level_1":
//
//                        arrStreet1.append(addressComponent.name)
//                    case "administrative_area_level_2","sublocality_level_2":
//
//                        arrStreet2.append(addressComponent.name)
                    default:
                        break
                    }
                    
                }
            }
            
//            model.street1 = arrStreet1.joined(separator: ", ")
//            model.street2 = arrStreet2.joined(separator: ", ")
        }
        
        return model
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.presentationController?.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

class GooglePlaceDataModel: NSObject {
    
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var city : String = ""
    var state : String = ""
    var country : String = ""
    var full_address : String = ""
    var postal_code : String = ""
    var street1 : String = ""
    var street2 : String = ""
}
