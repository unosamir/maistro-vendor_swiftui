//
//  EnumConstant.swift
//  MaiStro-Vendor
//
//  Created by samir on 05/09/19.
//  Copyright © 2019 samir. All rights reserved.
//

import UIKit

enum CellType {
    
    case none
    
    //Maintenance
    case MaintenanceItemType
    case MaintenanceCategory
    case MaintenanceTitle
    case MaintenanceDescription
    
    
    //Availability
    case AvailabilityGeneralSection
    case AvailabilityExceptionSection
    
    //ServiceDetails
    case Service
    case Orders
    case Customer
    
    case ServiceTime
    case PickupReturnStorage
    case OrderSubtotal
    case CustCell
    case OrderButtonDirection
    case OrderButtonReceipt
    case CustNotes
    
    case Storage
    case Maintenance
    case Transport
    case Packages
    
    //Acoount
    case ServicesPrices
    case HoursAvailability
    case PaymentProcessing
    case EarningsReports
    case ReferCustomer
    case About
    case HelpSupport
    case PrivacyPolicy
    case Settings
    
    //Package
    case Package
    case TotalDiscount
    case MarkedDown
}

enum ServiceType{
    
    case Storage
    case Transport
    case Maintenance
    case CombineService
}

enum OrderStatus{
    
    case Pending
    case Approved
    case Received
    case Complete
}

enum AboutScreenType {
    
    case About
    case HelpSupport
    case PrivacyPolicy
}
