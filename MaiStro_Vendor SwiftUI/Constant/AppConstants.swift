
import Foundation
import UIKit

//MARK: Global Constant
let DEFAULT_OPEN_TIME = "09:00:00"
let DEFAULT_CLOSE_TIME = "20:00:00"

let kAppDelegate = UIApplication.shared.delegate as! SceneDelegate
let APP_VERSION: String = Bundle.main.releaseVersionNumber!
let APP_BUILD_NUMBER: String = Bundle.main.buildVersionNumber!
let APP_BUNDLE_IDENTIFIER = Bundle.main.bundleIdentifier
let APP_PLATFORM = "ios"
let KStatusBarHeight = UIApplication.shared.statusBarFrame.height
let GOOGLE_PLACE_API_KEY = "AIzaSyBJs2f0TNNui_OcRHgVRTIsJAvGQ0EB7oA"

//MARK: Location Model
var userBusinessLocationModel : BusinessProfileModel{
    get{
        
        return ApplicationData.user.business_profile ?? BusinessProfileModel()
    }
}
let kNEW_NOTIFICATION = "new_notification"
let kUNREAD_COUNT_RELOAD_NOTIFICATION = "kUNREAD_COUNT_RELOAD_NOTIFICATION"

let kReloadPricing = "kReloadPricing"

//MARK : Target Wise Data
//let GOOGLE_PLACE_API_KEY = "AIzaSyDKJQAdeYS1wtatct71W_4n2RtZi4HsfKw"

let KFireBaseToken : String = "KFireBaseToken"

let URL_TERMS_CONDITION : String = "https://www.maistro.app/terms-of-use"
let URL_PRIVACY_POLICY : String = "https://www.maistro.app/about"
let URL_ABOUT_US : String = "https://www.maistro.app/about-us"

#if DEV

    let kAppName = "MaiStro Vendor"
    let GOOGLE_CLIENT_ID = "424186683852-daqf2lnctr9rf5fnr13d49en7hsi29d0.apps.googleusercontent.com"
    let SERVER = "https://maistro.dev.api.unoapp.io"
    let FAQ_SERVER = "http://faq.dev.api.unoapp.io"
    let FAQ_BUSINESS_ID = "19"

    //For Notification uses
    let KSDK_BASE_URL = "https://push-notifications.dev.api.unoapp.io"
    let KSDK_API_TOKEN = "1eb25290301cbadc826f73c592d12359ee133e2a"
    
    //stripe
    let STRIPE_PAYMENT_URL = "https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://maistro.dev.api.unoapp.io/api/v1/stripe/express/&client_id=ca_CBr3oCEX56we5n2p315QYRJ2dzVv1BXO&stripe_user[business_type]=company&scope=read_write&stripe_landing=login&state=\(userBusinessLocationModel.id ?? 0)"

#elseif BETA

    let kAppName = "MaiStro Vendor"
    let GOOGLE_CLIENT_ID = "424186683852-2033u6m6u0jl4dtiqsqg5otls32cfq2v.apps.googleusercontent.com"
    let SERVER = "https://maistro.beta.api.unoapp.io"
    let FAQ_SERVER = "http://faq.beta.api.unoapp.io"
    let FAQ_BUSINESS_ID = "8"

    //For Notification uses
    let KSDK_BASE_URL = "https://push-notifications.staging.api.unoapp.io"
    let KSDK_API_TOKEN = "7901e42d603ce4b2658f42e583700f0f47aa1654"
    
    //stripe
    let STRIPE_PAYMENT_URL = "https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://maistro.beta.api.unoapp.io/api/v1/stripe/express/&client_id=ca_CBr3oCEX56we5n2p315QYRJ2dzVv1BXO&stripe_user[business_type]=company&scope=read_write&stripe_landing=login&state=\(userBusinessLocationModel.id ?? 0)"

#elseif UAT

    let kAppName = "MaiStro Vendor"
    let GOOGLE_CLIENT_ID = "424186683852-ioqgiivqf9u1bt99faic898k6t4m93rb.apps.googleusercontent.com"
    let SERVER = "https://maistro.uat.api.unoapp.io"
    let FAQ_SERVER = "https://faq.uat.api.unoapp.io"
    let FAQ_BUSINESS_ID = "3"

    //For Notification uses
    let KSDK_BASE_URL = "https://push-notifications.uat.api.unoapp.io"	
    let KSDK_API_TOKEN = "9d5ea5e19e547ea1001518c730ed20cf758b98a9"
    
    //stripe
    let STRIPE_PAYMENT_URL = "https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://maistro.uat.api.unoapp.io/api/v1/stripe/express/&client_id=ca_CBr3oCEX56we5n2p315QYRJ2dzVv1BXO&stripe_user[business_type]=company&scope=read_write&stripe_landing=login&state=\(userBusinessLocationModel.id ?? 0)"

#elseif LIVE

let kAppName = "MaiStro Vendor"
let GOOGLE_CLIENT_ID = "424186683852-lhl1o051snjae21vpd2b08nabbmp1907.apps.googleusercontent.com"
let SERVER = "https://maistro.api.unoapp.io"
let FAQ_SERVER = "https://faq.api.unoapp.io"
let FAQ_BUSINESS_ID = "9"

//For Notification uses
let KSDK_BASE_URL = "https://push-notifications.api.unoapp.io"
let KSDK_API_TOKEN = "892b2139d004b7bc29143b32e7218afe20161d6d"

//stripe
let STRIPE_PAYMENT_URL = "https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://maistro.api.unoapp.io/api/v1/stripe/express/&client_id=ca_Fd1hxq0aV8EcuTi8ZE2x1wMzxEZwabw0&stripe_user[business_type]=company&scope=read_write&stripe_landing=login&state=\(userBusinessLocationModel.id ?? 0)"

#elseif APPSTORE

let kAppName = "MaiStro Vendor"
let GOOGLE_CLIENT_ID = "424186683852-lhl1o051snjae21vpd2b08nabbmp1907.apps.googleusercontent.com"
let SERVER = "https://maistro.api.unoapp.io"
let FAQ_SERVER = "https://faq.api.unoapp.io"
let FAQ_BUSINESS_ID = "9"

//For Notification uses
let KSDK_BASE_URL = "https://push-notifications.api.unoapp.io"
let KSDK_API_TOKEN = "892b2139d004b7bc29143b32e7218afe20161d6d"

//stripe
let STRIPE_PAYMENT_URL = "https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://maistro.api.unoapp.io/api/v1/stripe/express/&client_id=ca_Fd1hxq0aV8EcuTi8ZE2x1wMzxEZwabw0&stripe_user[business_type]=company&scope=read_write&stripe_landing=login&state=\(userBusinessLocationModel.id ?? 0)"

#endif

//MARK: AppConstant
struct AppConstants {
  
    //placeholder image
    static let placeholderImage = UIImage(named: "placeholderImage")
    
    struct UserDefaultKeys {
        
        static let UserInfoModelKey                = "UserInfoModelKey"
    }
 
    struct OrderStatus_APIText {
        
        static let SUBMITTED                        = "submitted"
        static let ACCEPTED                         = "accepted"
        static let RECEIVED                         = "received"
        static let COMPLETED                        = "completed"
        static let PENDING_CONFIRMATION             = "pending_complete_confirmation"
        static let REJECTED                         = "rejected"
        static let CANCELLED                        = "cancelled"
        static let APPROVED                         = "approved"
        
    }
    
    struct TransportType_Text {
        
        static let RETURN                           = "return"
        static let PICKUP                           = "pickup"
        static let TRANSPORT                        = "transport"
    }
    
    struct PushNotificationTypes {
        
        static let MESSAGE                           = "message"
        static let STATUS_UPDATE                     = "status_update"
        static let ORDER_PLACED                      = "order_placed"
        static let CHANGE_REQUEST                    = "change_request"
    }
    
    struct NotificationName {
        
        static let CHECK_VALIDATION                 = "checkValidation"
    }
}

//MARK: Device Constant
class DeviceConstant{
    
    enum UIUserInterfaceIdiom : Int
    {
        case Unspecified
        case Phone
        case Pad
    }
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_X_OR_ABOVE          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
        
    }
}
