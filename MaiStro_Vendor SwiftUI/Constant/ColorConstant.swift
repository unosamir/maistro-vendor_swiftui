//
//  ColorConstant.swift
//  MaiStro-Vendor
//
//  Created by samir on 27/08/19.
//  Copyright © 2019 samir. All rights reserved.
//

import UIKit

class ColorConstant: NSObject {

    static let ThemeColor                   = #colorLiteral(red: 0.1725490196, green: 0.6235294118, blue: 0.8156862745, alpha: 1)
    static let TextMainColor                = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
    static let SelectedServiceBoxColor      = #colorLiteral(red: 0.8823529412, green: 0.937254902, blue: 0.9529411765, alpha: 1)
    static let TextFieldTextColor           = #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1)
    static let TextFieldErrorColor          = #colorLiteral(red: 0.8156862745, green: 0.007843137255, blue: 0.1058823529, alpha: 1)
    static let DisableAndTextFieldBGColor   = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
    static let OffSwitchColor               = #colorLiteral(red: 0.8274509804, green: 0.8274509804, blue: 0.8274509804, alpha: 1)
    static let OrderDisableColor            = #colorLiteral(red: 0.8117647059, green: 0.8117647059, blue: 0.8117647059, alpha: 1)
    static let TabBarTextColor              = #colorLiteral(red: 0.262745098, green: 0.2823529412, blue: 0.3019607843, alpha: 1)
    static let PendingConfirmation          = #colorLiteral(red: 0.8196078431, green: 0.262745098, blue: 0, alpha: 1)
    static let PackageDisabled              = #colorLiteral(red: 0.8117647059, green: 0.8274509804, blue: 0.8392156863, alpha: 1)
    
    
//    D9D9D9
}
