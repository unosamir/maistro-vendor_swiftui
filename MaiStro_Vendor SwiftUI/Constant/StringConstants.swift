
import Foundation

struct StringConstants {
    
    ///// DO NOT CHANGE ANY CONSTANT WITHOUT DISCUSSION OF TEAM ///////////
    
    struct ButtonConstant {
        
        static let kOk                                      = "Ok"
        static let kCancel                                  = "Cancel"
        static let kYes                                     = "Yes"
        static let kNo                                      = "No"
    }
    
    struct MessageConstant {
        
        static let SaveEmailPassword                        = "Do you want to save your email and password?"
        static let ChangePwdSuccess                         = "Password change successfully."
        static let LogoutMsg                                = "Are you sure you want to sign out?"
        static let SelectOneService                         = "Please select at least one service."
        static let MaxOrderPerDay                           = "Please enter valid max order per day."
        static let SelectOneItem                            = "Please select at least one item."
        static let ItemNameValidation                       = "Item name should be at least 2 character long."
        static let NoImageFound                             = "No image added for this order."
        static let OneImageValidation                       = "Please add at least one image."
        static let SelectHolidaydate                        = "Please select holiday date."
        static let DeleteConfirm                            = "Are you sure you want to delete?"
        static let StorageDeleteConfirm                     = "Are you sure you want to delete storage service for this item?"
        static let ArchiveConfirm                           = "Are you sure you want to archive?"
        static let SelectProblem                            = "Please select a problem."
        static let EnterMessage                             = "Please enter a message."
        static let RejectMsg                                = "Are you sure you want to reject?"
        static let NoOrderApprove                           = "You don't have any service to approve."
        static let HourValidation                           = "Please select all closing time after opening time."
        static let OpenCloseTime                            = "Please select closing time after opening time."
        static let ConfirmRequestChange                     = "Are you sure you want to submit change request?"
        static let RequestApproved                          = "Request is already approved."

        
        static let StorageDuration                            = "Please enter storage duration."
        static let BeginDate                                  = "Please select begin date."
        static let FinishDate                                 = "Please select finish date."
        
        static let PackageTitle                               = "Please enter package title."
        static let PackageDesc                                = "Please enter package description."
        
        static let PendingReqChange                                = "There is pending request change for this order."
    }
    
    struct NoDataFound {
        
        static let NoItemFound                              = "No Items Found"
        static let NoOfferAdded                             = "You have not added any offers."
        static let NoArchiveFound                           = "You don't have archived services."
        static let NoOrderFound                             = "You don't have any orders yet."
        static let NoReports                                = "You don't have any orders"
        static let NoPackages                               = "You have not added any packges"
    }
  
    //transport
    struct TransportValidation {
        
        static let PriceMsg                                 = "Please enter price per km."
        static let TripsPerDayMsg                           = "Please enter trips per day."
        static let RadiusMsg                                = "Please select regular service radius."
        static let FreeTransportSorage                      = "Please select free transportation for storage."
        
        static let PricePerKm                                 = "Please enter price per km for extended radius service."
        static let FreeTransport                                 = "Please enter free transportation order limit."
         static let SelectRadius                                 = "Please select extended service radius."
    }
    
    //maintenance
    struct MaintenanceValidation {
        
        static let CategoryType                             = "Please select category type."
        static let ItemType                                 = "Please select item type."
        static let EnterTitle                               = "Please enter valid title."
        static let EnterPrice                               = "Please enter price."
        static let EnterDaysToComplete                      = "Please enter days to complete."
    }
}

/*****************************************************************************/

var PASSWORD_MATCHES : String = "Matches"
var ATLEAST_8_CHARACTERS : String = "At least 8 characters"
var ATLEAST_1_NUMBER : String = "At least one number"
var ATLEAST_1_CAPITAL : String = "At least one capital letter"
var ATLEAST_1_LOWERCASE : String = "At least one lowercase letter"
var ATLEAST_1_SPECIAL_CHARACTER : String = "At least one special character"
var PASSWORD_FORMATE_MESSAGE : String = "Please enter a valid password (minimum 8 characters with at least 1 capital letter and lower letter and number and special character)"

//MARK: Error
let ERROR_NOT_EXPECTED : String = "Unfortunately, we were not able to process your request. Please try again later."
let NULL_RESPONSE : String = "Unfortunately, we were not able to process your request. Please try again later."
let NO_INTERNET_CONNECTION : String = "There seems to be a connectivity issue. Please check your connection to the network and try again."
let REQUEST_TIMEOUT :String = "We ran into an issue getting you the information you requested. Please try again later."

//MARK:- ===Login-Registration-forgotpassword-Profile update-validations messages==

let please_enter_an_email_address = "Please enter an email address"
let please_enter_a_password = "Please enter a password"
let please_enter_a_valid_email_address = "Please enter a valid email address"
let Facebook_Access_token_expired = "Facebook access token is expired!"
let please_enter_a_first_name = "Please enter a first name"
let please_enter_a_last_name = "Please enter a last name"
let please_enter_a_valid_phonenumber = "Please enter a valid phone number"
let please_enter_a_valid_email = "Please enter a valid email"

//MARK:- ==== Change Password ====

let please_enter_a_current_password = "Please enter a current password"
let please_enter_a_new_password = "Please enter a new password"
let please_enter_a_confirm_password = "please enter a confirm password"
let password_does_not_match = "Password does not match"
let new_password_should_not_be_same_as_old_password = "New password should not be same as old password"
let Password_changed_successfully = "Password changed successfully."

//MARK:- ====Help Module Messages====

let please_setup_mail_account = "please set up a mail account in order to send email"
let msg_feedback_sent = "Feedback submitted successfully."

//MARK:- ==== MENU ITEM DETAIL SCREEN====

let msg_cart_item_remove_have_reward_promocode_giftcard = "By editing items in your cart, you will also be removing any rewards, promo code or gift card that were applied and will need to reapply the rewards, promo code and gift card. Are you sure you want to remove this item?"

let msg_cart_item_add_have_reward_promocode_giftcard = "By editing items in your cart, you will also be removing any rewards, promo code or gift card that were applied and will need to reapply the rewards, promo code and gift card. Are you sure you want to add this item?"

let msg_cart_item_edit_have_reward_promocode_giftcard = "By editing items in your cart, you will also be removing any rewards, promo code or gift card that were applied and will need to reapply the rewards, promo code and gift card. Are you sure you want to edit this item?"

let msg_cart_item_catering_add_previous_cart_regular = "Your regular items cart will be removed. Are you sure you want to proceed?"

let msg_cart_item_regular_add_previous_cart_catering = "Your catering items cart will be removed. Are you sure you want to proceed?"

let msg_reorder_clear_cart = "This may remove any existing items from your cart and change your store location."


//MARK:- ===== Cart Screen messages ===

let msg_promocode_applied = "Promocode applied successfully."
let msg_promocode_greterthan_cart = "The value of your promocde is more than your total."
let msg_reward_morethan_cart = "The value of your rewards and discounts are more than your total."


//MARK:- =====Add/Remove Payment Message ===

let msg_select_country_first = "Please select country first."
let msg_wantTo_remove_payment = "Are you sure you want to remove this payment card?"
let msg_enter_creditcard = "Please enter a credit card"
let msg_enter_valid_card = "Please enter a valid credit card"
let msg_enter_valid_exp_date  = "Please enter a valid expiry date"
let msg_enter_exp_date = "Please enter an expiry date"
let msg_country_select = "Please select country"
let msg_enter_postalCode = "Please enter a postal code"
let msg_enter_valid_postalCode = "Please enter a valid postal code"
let msg_enter_cvv = "Please enter a CVV"
let msg_cvv_length = "CVV length should be 3 or 4 digits"
let msg_card_seems_expire = "Your card seems to be expired."
//MARK:- ======Gift card Messages =====

let msg_please_select_giftCard = "Please select gift card"
let msg_please_select_Card = "Please select debit/credit card"
let msg_cant_select_more_giftcard = "You can't select any more gift card."
let msg_topup_balnce = "Please top up balance"
let msg_amount_greterThan_0 = "Amount should be greater than 0."
let msg_add_payment_method = "Please add payment method"
let MAXIMUM_CARD_BALANCE_LIMIT_REACH : String = "you can load max $"
let MIN_MAX_CARD_BALANCE_RELOAD_LIMIT_REACH : String = "Amount should be between $"
let msg_enter_valid_card_number = "Please enter valid card number."
let msg_enter_pinNo = "Please enter pin number."
let msg_enter_giftcard = "Please enter a gift card number"
let msg_enter_valid_giftcardNo = "Please enter a valid gift card number"
let msg_enter_pin = "Please enter a pin"
let msg_giftcard_pin_length  = "Pin length should be 4 digits"

//MARK:- ===Cart screen messages====

 let ON_SIMPLE_DELETE_OR_UPDATE_CART_ITEM_MESSAGE = "Are you sure you want to remove this item?"
 let msg_No_cart_found = "No cart found"
 let msg_Please_add_payment_method = "Please add payment method"
 let msg_please_select_pickup_time = "Please select pickup time."
 let msg_thankyou_for_place_order = "Thank you for your order. We’ll let you know when it’s ready for pickup!"
 let msg_reward_apply_success = "Rewards applied Successfully."
 let msg_remove_giftcart_while_Applying_reward_promocode = "By editing this, you will also be removing giftcard that were applied and will need to reapply the giftcard."
 let msg_reward_remove_success = "Rewards removed Successfully."
 let msg_reward_more_then_total_are_you_sure = "The value of your rewards are more than your total. Are you sure you want to apply this redeem point?"
 let msg_promocode_remove_success = "Promocode removed Successfully."
 let msg_login_session_expired = "Please login again! Your login session has been expired."
 let msg_giftcardbalance_not_sufficient_for_checkout = "Gift card balance is not sufficient for checkout."
let msg_please_select_payment_method = "Please select payment method."

//MARK:- ===Select Location messages====
let msg_cart_will_be_empty_to_change_location = "Your cart will become empty from previous store. Would you like to order from this location?"
let msg_could_not_get_cart = "Could not get cart ID."

//MARK:- ==== Item modification methods ===

let msg_items_select_lessthen_modifer_max = "Sorry, you can only add %@ items for %@."
let msg_modItem_cannotbe_remove = "%@ cannot be removed."
let msg_select_atleast = "Select atleast %@ modifier item for %@."
let msg_sure_to_remove_existing_items = "Are you sure you want to remove all existing items from %@?"
