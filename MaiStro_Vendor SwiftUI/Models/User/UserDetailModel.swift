//
//  UserDetailModel.swift
//  MaiStro-Vendor
//
//  Created by samir on 15/10/19.
//  Copyright © 2019 samir. All rights reserved.
//

import UIKit

class UserDetailModel: NSObject,NSCoding {
    
    public var id: Int?
    public var first_name: String?
    public var last_name: String?
    public var email: String?
    public var phone: String?
    public var token: String?
    public var marketing_opt_in = Bool()
    
    public var business_profile: BusinessProfileModel?
    
    override init() {
        
    }
    
    required public init?(dictionary: [String:Any]) {
        
        id = dictionary["id"] as? Int
        first_name = dictionary["first_name"] as? String
        last_name = dictionary["last_name"] as? String
        email = dictionary["email"] as? String
        phone = dictionary["phone"] as? String
        token = dictionary["token"] as? String
        marketing_opt_in = dictionary["marketing_opt_in"] as? Bool ?? false
        
        if let dictBusiness = dictionary["business_profile"] as? [String:Any]{
            
            business_profile = BusinessProfileModel.init(dictionary: dictBusiness)
        }
    }
    
    // Encode decode
    required init?(coder aDecoder: NSCoder) {
        
        id    = aDecoder.decodeObject(forKey: "id") as? Int
        first_name    = aDecoder.decodeObject(forKey: "first_name") as? String
        last_name    = aDecoder.decodeObject(forKey: "last_name") as? String
        email    = aDecoder.decodeObject(forKey: "email") as? String
        phone    = aDecoder.decodeObject(forKey: "phone") as? String
        token    = aDecoder.decodeObject(forKey: "token") as? String
        marketing_opt_in    = aDecoder.decodeBool(forKey: "marketing_opt_in")
        
        business_profile   = aDecoder.decodeObject(forKey: "business_profile") as? BusinessProfileModel
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(id, forKey: "id")
        aCoder.encode(first_name, forKey: "first_name")
        aCoder.encode(last_name, forKey: "last_name")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(token, forKey: "token")
        aCoder.encode(marketing_opt_in, forKey: "marketing_opt_in")
        
        aCoder.encode(business_profile, forKey: "business_profile")
    }
}

//MARK: Business Profile
class BusinessProfileModel: NSObject,NSCoding {
    
    public var id: Int?
    public var admin_id: Int?
    public var name: String?
    public var phone: String?
    public var license_number: String?
    public var province: String?
    public var city: String?
    public var address: String?
    public var postal_code: String?
    public var latitude: Double?
    public var longitude: Double?
    public var tax: Double?
    
    public var status = Bool()
    public var is_storage = Bool()
    public var is_transport = Bool()
    public var is_maintenance = Bool()
    
    override init() {
        
    }
    
    required public init?(dictionary: [String:Any]) {
        
        id = dictionary["id"] as? Int
        admin_id = dictionary["admin_id"] as? Int
        name = dictionary["name"] as? String
        phone = dictionary["phone"] as? String
        license_number = dictionary["license_number"] as? String
        province = dictionary["province"] as? String
        city = dictionary["city"] as? String
        address = dictionary["address"] as? String
        postal_code = dictionary["postal_code"] as? String
        latitude = dictionary["latitude"] as? Double
        longitude = dictionary["longitude"] as? Double
        tax = dictionary["tax"] as? Double
        
        status = dictionary["status"] as? Bool ?? false
        is_storage = dictionary["is_storage"] as? Bool ?? false
        is_transport = dictionary["is_transport"] as? Bool ?? false
        is_maintenance = dictionary["is_maintenance"] as? Bool ?? false
    }
    
    // Encode decode
    required init?(coder aDecoder: NSCoder) {
        
        id    = aDecoder.decodeObject(forKey: "id") as? Int
        admin_id    = aDecoder.decodeObject(forKey: "admin_id") as? Int
        name    = aDecoder.decodeObject(forKey: "name") as? String
        phone    = aDecoder.decodeObject(forKey: "phone") as? String
        license_number    = aDecoder.decodeObject(forKey: "license_number") as? String
        province    = aDecoder.decodeObject(forKey: "province") as? String
        city    = aDecoder.decodeObject(forKey: "city") as? String
        address    = aDecoder.decodeObject(forKey: "address") as? String
        postal_code    = aDecoder.decodeObject(forKey: "postal_code") as? String
        
        latitude    = aDecoder.decodeObject(forKey: "latitude") as? Double
        longitude    = aDecoder.decodeObject(forKey: "longitude") as? Double
        tax    = aDecoder.decodeObject(forKey: "tax") as? Double
        
        status    = aDecoder.decodeBool(forKey: "status")
        is_storage    = aDecoder.decodeBool(forKey: "is_storage")
        is_transport    = aDecoder.decodeBool(forKey: "is_transport")
        is_maintenance    = aDecoder.decodeBool(forKey: "is_maintenance")
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(id, forKey: "id")
        aCoder.encode(admin_id, forKey: "admin_id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(license_number, forKey: "license_number")
        aCoder.encode(province, forKey: "province")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(postal_code, forKey: "postal_code")
        
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(tax, forKey: "tax")
        
        aCoder.encode(status, forKey: "status")
        aCoder.encode(is_storage, forKey: "is_storage")
        aCoder.encode(is_transport, forKey: "is_transport")
        aCoder.encode(is_maintenance, forKey: "is_maintenance")
    }
    
    func isMultipleServiceSelected() -> Bool{
        
        var count = 0
        if is_storage{
            
            count += 1
        }
        
        if is_maintenance{
            
            count += 1
        }
        
        if is_transport {
            
            count += 1
        }
        
        if count == 1 || count == 0{
            
            return false
        }
        
        return true
    }
   
}

