//
//  LocationHoursModel.swift
//  MaiStro-Vendor
//
//  Created by samir on 12/11/19.
//  Copyright © 2019 samir. All rights reserved.
//

import Foundation
import Gloss

public class LocationHoursModel: Glossy {
    public var closeTime : String!
    public var createdAt : String!
    public var createdBy : Int!
    public var day : Int!
    public var endDate : String!
    public var id : Int!
    public var locationId : Int!
    public var open : Bool!
    public var openTime : String!
    public var startDate : String!
    public var updatedAt : String!
    public var updatedBy : Int!
    public var holiday_date : String!

    //MARK: Default Initializer
    init()
    {
        closeTime = ""
        createdAt = ""
        createdBy = 0
        day = 0
        endDate = ""
        id = 0
        locationId = 0
        open = false
        openTime = ""
        startDate = ""
        updatedAt = ""
        updatedBy = 0
        holiday_date = ""

    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        
        if let holiday_date : String = "holiday_date" <~~ json {
            self.holiday_date = holiday_date
        }else{
            self.holiday_date = ""
        }
        
        if let closeTime : String = "close_time" <~~ json {
            self.closeTime = closeTime
        }else{
            self.closeTime = ""
        }
        if let createdAt : String = "created_at" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let createdBy : Int = "created_by" <~~ json {
            self.createdBy = createdBy
        }else{
            self.createdBy = 0
        }
        if let day : Int = "day" <~~ json {
            self.day = day
        }else{
            self.day = 0
        }
        if let endDate : String = "end_date" <~~ json {
            self.endDate = endDate
        }else{
            self.endDate = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let locationId : Int = "location_id" <~~ json {
            self.locationId = locationId
        }else{
            self.locationId = 0
        }
        if let open : Bool = "open" <~~ json {
            self.open = open
        }else{
            self.open = false
        }
        if let openTime : String = "open_time" <~~ json {
            self.openTime = openTime
        }else{
            self.openTime = ""
        }
        if let startDate : String = "start_date" <~~ json {
            self.startDate = startDate
        }else{
            self.startDate = ""
        }
        if let updatedAt : String = "updated_at" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        if let updatedBy : Int = "updated_by" <~~ json {
            self.updatedBy = updatedBy
        }else{
            self.updatedBy = 0
        }
        
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            
            "holiday_date" ~~> holiday_date,
            "close_time" ~~> closeTime,
            "created_at" ~~> createdAt,
            "created_by" ~~> createdBy,
            "day" ~~> day,
            "end_date" ~~> endDate,
            "id" ~~> id,
            "location_id" ~~> locationId,
            "open" ~~> open,
            "open_time" ~~> openTime,
            "start_date" ~~> startDate,
            "updated_at" ~~> updatedAt,
            "updated_by" ~~> updatedBy,
            ])
    }
    
}
