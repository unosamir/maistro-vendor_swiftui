
import UIKit

class SectionModel: NSObject {
    
    var userText : String?
    var sectionType = CellType.none
    var dataArr = [Any]()
    var isShowButton = Bool()
    var buttonTitle : String?
    
    override init() {
    }
    
}


class CellModel: NSObject {
    
    var placeholder : String?
    var userText : String?
    var cellType = CellType.none
    var cellObj : Any?
    var imageName: String?
    var image: UIImage?
    var keyboardType: UIKeyboardType?
    var isSelected : Bool = false
    var isPromocode : Bool = false
    var dataArr = [Any]()
    var openTime : String = DEFAULT_OPEN_TIME
    var closeTime : String = DEFAULT_CLOSE_TIME
    var id : Int?
    var note : String?
    var transporterName : String?
    
    override init() {
    }
    
}
