//
//  ClsDashboardData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 27, 2019

import Foundation 
import Gloss

//MARK: - ClsDashboardData
public class ClsDashboardData: Glossy {
    public var activeOrders : ClsDashboardActiveOrder!
    public var completeToday : Int!
    public var receiveToday : Int!
    public var upcomingThisWeek : [ClsDashboardUpcomingThisWeek]!

	//MARK: Default Initializer 
	init()
	{
        activeOrders = ClsDashboardActiveOrder()
        completeToday = 0
        receiveToday = 0
        upcomingThisWeek = []
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let activeOrders : ClsDashboardActiveOrder = "active_orders" <~~ json {
            self.activeOrders = activeOrders
        }else{
            self.activeOrders = ClsDashboardActiveOrder()
        }
        if let completeToday : Int = "complete_today" <~~ json {
            self.completeToday = completeToday
        }else{
            self.completeToday = 0
        }
        if let receiveToday : Int = "receive_today" <~~ json {
            self.receiveToday = receiveToday
        }else{
            self.receiveToday = 0
        }
        if let upcomingThisWeek : [ClsDashboardUpcomingThisWeek] = "upcoming_this_week" <~~ json {
            self.upcomingThisWeek = upcomingThisWeek
        }else{
            self.upcomingThisWeek = []
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "active_orders" ~~> activeOrders,
        "complete_today" ~~> completeToday,
        "receive_today" ~~> receiveToday,
        "upcoming_this_week" ~~> upcomingThisWeek,
		])
	}

}
