//
//  ClsDashboardActiveOrder.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on November 27, 2019

import Foundation
import Gloss

//MARK: - ClsDashboardActiveOrder
public class ClsDashboardActiveOrder: Glossy {
    public var accepted : Int!
    public var cancelled : Int!
    public var completed : Int!
    public var inProgress : Int!
    public var pendingCompleteConfirmation : Int!
    public var submitted : Int!
    public var received : Int!

    //MARK: Default Initializer
    init()
    {
        accepted = 0
        cancelled = 0
        completed = 0
        inProgress = 0
        pendingCompleteConfirmation = 0
        submitted = 0
        received = 0
    }


    //MARK: Decodable
    public required init?(json: JSON){
        if let accepted : Int = "accepted" <~~ json {
            self.accepted = accepted
        }else{
            self.accepted = 0
        }
        if let cancelled : Int = "cancelled" <~~ json {
            self.cancelled = cancelled
        }else{
            self.cancelled = 0
        }
        if let completed : Int = "completed" <~~ json {
            self.completed = completed
        }else{
            self.completed = 0
        }
        if let inProgress : Int = "in_progress" <~~ json {
            self.inProgress = inProgress
        }else{
            self.inProgress = 0
        }
        if let pendingCompleteConfirmation : Int = "pending_complete_confirmation" <~~ json {
            self.pendingCompleteConfirmation = pendingCompleteConfirmation
        }else{
            self.pendingCompleteConfirmation = 0
        }
        if let submitted : Int = "submitted" <~~ json {
            self.submitted = submitted
        }else{
            self.submitted = 0
        }
        if let received : Int = "received" <~~ json {
            self.received = received
        }else{
            self.received = 0
        }
        
    }


    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
        "received" ~~> received,
        "accepted" ~~> accepted,
        "cancelled" ~~> cancelled,
        "completed" ~~> completed,
        "in_progress" ~~> inProgress,
        "pending_complete_confirmation" ~~> pendingCompleteConfirmation,
        "submitted" ~~> submitted,
        ])
    }

}
