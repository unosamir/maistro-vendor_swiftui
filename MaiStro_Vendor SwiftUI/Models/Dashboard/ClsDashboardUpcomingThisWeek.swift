//
//  ClsDashboardUpcomingThisWeek.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 27, 2019

import Foundation 
import Gloss

//MARK: - ClsDashboardUpcomingThisWeek
public class ClsDashboardUpcomingThisWeek: Glossy {
    public var complete : Int!
    public var date : String!
    public var receive : Int!

	//MARK: Default Initializer 
	init()
	{
        complete = 0
        date = ""
        receive = 0
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let complete : Int = "complete" <~~ json {
            self.complete = complete
        }else{
            self.complete = 0
        }
        if let date : String = "date" <~~ json {
            self.date = date
        }else{
            self.date = ""
        }
        if let receive : Int = "receive" <~~ json {
            self.receive = receive
        }else{
            self.receive = 0
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "complete" ~~> complete,
        "date" ~~> date,
        "receive" ~~> receive,
		])
	}

}
