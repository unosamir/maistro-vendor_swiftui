//
//  ClsThreadData.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 20, 2019

import Foundation 
import Gloss

//MARK: - ClsThreadData
public class ClsThreadData: Glossy {
    public var id : Int!
    public var location : ClsThreadLocation!
    public var locationId : Int!
    public var messages : [ClsThreadMessage]!
    public var user : ClsThreadUser!
    public var userId : Int!

	//MARK: Default Initializer 
	init()
	{
        id = 0
        location = ClsThreadLocation()
        locationId = 0
        messages = []
        user = ClsThreadUser()
        userId = 0
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let location : ClsThreadLocation = "location" <~~ json {
            self.location = location
        }else{
            self.location = ClsThreadLocation()
        }
        if let locationId : Int = "location_id" <~~ json {
            self.locationId = locationId
        }else{
            self.locationId = 0
        }
        if let messages : [ClsThreadMessage] = "messages" <~~ json {
            self.messages = messages
        }else{
            self.messages = []
        }
        if let user : ClsThreadUser = "user" <~~ json {
            self.user = user
        }else{
            self.user = ClsThreadUser()
        }
        if let userId : Int = "user_id" <~~ json {
            self.userId = userId
        }else{
            self.userId = 0
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "id" ~~> id,
        "location" ~~> location,
        "location_id" ~~> locationId,
        "messages" ~~> messages,
        "user" ~~> user,
        "user_id" ~~> userId,
		])
	}

}
