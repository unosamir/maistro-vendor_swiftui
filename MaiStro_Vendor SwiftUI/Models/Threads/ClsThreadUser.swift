//
//  ClsThreadUser.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 20, 2019

import Foundation 
import Gloss

//MARK: - ClsThreadUser
public class ClsThreadUser: Glossy {
    public var active : Bool!
    public var applicationId : AnyObject!
    public var createdAt : String!
    public var deletedAt : AnyObject!
    public var email : String!
    public var emailVerificationStatus : Bool!
    public var firstName : String!
    public var id : Int!
    public var lastName : String!
    public var phone : AnyObject!
    public var status : String!
    public var updatedAt : String!

	//MARK: Default Initializer 
	init()
	{
        active = false
        applicationId = nil
                createdAt = ""
        deletedAt = nil
                email = ""
        emailVerificationStatus = false
        firstName = ""
        id = 0
        lastName = ""
        phone = nil
                status = ""
        updatedAt = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let active : Bool = "active" <~~ json {
            self.active = active
        }else{
            self.active = false
        }
        if let applicationId : AnyObject = "application_id" <~~ json {
            self.applicationId = applicationId
        }else{
        }
        if let createdAt : String = "createdAt" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let deletedAt : AnyObject = "deleted_at" <~~ json {
            self.deletedAt = deletedAt
        }else{
        }
        if let email : String = "email" <~~ json {
            self.email = email
        }else{
            self.email = ""
        }
        if let emailVerificationStatus : Bool = "email_verification_status" <~~ json {
            self.emailVerificationStatus = emailVerificationStatus
        }else{
            self.emailVerificationStatus = false
        }
        if let firstName : String = "first_name" <~~ json {
            self.firstName = firstName
        }else{
            self.firstName = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let lastName : String = "last_name" <~~ json {
            self.lastName = lastName
        }else{
            self.lastName = ""
        }
        if let phone : AnyObject = "phone" <~~ json {
            self.phone = phone
        }else{
        }
        if let status : String = "status" <~~ json {
            self.status = status
        }else{
            self.status = ""
        }
        if let updatedAt : String = "updatedAt" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "active" ~~> active,
        "application_id" ~~> applicationId,
        "createdAt" ~~> createdAt,
        "deleted_at" ~~> deletedAt,
        "email" ~~> email,
        "email_verification_status" ~~> emailVerificationStatus,
        "first_name" ~~> firstName,
        "id" ~~> id,
        "last_name" ~~> lastName,
        "phone" ~~> phone,
        "status" ~~> status,
        "updatedAt" ~~> updatedAt,
		])
	}

}
