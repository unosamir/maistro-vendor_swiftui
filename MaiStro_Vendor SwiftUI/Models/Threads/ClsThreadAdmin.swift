//
//  ClsThreadAdmin.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 20, 2019

import Foundation 
import Gloss

//MARK: - ClsThreadAdmin
public class ClsThreadAdmin: Glossy {
    public var createdAt : String!
    public var email : String!
    public var emailVerificationStatus : Bool!
    public var firstName : String!
    public var id : Int!
    public var lastName : String!
    public var marketingOptIn : AnyObject!
    public var phone : AnyObject!
    public var updatedAt : String!

	//MARK: Default Initializer 
	init()
	{
        createdAt = ""
        email = ""
        emailVerificationStatus = false
        firstName = ""
        id = 0
        lastName = ""
        marketingOptIn = nil
                phone = nil
                updatedAt = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let createdAt : String = "createdAt" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let email : String = "email" <~~ json {
            self.email = email
        }else{
            self.email = ""
        }
        if let emailVerificationStatus : Bool = "email_verification_status" <~~ json {
            self.emailVerificationStatus = emailVerificationStatus
        }else{
            self.emailVerificationStatus = false
        }
        if let firstName : String = "first_name" <~~ json {
            self.firstName = firstName
        }else{
            self.firstName = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let lastName : String = "last_name" <~~ json {
            self.lastName = lastName
        }else{
            self.lastName = ""
        }
        if let marketingOptIn : AnyObject = "marketing_opt_in" <~~ json {
            self.marketingOptIn = marketingOptIn
        }else{
        }
        if let phone : AnyObject = "phone" <~~ json {
            self.phone = phone
        }else{
        }
        if let updatedAt : String = "updatedAt" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "createdAt" ~~> createdAt,
        "email" ~~> email,
        "email_verification_status" ~~> emailVerificationStatus,
        "first_name" ~~> firstName,
        "id" ~~> id,
        "last_name" ~~> lastName,
        "marketing_opt_in" ~~> marketingOptIn,
        "phone" ~~> phone,
        "updatedAt" ~~> updatedAt,
		])
	}

}
