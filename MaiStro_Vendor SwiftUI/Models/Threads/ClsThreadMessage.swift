//
//  ClsThreadMessage.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 20, 2019

import Foundation 
import Gloss

//MARK: - ClsThreadMessage
public class ClsThreadMessage: Glossy {
    public var createdAt : String!
    public var id : Int!
    public var message : String!
    public var messageType : String!
    public var senderId : Int!
    public var senderType : String!
    public var threadId : Int!
    public var updatedAt : String!

	//MARK: Default Initializer 
	init()
	{
        createdAt = ""
        id = 0
        message = ""
        messageType = ""
        senderId = 0
        senderType = ""
        threadId = 0
        updatedAt = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let createdAt : String = "created_at" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let message : String = "message" <~~ json {
            self.message = message
        }else{
            self.message = ""
        }
        if let messageType : String = "message_type" <~~ json {
            self.messageType = messageType
        }else{
            self.messageType = ""
        }
        if let senderId : Int = "sender_id" <~~ json {
            self.senderId = senderId
        }else{
            self.senderId = 0
        }
        if let senderType : String = "sender_type" <~~ json {
            self.senderType = senderType
        }else{
            self.senderType = ""
        }
        if let threadId : Int = "thread_id" <~~ json {
            self.threadId = threadId
        }else{
            self.threadId = 0
        }
        if let updatedAt : String = "updated_at" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "created_at" ~~> createdAt,
        "id" ~~> id,
        "message" ~~> message,
        "message_type" ~~> messageType,
        "sender_id" ~~> senderId,
        "sender_type" ~~> senderType,
        "thread_id" ~~> threadId,
        "updated_at" ~~> updatedAt,
		])
	}

}
