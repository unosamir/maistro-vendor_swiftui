//
//  ClsThreadLocation.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 20, 2019

import Foundation 
import Gloss

//MARK: - ClsThreadLocation
public class ClsThreadLocation: Glossy {
    public var address : String!
    public var admin : ClsThreadAdmin!
    public var adminId : Int!
    public var city : String!
    public var createdAt : String!
    public var id : Int!
    public var isMaintenance : Bool!
    public var isStorage : Bool!
    public var isTransport : Bool!
    public var latitude : Float!
    public var licenseNumber : String!
    public var longitude : Float!
    public var name : String!
    public var phone : String!
    public var postalCode : String!
    public var province : String!
    public var tax : Int!
    public var updatedAt : String!

	//MARK: Default Initializer 
	init()
	{
        address = ""
        admin = ClsThreadAdmin()
        adminId = 0
        city = ""
        createdAt = ""
        id = 0
        isMaintenance = false
        isStorage = false
        isTransport = false
        latitude = 0.0
        licenseNumber = ""
        longitude = 0.0
        name = ""
        phone = ""
        postalCode = ""
        province = ""
        tax = 0
        updatedAt = ""
    }


	//MARK: Decodable
	public required init?(json: JSON){
        if let address : String = "address" <~~ json {
            self.address = address
        }else{
            self.address = ""
        }
        if let admin : ClsThreadAdmin = "admin" <~~ json {
            self.admin = admin
        }else{
            self.admin = ClsThreadAdmin()
        }
        if let adminId : Int = "admin_id" <~~ json {
            self.adminId = adminId
        }else{
            self.adminId = 0
        }
        if let city : String = "city" <~~ json {
            self.city = city
        }else{
            self.city = ""
        }
        if let createdAt : String = "createdAt" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let isMaintenance : Bool = "is_maintenance" <~~ json {
            self.isMaintenance = isMaintenance
        }else{
            self.isMaintenance = false
        }
        if let isStorage : Bool = "is_storage" <~~ json {
            self.isStorage = isStorage
        }else{
            self.isStorage = false
        }
        if let isTransport : Bool = "is_transport" <~~ json {
            self.isTransport = isTransport
        }else{
            self.isTransport = false
        }
        if let latitude : Float = "latitude" <~~ json {
            self.latitude = latitude
        }else{
            self.latitude = 0.0
        }
        if let licenseNumber : String = "license_number" <~~ json {
            self.licenseNumber = licenseNumber
        }else{
            self.licenseNumber = ""
        }
        if let longitude : Float = "longitude" <~~ json {
            self.longitude = longitude
        }else{
            self.longitude = 0.0
        }
        if let name : String = "name" <~~ json {
            self.name = name
        }else{
            self.name = ""
        }
        if let phone : String = "phone" <~~ json {
            self.phone = phone
        }else{
            self.phone = ""
        }
        if let postalCode : String = "postal_code" <~~ json {
            self.postalCode = postalCode
        }else{
            self.postalCode = ""
        }
        if let province : String = "province" <~~ json {
            self.province = province
        }else{
            self.province = ""
        }
        if let tax : Int = "tax" <~~ json {
            self.tax = tax
        }else{
            self.tax = 0
        }
        if let updatedAt : String = "updatedAt" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        
	}


	//MARK: Encodable
	public func toJSON() -> JSON? {
		return jsonify([
        "address" ~~> address,
        "admin" ~~> admin,
        "admin_id" ~~> adminId,
        "city" ~~> city,
        "createdAt" ~~> createdAt,
        "id" ~~> id,
        "is_maintenance" ~~> isMaintenance,
        "is_storage" ~~> isStorage,
        "is_transport" ~~> isTransport,
        "latitude" ~~> latitude,
        "license_number" ~~> licenseNumber,
        "longitude" ~~> longitude,
        "name" ~~> name,
        "phone" ~~> phone,
        "postal_code" ~~> postalCode,
        "province" ~~> province,
        "tax" ~~> tax,
        "updatedAt" ~~> updatedAt,
		])
	}

}
