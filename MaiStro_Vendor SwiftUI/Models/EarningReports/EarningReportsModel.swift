//
//  EarningReportsModel.swift
//  MaiStro-Vendor
//
//  Created by samir on 02/12/19.
//  Copyright © 2019 samir. All rights reserved.
//

import UIKit

class EarningReportsModel: NSObject {
    
    public var id: Int?
    public var user_id: Int?
    public var status: String?
    public var updatedAt: String?
    public var createdAt: String?
    public var total: Double = 0.0
    public var total_promocode: Double = 0.0
    
    override init() {
        
    }
    
    required public init?(dictionary: [String:Any]) {
        
        id = dictionary["id"] as? Int
        user_id = dictionary["user_id"] as? Int
        status = dictionary["status"] as? String
        updatedAt = dictionary["updatedAt"] as? String
        createdAt = dictionary["createdAt"] as? String
//        total_promocode = dictionary["total_promocode"] as? Double ?? 0.0
        
        total = 0.0
    
        //calculate total amount with tax
        if let arrOrderItems = dictionary["order_items"] as? [[String:Any]]{
            
            for dict in arrOrderItems{
                
                //storage
                if let storage = dict["order_item_storage"] as? [String:Any],let location_id = storage["location_id"] as? Int, location_id == userBusinessLocationModel.id, let total = storage["total"] as? Double,let tax = storage["tax"] as? Double{
                    
                    self.total += total
                    self.total += tax
                    
                    //promo
                    if let promoDict = storage["order_item_storage_promocode"] as? [String:Any],let applyValue = promoDict["applied_value"] as? Double{
                        
                        self.total_promocode += applyValue
                    }
                }
                
                //maintenance
                if let arrMaintenance = dict["order_item_maintenances"] as? [[String:Any]]{
                    
                    for dictMM in arrMaintenance{
                        
                        if let location_id = dictMM["location_id"] as? Int, location_id == userBusinessLocationModel.id, let total = dictMM["total"] as? Double,let tax = dictMM["tax"] as? Double{
                            
                            self.total += total
                            self.total += tax
                            
                            //promo
                            if let promoDict = dictMM["order_item_maintenance_promocode"] as? [String:Any],let applyValue = promoDict["applied_value"] as? Double{
                                
                                self.total_promocode += applyValue
                            }
                        }
                    }
                }
                
                //transport
                if let arrTransport = dict["order_item_transportations"] as? [[String:Any]]{
                    
                    for dictMM in arrTransport{
                        
                        if let location_id = dictMM["location_id"] as? Int, location_id == userBusinessLocationModel.id, let total = dictMM["total"] as? Double,let tax = dictMM["tax"] as? Double{
                            
                            self.total += total
                            self.total += tax
                            
                            //promo
                            if let promoDict = dictMM["order_item_transportation_promocode"] as? [String:Any],let applyValue = promoDict["applied_value"] as? Double{
                                
                                self.total_promocode += applyValue
                            }
                        }
                    }
                }
                
            }
        }
    }
}
