//
//  OrderItemTransportModel.swift
//  MaiStro-Vendor
//
//  Created by samir on 07/11/19.
//  Copyright © 2019 samir. All rights reserved.
//

import Foundation
import Gloss

public class OrderItemTransportModel: Glossy {
    public var createdAt : String!
    public var dropOffCity : String!
    public var dropOffLat : Double!
    public var dropOffLng : Double!
    public var dropOffNote : String!
    public var dropOffPostalCode : String!
    public var dropOffState : String!
    public var dropOffStreet1 : String!
    public var dropOffStreet2 : String!
    public var id : Int!
    public var locationId : Int!
    public var note : AnyObject!
    public var orderItemId : Int!
    public var pickupCity : String!
    public var pickupLat : Double!
    public var pickupLng : Double!
    public var pickupNote : String!
    public var pickupPostalCode : String!
    public var pickupState : String!
    public var pickupStreet1 : String!
    public var pickupStreet2 : String!
    public var rate : Double!
    public var status : String!
    public var tax : Double!
    public var total : Double!
    public var totalDistance : Double!
    public var transportType : String!
    public var transportationPickup : String!
    public var transportationQuotedPickup : String!
    public var updatedAt : String!
    public var location : ClsLocation!
    
    public var order_item_transportation_promocode : OrderPromocodeModel?
    
    //MARK: Default Initializer
    init()
    {
        createdAt = ""
        dropOffCity = ""
        dropOffLat = 0.0
        dropOffLng = 0.0
        dropOffNote = ""
        dropOffPostalCode = ""
        dropOffState = ""
        dropOffStreet1 = ""
        dropOffStreet2 = ""
        id = 0
        locationId = 0
        note = nil
        orderItemId = 0
        pickupCity = ""
        pickupLat = 0.0
        pickupLng = 0.0
        pickupNote = ""
        pickupPostalCode = ""
        pickupState = ""
        pickupStreet1 = ""
        pickupStreet2 = ""
        rate = 0.0
        status = ""
        tax = 0.0
        total = 0.0
        totalDistance = 0.0
        transportType = ""
        transportationPickup = ""
        transportationQuotedPickup = ""
        updatedAt = ""
        location = ClsLocation()
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let createdAt : String = "createdAt" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let dropOffCity : String = "drop_off_city" <~~ json {
            self.dropOffCity = dropOffCity
        }else{
            self.dropOffCity = ""
        }
        if let dropOffLat : Double = "drop_off_lat" <~~ json {
            self.dropOffLat = dropOffLat
        }else{
            self.dropOffLat = 0.0
        }
        if let dropOffLng : Double = "drop_off_lng" <~~ json {
            self.dropOffLng = dropOffLng
        }else{
            self.dropOffLng = 0.0
        }
        if let dropOffNote : String = "drop_off_note" <~~ json {
            self.dropOffNote = dropOffNote
        }else{
            self.dropOffNote = ""
        }
        if let dropOffPostalCode : String = "drop_off_postal_code" <~~ json {
            self.dropOffPostalCode = dropOffPostalCode
        }else{
            self.dropOffPostalCode = ""
        }
        if let dropOffState : String = "drop_off_state" <~~ json {
            self.dropOffState = dropOffState
        }else{
            self.dropOffState = ""
        }
        if let dropOffStreet1 : String = "drop_off_street1" <~~ json {
            self.dropOffStreet1 = dropOffStreet1
        }else{
            self.dropOffStreet1 = ""
        }
        if let dropOffStreet2 : String = "drop_off_street2" <~~ json {
            self.dropOffStreet2 = dropOffStreet2
        }else{
            self.dropOffStreet2 = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let locationId : Int = "location_id" <~~ json {
            self.locationId = locationId
        }else{
            self.locationId = 0
        }
        if let note : AnyObject = "note" <~~ json {
            self.note = note
        }else{
        }
        if let orderItemId : Int = "order_item_id" <~~ json {
            self.orderItemId = orderItemId
        }else{
            self.orderItemId = 0
        }
        if let pickupCity : String = "pickup_city" <~~ json {
            self.pickupCity = pickupCity
        }else{
            self.pickupCity = ""
        }
        if let pickupLat : Double = "pickup_lat" <~~ json {
            self.pickupLat = pickupLat
        }else{
            self.pickupLat = 0.0
        }
        if let pickupLng : Double = "pickup_lng" <~~ json {
            self.pickupLng = pickupLng
        }else{
            self.pickupLng = 0.0
        }
        if let pickupNote : String = "pickup_note" <~~ json {
            self.pickupNote = pickupNote
        }else{
            self.pickupNote = ""
        }
        if let pickupPostalCode : String = "pickup_postal_code" <~~ json {
            self.pickupPostalCode = pickupPostalCode
        }else{
            self.pickupPostalCode = ""
        }
        if let pickupState : String = "pickup_state" <~~ json {
            self.pickupState = pickupState
        }else{
            self.pickupState = ""
        }
        if let pickupStreet1 : String = "pickup_street1" <~~ json {
            self.pickupStreet1 = pickupStreet1
        }else{
            self.pickupStreet1 = ""
        }
        if let pickupStreet2 : String = "pickup_street2" <~~ json {
            self.pickupStreet2 = pickupStreet2
        }else{
            self.pickupStreet2 = ""
        }
        if let rate : Double = "rate" <~~ json {
            self.rate = rate
            self.rate = self.rate.rounded(toPlaces: 2)
        }else{
            self.rate = 0.0
        }
        if let status : String = "status" <~~ json {
            self.status = status
        }else{
            self.status = ""
        }
        if let tax : Double = "tax" <~~ json {
            self.tax = tax
            self.tax = self.tax.rounded(toPlaces: 2)
        }else{
            self.tax = 0.0
        }
        if let total : Double = "total" <~~ json {
            self.total = total
            self.total = self.total.rounded(toPlaces: 2)
        }else{
            self.total = 0.0
        }
        if let totalDistance : Double = "total_distance" <~~ json {
            self.totalDistance = totalDistance
        }else{
            self.totalDistance = 0.0
        }
        if let transportType : String = "transport_type" <~~ json {
            self.transportType = transportType
        }else{
            self.transportType = ""
        }
        if let transportationPickup : String = "transportation_pickup" <~~ json {
            self.transportationPickup = transportationPickup
        }else{
            self.transportationPickup = ""
        }
        if let transportationQuotedPickup : String = "transportation_quoted_pickup" <~~ json {
            self.transportationQuotedPickup = transportationQuotedPickup
        }else{
            self.transportationQuotedPickup = ""
        }
        if let updatedAt : String = "updatedAt" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        if let location : ClsLocation = "location" <~~ json {
            self.location = location
        }else{
            self.location = ClsLocation()
        }
        
        //transport promo
          if let order_item_transportation_promocode = json["order_item_transportation_promocode"] as? [String:Any]{
              
              self.order_item_transportation_promocode = OrderPromocodeModel.init(json: order_item_transportation_promocode)
          }
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "createdAt" ~~> createdAt,
            "drop_off_city" ~~> dropOffCity,
            "drop_off_lat" ~~> dropOffLat,
            "drop_off_lng" ~~> dropOffLng,
            "drop_off_note" ~~> dropOffNote,
            "drop_off_postal_code" ~~> dropOffPostalCode,
            "drop_off_state" ~~> dropOffState,
            "drop_off_street1" ~~> dropOffStreet1,
            "drop_off_street2" ~~> dropOffStreet2,
            "id" ~~> id,
            "location_id" ~~> locationId,
            "location" ~~> location,
            "note" ~~> note,
            "order_item_id" ~~> orderItemId,
            "pickup_city" ~~> pickupCity,
            "pickup_lat" ~~> pickupLat,
            "pickup_lng" ~~> pickupLng,
            "pickup_note" ~~> pickupNote,
            "pickup_postal_code" ~~> pickupPostalCode,
            "pickup_state" ~~> pickupState,
            "pickup_street1" ~~> pickupStreet1,
            "pickup_street2" ~~> pickupStreet2,
            "rate" ~~> rate,
            "status" ~~> status,
            "tax" ~~> tax,
            "total" ~~> total,
            "total_distance" ~~> totalDistance,
            "transport_type" ~~> transportType,
            "transportation_pickup" ~~> transportationPickup,
            "transportation_quoted_pickup" ~~> transportationQuotedPickup,
            "updatedAt" ~~> updatedAt,
            ])
    }
    
}


//MARK: - ClsLocation
public class ClsLocation: Glossy {
    public var address : String!
    public var adminId : Int!
    public var city : String!
    public var createdAt : String!
    public var id : Int!
    public var isMaintenance : Bool!
    public var isStorage : Bool!
    public var isTransport : Bool!
    public var latitude : Float!
    public var licenseNumber : String!
    public var longitude : Float!
    public var name : String!
    public var phone : String!
    public var postalCode : String!
    public var province : String!
    public var tax : Int!
    public var updatedAt : String!

    //MARK: Default Initializer
    init()
    {
        address = ""
        adminId = 0
        city = ""
        createdAt = ""
        id = 0
        isMaintenance = false
        isStorage = false
        isTransport = false
        latitude = 0.0
        licenseNumber = ""
        longitude = 0.0
        name = ""
        phone = ""
        postalCode = ""
        province = ""
        tax = 0
        updatedAt = ""
    }


    //MARK: Decodable
    public required init?(json: JSON){
        if let address : String = "address" <~~ json {
            self.address = address
        }else{
            self.address = ""
        }
        if let adminId : Int = "admin_id" <~~ json {
            self.adminId = adminId
        }else{
            self.adminId = 0
        }
        if let city : String = "city" <~~ json {
            self.city = city
        }else{
            self.city = ""
        }
        if let createdAt : String = "createdAt" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let isMaintenance : Bool = "is_maintenance" <~~ json {
            self.isMaintenance = isMaintenance
        }else{
            self.isMaintenance = false
        }
        if let isStorage : Bool = "is_storage" <~~ json {
            self.isStorage = isStorage
        }else{
            self.isStorage = false
        }
        if let isTransport : Bool = "is_transport" <~~ json {
            self.isTransport = isTransport
        }else{
            self.isTransport = false
        }
        if let latitude : Float = "latitude" <~~ json {
            self.latitude = latitude
        }else{
            self.latitude = 0.0
        }
        if let licenseNumber : String = "license_number" <~~ json {
            self.licenseNumber = licenseNumber
        }else{
            self.licenseNumber = ""
        }
        if let longitude : Float = "longitude" <~~ json {
            self.longitude = longitude
        }else{
            self.longitude = 0.0
        }
        if let name : String = "name" <~~ json {
            self.name = name
        }else{
            self.name = ""
        }
        if let phone : String = "phone" <~~ json {
            self.phone = phone
        }else{
            self.phone = ""
        }
        if let postalCode : String = "postal_code" <~~ json {
            self.postalCode = postalCode
        }else{
            self.postalCode = ""
        }
        if let province : String = "province" <~~ json {
            self.province = province
        }else{
            self.province = ""
        }
        if let tax : Int = "tax" <~~ json {
            self.tax = tax
        }else{
            self.tax = 0
        }
        if let updatedAt : String = "updatedAt" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        
    }


    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
        "address" ~~> address,
        "admin_id" ~~> adminId,
        "city" ~~> city,
        "createdAt" ~~> createdAt,
        "id" ~~> id,
        "is_maintenance" ~~> isMaintenance,
        "is_storage" ~~> isStorage,
        "is_transport" ~~> isTransport,
        "latitude" ~~> latitude,
        "license_number" ~~> licenseNumber,
        "longitude" ~~> longitude,
        "name" ~~> name,
        "phone" ~~> phone,
        "postal_code" ~~> postalCode,
        "province" ~~> province,
        "tax" ~~> tax,
        "updatedAt" ~~> updatedAt,
        ])
    }

}
