//
//  OrderItemMaintenanceModel.swift
//  MaiStro-Vendor
//
//  Created by samir on 07/11/19.
//  Copyright © 2019 samir. All rights reserved.
//

import Foundation
import Gloss

public class OrderItemMaintenanceModel: Glossy {
    
    public var createdAt : String!
    public var id : Int!
    public var locationId : Int!
    public var maintenanceQuotedTime : String!
    public var maintenanceStart : String!
    public var note : String!
    public var orderItemId : Int!
    public var rate : Double!
    public var status : String!
    public var tax : Double!
    public var total : Double!
    public var updatedAt : String!
    public var title : String!
    public var maintenance_service_id : Int!
    
    public var order_item_maintenance_promocode : OrderPromocodeModel?
    
    //MARK: Default Initializer
    init()
    {
        createdAt = ""
        id = 0
        locationId = 0
        maintenanceQuotedTime = ""
        maintenanceStart = ""
        note = ""
        orderItemId = 0
        rate = 0.0
        status = ""
        tax = 0.0
        total = 0.0
        updatedAt = ""
        title = ""
        maintenance_service_id = 0
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let createdAt : String = "createdAt" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let locationId : Int = "location_id" <~~ json {
            self.locationId = locationId
        }else{
            self.locationId = 0
        }
        if let maintenanceQuotedTime : String = "maintenance_quoted_time" <~~ json {
            self.maintenanceQuotedTime = maintenanceQuotedTime
        }else{
            self.maintenanceQuotedTime = ""
        }
        if let maintenanceStart : String = "maintenance_start" <~~ json {
            self.maintenanceStart = maintenanceStart
        }else{
            self.maintenanceStart = ""
        }
        if let note : String = "note" <~~ json {
            self.note = note
        }else{
            self.note = ""
        }
        if let orderItemId : Int = "order_item_id" <~~ json {
            self.orderItemId = orderItemId
        }else{
            self.orderItemId = 0
        }
        if let rate : Double = "rate" <~~ json {
            self.rate = rate
            self.rate = self.rate.rounded(toPlaces: 2)
        }else{
            self.rate = 0.0
        }
        if let status : String = "status" <~~ json {
            self.status = status
        }else{
            self.status = ""
        }
        if let tax : Double = "tax" <~~ json {
            self.tax = tax
            self.tax = self.tax.rounded(toPlaces: 2)
        }else{
            self.tax = 0.0
        }
        if let total : Double = "total" <~~ json {
            self.total = total
            self.total = self.total.rounded(toPlaces: 2)
        }else{
            self.total = 0.0
        }
        if let updatedAt : String = "updatedAt" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        if let maintenance_service_id : Int = "maintenance_service_id" <~~ json {
            self.maintenance_service_id = maintenance_service_id
        }else{
            self.maintenance_service_id = 0
        }
        
        if let objService = json["maintenance_service"] as? [String:Any],let title = objService["title"] as? String{
            
            self.title = title
        }
        
        //maintenance promo
       if let order_item_maintenance_promocode = json["order_item_maintenance_promocode"] as? [String:Any]{
           
           self.order_item_maintenance_promocode = OrderPromocodeModel.init(json: order_item_maintenance_promocode)
       }
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "createdAt" ~~> createdAt,
            "id" ~~> id,
            "location_id" ~~> locationId,
            "maintenance_quoted_time" ~~> maintenanceQuotedTime,
            "maintenance_start" ~~> maintenanceStart,
            "note" ~~> note,
            "order_item_id" ~~> orderItemId,
            "rate" ~~> rate,
            "status" ~~> status,
            "tax" ~~> tax,
            "total" ~~> total,
            "updatedAt" ~~> updatedAt,
            ])
    }
}
