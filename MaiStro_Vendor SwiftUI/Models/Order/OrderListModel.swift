//
//  OrderListModel.swift
//  MaiStro-Vendor
//
//  Created by samir on 07/11/19.
//  Copyright © 2019 samir. All rights reserved.
//

import Foundation
import Gloss

class OrderListModel: NSObject {

    public var id: Int?
    public var order_id: Int?
    public var item_id: Int?
    public var item_name: String?
    public var item_description: String?
    public var item_status: String?
    public var createdAt: String?
    public var updatedAt: String?

    public var order_item_storage: OrderItemStorageModel?
    public var order_item_maintenances: [OrderItemMaintenanceModel]?
    public var order_item_transportations: [OrderItemTransportModel]?
    public var order_item_images: [OrderItemImageModel]?
    public var user_order: OrderUserModel?
    
    override init() {
        
    }
    
    required public init?(dictionary: [String:Any]) {
        
        id = dictionary["id"] as? Int
        order_id = dictionary["order_id"] as? Int
        item_id = dictionary["item_id"] as? Int
        item_status = dictionary["item_status"] as? String
        createdAt = dictionary["createdAt"] as? String
        updatedAt = dictionary["updatedAt"] as? String
        item_status = item_status?.capitalized
        
        if let objItem = dictionary["item"] as? [String:Any],let itemName = objItem["name"] as? String{
            
            item_name = itemName
        }
        
        if let description = dictionary["note"] as? String{
            item_description = description
        }
        
        //storage
        if let order_item_storage = dictionary["order_item_storage"] as? [String:Any]{
            
            self.order_item_storage = OrderItemStorageModel.init(json: order_item_storage)
        }
        
        //maintenance
        if let arr_order_item_maintenances = dictionary["order_item_maintenances"] as? [[String:Any]]{
            
            self.order_item_maintenances = [OrderItemMaintenanceModel].from(jsonArray: arr_order_item_maintenances)
        }
        
        //transport
        if let arr_order_item_transportations = dictionary["order_item_transportations"] as? [[String:Any]]{
            
            self.order_item_transportations = [OrderItemTransportModel].from(jsonArray: arr_order_item_transportations)
        }
        
        //user
        if let dictUser = dictionary["user_order"] as? [String:Any]{
            
            if let user = dictUser["user"] as? [String:Any]{
                
                var newDict = dictUser
                newDict.merge(user){(current, _) in current}
                self.user_order = OrderUserModel.init(json: newDict)
            }
        }
        
        //images
        if let arr_order_item_images = dictionary["order_item_images"] as? [[String:Any]]{
            
            self.order_item_images = [OrderItemImageModel].from(jsonArray: arr_order_item_images)
        }
    }
    
    
    //function
    static func getOrderStatus(model:OrderListModel,orderStatus:OrderStatus) -> String?{
        
        //status
        var currentStatus = ""
        if orderStatus == .Pending{
            
            currentStatus = AppConstants.OrderStatus_APIText.SUBMITTED
        }
        else if orderStatus == .Approved{
            
            currentStatus = AppConstants.OrderStatus_APIText.ACCEPTED
        }
        else if orderStatus == .Received{
            
            currentStatus = AppConstants.OrderStatus_APIText.RECEIVED
        }
        else if orderStatus == .Complete{
            
            currentStatus = AppConstants.OrderStatus_APIText.COMPLETED
        }
        
        //maintenance
        var arrSameIDMaintenance = [OrderItemMaintenanceModel]()
        if let arrData = model.order_item_maintenances, arrData.count > 0{
            
            arrSameIDMaintenance = arrData.filter {$0.locationId == userBusinessLocationModel.id && $0.status == currentStatus}
        }
        
        //storage
        var storageService : OrderItemStorageModel?
        if let modelStorage = model.order_item_storage,modelStorage.locationId == userBusinessLocationModel.id, modelStorage.status == currentStatus{
            
            storageService = modelStorage
        }
        
        //transport
        var arrSameIDTransport = [OrderItemTransportModel]()
        if let arrData = model.order_item_transportations, arrData.count > 0{
            
            arrSameIDTransport = arrData.filter {$0.locationId == userBusinessLocationModel.id && $0.status == currentStatus}
        }
        
        //Check For Complete Tab
        if orderStatus == .Complete{
            
            //check in maintenance
            if let checkCompleteMaintenance = model.order_item_maintenances?.filter({$0.locationId == userBusinessLocationModel.id && $0.status == AppConstants.OrderStatus_APIText.PENDING_CONFIRMATION}),checkCompleteMaintenance.count > 0{
                
                return "Pending Confirmation"
            }
           
            //check in storage
            if let model = model.order_item_storage,model.locationId == userBusinessLocationModel.id, model.status == AppConstants.OrderStatus_APIText.PENDING_CONFIRMATION{
                
                return "Pending Confirmation"
            }
            
            //check in transport
            if let checkCompleteTransport = model.order_item_transportations?.filter({$0.locationId == userBusinessLocationModel.id && $0.status == AppConstants.OrderStatus_APIText.PENDING_CONFIRMATION}),checkCompleteTransport.count > 0{
                
                return "Pending Confirmation"
            }
       
            return nil
        }
        
        //******** Below logic is for PENDING - APPROVED - RECEIVED status order
        
        //maintenance date
        var maintenance_start : Date?
        if arrSameIDMaintenance.count > 0{
            
            var arrTempDate = [Date]()
            for model in arrSameIDMaintenance{
                
                if let dt = DateFunctions.date(from: model.maintenanceStart, format: DateFunctions.DateFormat.ISOFormat, isCurrentTimeZone: false){
                    
                    arrTempDate.append(dt)
                }
                
            }
            
            //lowest date
            if let earliest = arrTempDate.min() {
                
                maintenance_start = earliest
            }
        }
        
        //transport date
        var transportation_pickup : Date?
        if arrSameIDTransport.count > 0{
            
            var arrTempDate = [Date]()
            for model in arrSameIDTransport{
                
                if let dt = DateFunctions.date(from: model.transportationPickup, format: DateFunctions.DateFormat.ISOFormat, isCurrentTimeZone: false){
                    
                    arrTempDate.append(dt)
                }
                
            }
            
            //lowest date
            if let earliest = arrTempDate.min() {
                
                transportation_pickup = earliest
            }
        }
        
        //make array of lowest date below we used this array for find final lowest date
        var arrDate = [Date]()
        if let maintenance = maintenance_start{
            
            arrDate.append(maintenance)
        }
        if let transport = transportation_pickup{
            
            arrDate.append(transport)
        }
        
        //storage date
        var storageDate : String? = storageService?.storageStartDate
        if orderStatus == .Received{
            
            storageDate = storageService?.storageEndDate
        }
        
        var storageDateObj : Date?
        if let dateStr = storageDate{
            
            if let storageStartDate = DateFunctions.date(from: dateStr, format: DateFunctions.DateFormat.YYYYMMDD, isCurrentTimeZone: false){
                
                storageDateObj = storageStartDate
                arrDate.append(storageStartDate)
            }
        }
        
        //get final lowest date
        var lowestDate : Date?
        if let earliest = arrDate.min() {
            
            lowestDate = earliest
        }
        
        //if no lowest date found please return nil
        if lowestDate == nil{
            
            return nil
        }
        
        //date str
        var lowestDateStr = DateFunctions.stringfromDate(from_date: lowestDate, formate: DateFunctions.DateFormat.ISOFormat, isCurrentTimeZone: false)
        var currentDateStr = DateFunctions.stringfromDate(from_date: Date(), formate: DateFunctions.DateFormat.ISOFormat, isCurrentTimeZone: false)
        var tomorrowDateStr = DateFunctions.stringfromDate(from_date: Date().dateByAddingDay(1), formate: DateFunctions.DateFormat.ISOFormat, isCurrentTimeZone: false)
        
        //if lowest date is storage date then don't need to convert it to local
        if storageDateObj != lowestDate{
            
            if let temp = lowestDateStr{

                lowestDateStr = DateFunctions.UTCToLocal(date: temp, fromFormat: DateFunctions.DateFormat.ISOFormat, toFormat: DateFunctions.DateFormat.YYYYMMDD)
            }
        }else{
            
            lowestDateStr = DateFunctions.stringfromDate(from_date: lowestDate, formate: DateFunctions.DateFormat.YYYYMMDD, isCurrentTimeZone: false)
        }
        

        if let temp = currentDateStr{

            currentDateStr = DateFunctions.UTCToLocal(date: temp, fromFormat: DateFunctions.DateFormat.ISOFormat, toFormat: DateFunctions.DateFormat.YYYYMMDD)
        }

        if let temp = tomorrowDateStr{

            tomorrowDateStr = DateFunctions.UTCToLocal(date: temp, fromFormat: DateFunctions.DateFormat.ISOFormat, toFormat: DateFunctions.DateFormat.YYYYMMDD)
        }
        
        //logic for PENDING & APPROVED Status
        if orderStatus == .Pending || orderStatus == .Approved{
            
            if lowestDateStr == currentDateStr{
                
                return "Recieve Today"
            }
            else if lowestDateStr == tomorrowDateStr{
                
                return "Receive Tomorrow"
            }
            else{
                
                let str = DateFunctions.stringfromDate(from_date: lowestDate!, formate: "MMM dd", isCurrentTimeZone: false)
                return "Receive \(str ?? "")"
            }
        }
        
        //logic for RECEIVED Status
        if orderStatus == .Received{
      
            if lowestDateStr == currentDateStr{
                
                return "Complete Today"
            }
            else if lowestDateStr == tomorrowDateStr{
                
                return "Complete Tomorrow"
            }
            else{
                
                let str = DateFunctions.stringfromDate(from_date: lowestDate!, formate: "MMM dd", isCurrentTimeZone: true)
                return "Complete \(str ?? "")"
            }
        }
        
        return nil
    }
}

public class OrderItemImageModel: Glossy {
    public var isDefault: Bool!
    public var id : Int!
    public var label : String!
    public var orderItemId : Int!
    public var url : String!
    
    //MARK: Default Initializer
    init()
    {
        isDefault = false
        id = 0
        label = ""
        orderItemId = 0
        url = ""
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let isDefault : Bool = "default" <~~ json {
            self.isDefault = isDefault
        }else{
            self.isDefault = false
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let label : String = "label" <~~ json {
            self.label = label
        }else{
        }
        if let orderItemId : Int = "order_item_id" <~~ json {
            self.orderItemId = orderItemId
        }else{
            self.orderItemId = 0
        }
        if let url : String = "url" <~~ json {
            self.url = url
        }else{
            self.url = ""
        }
        
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "default" ~~> isDefault,
            "id" ~~> id,
            "label" ~~> label,
            "order_item_id" ~~> orderItemId,
            "url" ~~> url,
            ])
    }
    
}
