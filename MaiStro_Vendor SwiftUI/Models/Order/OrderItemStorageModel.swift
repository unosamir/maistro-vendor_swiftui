//
//  OrderItemStorageModel.swift
//  MaiStro-Vendor
//
//  Created by samir on 07/11/19.
//  Copyright © 2019 samir. All rights reserved.
//

import Foundation
import Gloss

public class OrderItemStorageModel: Glossy {
    
    public var createdAt : String!
    public var id : Int!
    public var locationId : Int!
    public var note : AnyObject!
    public var orderItemId : Int!
    public var status : String!
    public var storageEndDate : String!
    public var storageRate : Double!
    public var storageStartDate : String!
    public var tax : Double!
    public var total : Double!
    public var updatedAt : String!
    public var order_item_storage_promocode : OrderPromocodeModel?
    
    //MARK: Default Initializer
    init()
    {
        createdAt = ""
        id = 0
        locationId = 0
        note = nil
        orderItemId = 0
        status = ""
        storageEndDate = ""
        storageRate = 0.0
        storageStartDate = ""
        tax = 0.0
        total = 0.0
        updatedAt = ""
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let createdAt : String = "createdAt" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let locationId : Int = "location_id" <~~ json {
            self.locationId = locationId
        }else{
            self.locationId = 0
        }
        if let note : AnyObject = "note" <~~ json {
            self.note = note
        }else{
        }
        if let orderItemId : Int = "order_item_id" <~~ json {
            self.orderItemId = orderItemId
        }else{
            self.orderItemId = 0
        }
        if let status : String = "status" <~~ json {
            self.status = status
        }else{
            self.status = ""
        }
        if let storageEndDate : String = "storage_end_date" <~~ json {
            self.storageEndDate = storageEndDate
        }else{
            self.storageEndDate = ""
        }
        if let storageRate : Double = "storage_rate" <~~ json {
            self.storageRate = storageRate
            self.storageRate = self.storageRate.rounded(toPlaces: 2)
        }else{
            self.storageRate = 0.0
        }
        if let storageStartDate : String = "storage_start_date" <~~ json {
            self.storageStartDate = storageStartDate
        }else{
            self.storageStartDate = ""
        }
        if let tax : Double = "tax" <~~ json {
            self.tax = tax
            self.tax = self.tax.rounded(toPlaces: 2)
        }else{
            self.tax = 0.0
        }
        if let total : Double = "total" <~~ json {
            self.total = total
            self.total = self.total.rounded(toPlaces: 2)
        }else{
            self.total = 0.0
        }
        if let updatedAt : String = "updatedAt" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        
        
        //storage
        if let order_item_storage_promocode = json["order_item_storage_promocode"] as? [String:Any]{
            
            self.order_item_storage_promocode = OrderPromocodeModel.init(json: order_item_storage_promocode)
        }
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "createdAt" ~~> createdAt,
            "id" ~~> id,
            "location_id" ~~> locationId,
            "note" ~~> note,
            "order_item_id" ~~> orderItemId,
            "status" ~~> status,
            "storage_end_date" ~~> storageEndDate,
            "storage_rate" ~~> storageRate,
            "storage_start_date" ~~> storageStartDate,
            "tax" ~~> tax,
            "total" ~~> total,
            "updatedAt" ~~> updatedAt,
            ])
    }
}

public class OrderPromocodeModel: Glossy {
    
    public var createdAt : String!
    public var id : Int!
    public var applied_value : Double!
    public var value_type : String!
    public var value : Double!
    public var order_item_storage_id : Int!
    public var apply_on : String!
    public var updatedAt : String!
    public var promocode : String!
   
    public var order_item_transportation_id : Int!
    public var order_item_maintenance_id : Int!
    
    //MARK: Default Initializer
    init()
    {
        createdAt = ""
        id = 0
        applied_value = 0.0
        value_type = ""
        value = 0.0
        order_item_storage_id = 0
        apply_on = ""
        updatedAt = ""
        promocode = ""
        
        order_item_transportation_id = 0
        order_item_maintenance_id = 0
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        
        if let createdAt : String = "createdAt" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let applied_value : Double = "applied_value" <~~ json {
            self.applied_value = applied_value
        }else{
            self.applied_value = 0.0
        }
        
        if let value_type : String = "value_type" <~~ json {
            self.value_type = value_type
        }else{
            self.value_type = ""
        }
        
        if let value : Double = "value" <~~ json {
            self.value = value
        }else{
            self.value = 0.0
        }
        
        if let order_item_storage_id : Int = "order_item_storage_id" <~~ json {
            self.order_item_storage_id = order_item_storage_id
        }else{
            self.order_item_storage_id = 0
        }
        
        if let apply_on : String = "apply_on" <~~ json {
            self.apply_on = apply_on
        }else{
            self.apply_on = ""
        }
        
        if let updatedAt : String = "updatedAt" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        
        if let order_item_maintenance_id : Int = "order_item_maintenance_id" <~~ json {
            self.order_item_maintenance_id = order_item_maintenance_id
        }else{
            self.order_item_maintenance_id = 0
        }
        
        if let order_item_transportation_id : Int = "order_item_transportation_id" <~~ json {
            self.order_item_transportation_id = order_item_transportation_id
        }else{
            self.order_item_transportation_id = 0
        }
        
        if let promocode : String = "promocode" <~~ json {
           self.promocode = promocode
        }else{
           self.promocode = ""
        }
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "createdAt" ~~> createdAt,
            "id" ~~> id,
            "applied_value" ~~> applied_value,
            "value_type" ~~> value_type,
            "value" ~~> value,
            "order_item_storage_id" ~~> order_item_storage_id,
            "apply_on" ~~> apply_on,
            "updatedAt" ~~> updatedAt,
            "promocode" ~~> promocode,
            ])
    }
}
