//
//  OrderUserModel.swift
//  MaiStro-Vendor
//
//  Created by samir on 11/11/19.
//  Copyright © 2019 samir. All rights reserved.
//

import Foundation
import Gloss

//MARK: - RootClass
public class OrderUserModel: Glossy {
    public var createdAt : String!
    public var email : String!
    public var firstName : String!
    public var id : Int!
    public var lastName : String!
    public var phone : String!
    public var status : String!
    public var updatedAt : String!
    public var userId : Int!
    
    var fullName = ""
    
    //MARK: Default Initializer
    init()
    {
        createdAt = ""
        email = ""
        firstName = ""
        id = 0
        lastName = ""
        phone = ""
        status = ""
        updatedAt = ""
        userId = 0
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let createdAt : String = "createdAt" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let email : String = "email" <~~ json {
            self.email = email
        }else{
            self.email = ""
        }
        if let firstName : String = "first_name" <~~ json {
            self.firstName = firstName
        }else{
            self.firstName = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let lastName : String = "last_name" <~~ json {
            self.lastName = lastName
        }else{
            self.lastName = ""
        }
        if let phone : String = "phone" <~~ json {
            self.phone = phone
        }else{
            self.phone = ""
        }
        if let status : String = "status" <~~ json {
            self.status = status
        }else{
            self.status = ""
        }
        if let updatedAt : String = "updatedAt" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        if let userId : Int = "user_id" <~~ json {
            self.userId = userId
        }else{
            self.userId = 0
        }
        
        fullName = "\(firstName.capitalized) \(lastName.capitalized)"
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "createdAt" ~~> createdAt,
            "email" ~~> email,
            "first_name" ~~> firstName,
            "id" ~~> id,
            "last_name" ~~> lastName,
            "phone" ~~> phone,
            "status" ~~> status,
            "updatedAt" ~~> updatedAt,
            "user_id" ~~> userId,
            ])
    }
    
}
