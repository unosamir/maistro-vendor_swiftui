//
//  ModelExtension.swift
//  Maistro_User
//
//  Created by Meghdoot on 29/11/19.
//  Copyright © 2019 Ravi Alagiya. All rights reserved.
//

import Foundation
import Gloss

extension ClsInboxData {
    
    func getPushPayload() -> ClsPushPayload {
        return ClsPushPayload.init(json: CommonMethods.convertToDictionary(text: self.body) ?? [:]) ?? ClsPushPayload()
    }
}

//MARK: - ClsPushPayload
public class ClsPushPayload: Glossy {
    public var message : String!
    public var messageType : String!
    public var name : String!
    public var threadId : Int!
    private var orderId : Int!
    public var orderItemId : Int!
    public var userId : Int!
    public var adminId : Int!
    public var locationId : Int!
    public var showType : String!
    
    public var order_item_transportation_id : Int!
    public var order_item_maintenance_id : Int!
    public var order_item_storage_id : Int!
    
    //MARK: Default Initializer
    init()
    {
        message = ""
        messageType = ""
        name = ""
        threadId = 0
        orderId = 0
        orderItemId = 0
        userId = 0
        adminId = 0
        locationId = 0
        showType = ""
        
        order_item_transportation_id = 0
        order_item_maintenance_id = 0
        order_item_storage_id = 0
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let message : String = "message" <~~ json {
            self.message = message
        }else{
            self.message = ""
        }
        if let messageType : String = "message_type" <~~ json {
            self.messageType = messageType
        }else{
            self.messageType = ""
        }
        if let name : String = "name" <~~ json {
            self.name = name
        }else{
            self.name = ""
        }
        if let threadId : Int = "thread_id" <~~ json {
            self.threadId = threadId
        }else{
            self.threadId = 0
        }
        if let orderId : Int = "order_id" <~~ json {
            self.orderId = orderId
        }else{
            self.orderId = 0
        }
        if let orderItemId : Int = "order_item_id" <~~ json {
            self.orderItemId = orderItemId
        }else{
            self.orderItemId = 0
        }
        if let userId : Int = "user_id" <~~ json {
            self.userId = userId
        }else{
            self.userId = 0
        }
        if let adminId : Int = "admin_id" <~~ json {
            self.adminId = adminId
        }else{
            self.adminId = 0
        }
        if let locationId : Int = "location_id" <~~ json {
            self.locationId = locationId
        }else{
            self.locationId = 0
        }
        
        if let order_item_transportation_id : Int = "order_item_transportation_id" <~~ json {
            self.order_item_transportation_id = order_item_transportation_id
        }else{
            self.order_item_transportation_id = 0
        }
        if let order_item_maintenance_id : Int = "order_item_maintenance_id" <~~ json {
            self.order_item_maintenance_id = order_item_maintenance_id
        }else{
            self.order_item_maintenance_id = 0
        }
        if let order_item_storage_id : Int = "order_item_storage_id" <~~ json {
            self.order_item_storage_id = order_item_storage_id
        }else{
            self.order_item_storage_id = 0
        }
        if let showType : String = "show_type" <~~ json {
            self.showType = showType
        }else{
            self.showType = ""
        }
        
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "message" ~~> message,
            "message_type" ~~> messageType,
            "name" ~~> name,
            "thread_id" ~~> threadId,
            "order_id" ~~> orderId,
            "order_item_id" ~~> orderItemId,
            "user_id" ~~> userId ,
            "admin_id" ~~> adminId,
            "location_id" ~~> locationId,
            "show_type" ~~> showType,
            
            "order_item_transportation_id" ~~> order_item_transportation_id ,
            "order_item_maintenance_id" ~~> order_item_maintenance_id,
            "order_item_storage_id" ~~> order_item_storage_id
        ])
    }
    
}
