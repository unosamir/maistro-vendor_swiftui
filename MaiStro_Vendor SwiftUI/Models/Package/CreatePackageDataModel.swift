//
//  CreatePackageDataModel.swift
//  MaiStro-Vendor
//
//  Created by Samir on 01/07/20.
//  Copyright © 2020 samir. All rights reserved.
//

import UIKit

class CreatePackageDataModel: NSObject {

    var selectedCategory : CommonServiceModel?
    var selectedItem : CommonServiceModel?
    var selectedMaintenanceService = [ClsService]()
    
    //storage
    var storageModel = PackageStorageModel()
    
    //Transport
    var transportModel : PackageTransportModel?
    
    var packageTitle : String?
    var packageDes : String?
    
    var isSelected = false
    
    override init() {
        
    }
}

class PackageStorageModel: NSObject {

    //storage
    var isDuration : Bool?
    var days : Int?
    var beginDate : Date?
    var finishDate : Date?
    
    override init() {
        
    }
    
    func resetData(){
        
        isDuration = nil
        days = nil
        beginDate = nil
        finishDate = nil
    }
}

class PackageTransportModel: NSObject {

    //Transport
    var isExtended = Bool()
    var regularPrice : Double?
    var extendedPrice : Double?
    
    override init() {
        
    }
}
