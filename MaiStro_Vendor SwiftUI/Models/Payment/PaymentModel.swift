//
//  PaymentModel.swift
//  MaiStro-Vendor
//
//  Created by samir on 12/12/19.
//  Copyright © 2019 samir. All rights reserved.
//

import UIKit

class PaymentModel: NSObject {
    
    public var business_name: String?
    public var default_currency: String?
    public var country: String?
    public var email: String?
    public var bank_name: String?
    public var account_last: String?
    public var routing_number: String?
  
    override init() {
        
    }
    
    required public init?(dictionary: [String:Any]) {
        
        if let stripeDict = dictionary["stripeAccountDetails"] as? [String:Any]{
            
            business_name = stripeDict["business_name"] as? String
            default_currency = stripeDict["default_currency"] as? String
            country = stripeDict["country"] as? String
            email = stripeDict["email"] as? String
            
            if let external_accounts = stripeDict["external_accounts"] as? [String:Any],let arrDict = external_accounts["data"] as? [[String:Any]],let dataDict = arrDict.first{
                
                bank_name = dataDict["bank_name"] as? String
                account_last = dataDict["last4"] as? String
                routing_number = dataDict["routing_number"] as? String
            }
        }
    }
}
