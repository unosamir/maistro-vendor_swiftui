//
//	ClsFAQs.swift
//
//	Create by Ravi Alagiya on 28/11/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ClsFAQs : NSObject, NSCoding{

	var answer : String!
	var createdAt : String!
	var createdBy : Int!
	var ctaType : String!
	var ctaValue : String!
	var externalLink : String!
	var faqTypeId : Int!
	var id : Int!
	var order : Int!
	var question : String!
	var status : Int!
	var updatedAt : String!
	var updatedBy : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		answer = dictionary["answer"] as? String == nil ? "" : dictionary["answer"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		createdBy = dictionary["created_by"] as? Int == nil ? 0 : dictionary["created_by"] as? Int
		ctaType = dictionary["cta_type"] as? String == nil ? "" : dictionary["cta_type"] as? String
		ctaValue = dictionary["cta_value"] as? String == nil ? "" : dictionary["cta_value"] as? String
		externalLink = dictionary["external_link"] as? String == nil ? "" : dictionary["external_link"] as? String
		faqTypeId = dictionary["faq_type_id"] as? Int == nil ? 0 : dictionary["faq_type_id"] as? Int
		id = dictionary["id"] as? Int == nil ? 0 : dictionary["id"] as? Int
		order = dictionary["order"] as? Int == nil ? 0 : dictionary["order"] as? Int
		question = dictionary["question"] as? String == nil ? "" : dictionary["question"] as? String
		status = dictionary["status"] as? Int == nil ? 0 : dictionary["status"] as? Int
		updatedAt = dictionary["updated_at"] as? String == nil ? "" : dictionary["updated_at"] as? String
		updatedBy = dictionary["updated_by"] as? Int == nil ? 0 : dictionary["updated_by"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if answer != nil{
			dictionary["answer"] = answer
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if ctaType != nil{
			dictionary["cta_type"] = ctaType
		}
		if ctaValue != nil{
			dictionary["cta_value"] = ctaValue
		}
		if externalLink != nil{
			dictionary["external_link"] = externalLink
		}
		if faqTypeId != nil{
			dictionary["faq_type_id"] = faqTypeId
		}
		if id != nil{
			dictionary["id"] = id
		}
		if order != nil{
			dictionary["order"] = order
		}
		if question != nil{
			dictionary["question"] = question
		}
		if status != nil{
			dictionary["status"] = status
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		if updatedBy != nil{
			dictionary["updated_by"] = updatedBy
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         answer = aDecoder.decodeObject(forKey: "answer") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? Int
         ctaType = aDecoder.decodeObject(forKey: "cta_type") as? String
         ctaValue = aDecoder.decodeObject(forKey: "cta_value") as? String
         externalLink = aDecoder.decodeObject(forKey: "external_link") as? String
         faqTypeId = aDecoder.decodeObject(forKey: "faq_type_id") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? Int
         order = aDecoder.decodeObject(forKey: "order") as? Int
         question = aDecoder.decodeObject(forKey: "question") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if answer != nil{
			aCoder.encode(answer, forKey: "answer")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if ctaType != nil{
			aCoder.encode(ctaType, forKey: "cta_type")
		}
		if ctaValue != nil{
			aCoder.encode(ctaValue, forKey: "cta_value")
		}
		if externalLink != nil{
			aCoder.encode(externalLink, forKey: "external_link")
		}
		if faqTypeId != nil{
			aCoder.encode(faqTypeId, forKey: "faq_type_id")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if order != nil{
			aCoder.encode(order, forKey: "order")
		}
		if question != nil{
			aCoder.encode(question, forKey: "question")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updated_by")
		}

	}

}