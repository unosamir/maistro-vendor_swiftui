//
//  CommonServiceModel.swift
//  MaiStro-Vendor
//
//  Created by samir on 18/10/19.
//  Copyright © 2019 samir. All rights reserved.
//

import UIKit
import Gloss

public class CommonServiceModel: Glossy {
    public var createdBy : AnyObject!
    public var createdAt : String!
    public var id : Int!
    public var name : String!
    public var sort : Int!
    public var updatedBy : AnyObject!
    public var updatedAt : String!
    public var price : Double!
    public var item_id : Int!
    public var title : String!
    public var desc : String!
    public var items : [CommonServiceModel]!
    
    var isAddOn = false
    var isSelected = false
    var isAlreadyAdded = false

    //MARK: Default Initializer
    init()
    {
        createdBy = nil
        createdAt = ""
        id = 0
        name = ""
        sort = 0
        updatedBy = nil
        updatedAt = ""
        price = 0.0
        item_id = 0
        title = ""
        desc = ""
        items = []
    }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let createdBy : AnyObject = "created_by" <~~ json {
            self.createdBy = createdBy
        }else{
        }
        if let createdAt : String = "createdAt" <~~ json {
            self.createdAt = createdAt
        }else{
            self.createdAt = ""
        }
        if let id : Int = "id" <~~ json {
            self.id = id
        }else{
            self.id = 0
        }
        if let name : String = "name" <~~ json {
            self.name = name
        }else{
            self.name = ""
        }
        if let sort : Int = "sort" <~~ json {
            self.sort = sort
        }else{
            self.sort = 0
        }
        if let updatedBy : AnyObject = "updated_by" <~~ json {
            self.updatedBy = updatedBy
        }else{
        }
        if let updatedAt : String = "updatedAt" <~~ json {
            self.updatedAt = updatedAt
        }else{
            self.updatedAt = ""
        }
        if let price : Double = "price" <~~ json {
            self.price = price
        }else{
            self.price = 0.0
        }
        if let item_id : Int = "item_id" <~~ json {
            self.item_id = item_id
        }else{
            self.item_id = 0
        }
        
        if let title : String = "title" <~~ json {
            self.title = title
        }else{
            self.title = ""
        }
        
        if let desc : String = "description" <~~ json {
            self.desc = desc
        }else{
            self.desc = ""
        }
        
        if name.count == 0{
            
            if let dict : [String:Any] = "item" <~~ json {
                
                if let name = dict["name"] as? String{
                    
                    self.name = name
                }else{
                    
                    self.name = ""
                }
            }else{
                self.name = ""
            }
        }
        
        if let arrItems : [[String:Any]] = "items" <~~ json {
            
            self.items = [CommonServiceModel].from(jsonArray: arrItems) ?? []
        }else{
            
            self.items = []
        }
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "created_by" ~~> createdBy,
            "createdAt" ~~> createdAt,
            "id" ~~> id,
            "name" ~~> name,
            "sort" ~~> sort,
            "updated_by" ~~> updatedBy,
            "updatedAt" ~~> updatedAt,
            "price" ~~> price,
            "item_id" ~~> item_id,
            "items" ~~> items
            ])
    }
    
}
