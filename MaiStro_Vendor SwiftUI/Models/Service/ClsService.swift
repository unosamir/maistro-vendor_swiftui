//
//  ClsService.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 24, 2020

import Foundation


class ClsService : NSObject, NSCoding{

    var createdBy : Int!
    var createdAt : String!
    var daysToComplete : Int!
    var descriptionField : String!
    var id : Int!
    var itemId : Int!
    var itemcategoryId : Int!
    var itemcategorycreatedAt : String!
    var itemcategorydescription : String!
    var itemcategoryid : Int!
    var itemcategoryimage : String!
    var itemcategoryname : String!
    var itemcategorysort : Int!
    var itemcategoryupdatedAt : String!
    var itemcreatedBy : String!
    var itemcreatedAt : String!
    var itemid : Int!
    var itemname : String!
    var itemsort : Int!
    var itemupdatedBy : String!
    var itemupdatedAt : String!
    var locationId : Int!
    var locationaddress : String!
    var locationadminId : Int!
    var locationavgRating : Int!
    var locationcity : String!
    var locationcreatedAt : String!
    var locationid : Int!
    var locationisMaintenance : Int!
    var locationisStorage : Int!
    var locationisTransport : Int!
    var locationlatitude : Double!
    var locationlicenseNumber : String!
    var locationlocationOrderConfigcreatedBy : String!
    var locationlocationOrderConfigcreatedAt : String!
    var locationlocationOrderConfigid : Int!
    var locationlocationOrderConfiglocationId : Int!
    var locationlocationOrderConfigmaxMaintenanceOrders : Int!
    var locationlocationOrderConfigmaxStorageOrders : Int!
    var locationlocationOrderConfigmaxTransportationOrders : Int!
    var locationlocationOrderConfigupdatedBy : String!
    var locationlocationOrderConfigupdatedAt : String!
    var locationlongitude : Double!
    var locationname : String!
    var locationphone : String!
    var locationpostalCode : String!
    var locationprovince : String!
    var locationstatus : Int!
    var locationtax : Double!
    var locationtimezone : String!
    var locationupdatedAt : String!
    var price : Double!
    var sort : Int!
    var status : String!
    var title : String!
    var updatedBy : Int!
    var updatedAt : String!

    var isOptional = true
    var isSelected = false
    var isAddOn = false
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    
    override init() {
        
    }
    
    init(fromDictionary dictionary: [String:Any]){
        
        createdBy = dictionary["created_by"] as? Int ?? 00
        createdAt = dictionary["createdAt"] as? String ?? ""
        daysToComplete = dictionary["days_to_complete"] as? Int ?? 00
        descriptionField = dictionary["description"] as? String ?? "" ?? ""
        id = dictionary["id"] as? Int ?? 00
        itemId = dictionary["item_id"] as? Int ?? 00
        itemcategoryId = dictionary["item.category_id"] as? Int ?? 00
        itemcategorycreatedAt = dictionary["item.category.createdAt"] as? String ?? ""
        itemcategorydescription = dictionary["item.category.description"] as? String ?? ""
        itemcategoryid = dictionary["item.category.id"] as? Int ?? 00
        itemcategoryimage = dictionary["item.category.image"] as? String ?? ""
        itemcategoryname = dictionary["item.category.name"] as? String ?? ""
        itemcategorysort = dictionary["item.category.sort"] as? Int ?? 00
        itemcategoryupdatedAt = dictionary["item.category.updatedAt"] as? String ?? ""
        itemcreatedBy = dictionary["item.created_by"] as? String ?? ""
        itemcreatedAt = dictionary["item.createdAt"] as? String ?? ""
        itemid = dictionary["item.id"] as? Int ?? 00
        itemname = dictionary["item.name"] as? String ?? ""
        itemsort = dictionary["item.sort"] as? Int ?? 00
        itemupdatedBy = dictionary["item.updated_by"] as? String ?? ""
        itemupdatedAt = dictionary["item.updatedAt"] as? String ?? ""
        locationId = dictionary["location_id"] as? Int ?? 00
        locationaddress = dictionary["location.address"] as? String ?? ""
        locationadminId = dictionary["location.admin_id"] as? Int ?? 00
        locationavgRating = dictionary["location.avg_rating"] as? Int ?? 00
        locationcity = dictionary["location.city"] as? String ?? ""
        locationcreatedAt = dictionary["location.createdAt"] as? String ?? ""
        locationid = dictionary["location.id"] as? Int ?? 00
        locationisMaintenance = dictionary["location.is_maintenance"] as? Int ?? 00
        locationisStorage = dictionary["location.is_storage"] as? Int ?? 00
        locationisTransport = dictionary["location.is_transport"] as? Int ?? 00
        locationlatitude = dictionary["location.latitude"] as? Double ?? 0.0
        locationlicenseNumber = dictionary["location.license_number"] as? String ?? ""
        locationlocationOrderConfigcreatedBy = dictionary["location.location_order_config.created_by"] as? String ?? ""
        locationlocationOrderConfigcreatedAt = dictionary["location.location_order_config.createdAt"] as? String ?? ""
        locationlocationOrderConfigid = dictionary["location.location_order_config.id"] as? Int ?? 00
        locationlocationOrderConfiglocationId = dictionary["location.location_order_config.location_id"] as? Int ?? 00
        locationlocationOrderConfigmaxMaintenanceOrders = dictionary["location.location_order_config.max_maintenance_orders"] as? Int ?? 00
        locationlocationOrderConfigmaxStorageOrders = dictionary["location.location_order_config.max_storage_orders"] as? Int ?? 00
        locationlocationOrderConfigmaxTransportationOrders = dictionary["location.location_order_config.max_transportation_orders"] as? Int ?? 00
        locationlocationOrderConfigupdatedBy = dictionary["location.location_order_config.updated_by"] as? String ?? ""
        locationlocationOrderConfigupdatedAt = dictionary["location.location_order_config.updatedAt"] as? String ?? ""
        locationlongitude = dictionary["location.longitude"] as? Double ?? 0.0
        locationname = dictionary["location.name"] as? String ?? ""
        locationphone = dictionary["location.phone"] as? String ?? ""
        locationpostalCode = dictionary["location.postal_code"] as? String ?? ""
        locationprovince = dictionary["location.province"] as? String ?? ""
        locationstatus = dictionary["location.status"] as? Int ?? 00
        locationtax = dictionary["location.tax"] as? Double ?? 0.0
        locationtimezone = dictionary["location.timezone"] as? String ?? ""
        locationupdatedAt = dictionary["location.updatedAt"] as? String ?? ""
        price = dictionary["price"] as? Double ?? 0.0
        sort = dictionary["sort"] as? Int ?? 00
        status = dictionary["status"] as? String ?? ""
        title = dictionary["title"] as? String ?? ""
        updatedBy = dictionary["updated_by"] as? Int ?? 00
        updatedAt = dictionary["updatedAt"] as? String ?? ""
        
        isAddOn = dictionary["is_add_on_only"] as? Bool ?? false
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdBy != nil{
            dictionary["created_by"] = createdBy
        }
        if createdAt != nil{
            dictionary["createdAt"] = createdAt
        }
        if daysToComplete != nil{
            dictionary["days_to_complete"] = daysToComplete
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if id != nil{
            dictionary["id"] = id
        }
        if itemId != nil{
            dictionary["item_id"] = itemId
        }
        if itemcategoryId != nil{
            dictionary["item.category_id"] = itemcategoryId
        }
        if itemcategorycreatedAt != nil{
            dictionary["item.category.createdAt"] = itemcategorycreatedAt
        }
        if itemcategorydescription != nil{
            dictionary["item.category.description"] = itemcategorydescription
        }
        if itemcategoryid != nil{
            dictionary["item.category.id"] = itemcategoryid
        }
        if itemcategoryimage != nil{
            dictionary["item.category.image"] = itemcategoryimage
        }
        if itemcategoryname != nil{
            dictionary["item.category.name"] = itemcategoryname
        }
        if itemcategorysort != nil{
            dictionary["item.category.sort"] = itemcategorysort
        }
        if itemcategoryupdatedAt != nil{
            dictionary["item.category.updatedAt"] = itemcategoryupdatedAt
        }
        if itemcreatedBy != nil{
            dictionary["item.created_by"] = itemcreatedBy
        }
        if itemcreatedAt != nil{
            dictionary["item.createdAt"] = itemcreatedAt
        }
        if itemid != nil{
            dictionary["item.id"] = itemid
        }
        if itemname != nil{
            dictionary["item.name"] = itemname
        }
        if itemsort != nil{
            dictionary["item.sort"] = itemsort
        }
        if itemupdatedBy != nil{
            dictionary["item.updated_by"] = itemupdatedBy
        }
        if itemupdatedAt != nil{
            dictionary["item.updatedAt"] = itemupdatedAt
        }
        if locationId != nil{
            dictionary["location_id"] = locationId
        }
        if locationaddress != nil{
            dictionary["location.address"] = locationaddress
        }
        if locationadminId != nil{
            dictionary["location.admin_id"] = locationadminId
        }
        if locationavgRating != nil{
            dictionary["location.avg_rating"] = locationavgRating
        }
        if locationcity != nil{
            dictionary["location.city"] = locationcity
        }
        if locationcreatedAt != nil{
            dictionary["location.createdAt"] = locationcreatedAt
        }
        if locationid != nil{
            dictionary["location.id"] = locationid
        }
        if locationisMaintenance != nil{
            dictionary["location.is_maintenance"] = locationisMaintenance
        }
        if locationisStorage != nil{
            dictionary["location.is_storage"] = locationisStorage
        }
        if locationisTransport != nil{
            dictionary["location.is_transport"] = locationisTransport
        }
        if locationlatitude != nil{
            dictionary["location.latitude"] = locationlatitude
        }
        if locationlicenseNumber != nil{
            dictionary["location.license_number"] = locationlicenseNumber
        }
        if locationlocationOrderConfigcreatedBy != nil{
            dictionary["location.location_order_config.created_by"] = locationlocationOrderConfigcreatedBy
        }
        if locationlocationOrderConfigcreatedAt != nil{
            dictionary["location.location_order_config.createdAt"] = locationlocationOrderConfigcreatedAt
        }
        if locationlocationOrderConfigid != nil{
            dictionary["location.location_order_config.id"] = locationlocationOrderConfigid
        }
        if locationlocationOrderConfiglocationId != nil{
            dictionary["location.location_order_config.location_id"] = locationlocationOrderConfiglocationId
        }
        if locationlocationOrderConfigmaxMaintenanceOrders != nil{
            dictionary["location.location_order_config.max_maintenance_orders"] = locationlocationOrderConfigmaxMaintenanceOrders
        }
        if locationlocationOrderConfigmaxStorageOrders != nil{
            dictionary["location.location_order_config.max_storage_orders"] = locationlocationOrderConfigmaxStorageOrders
        }
        if locationlocationOrderConfigmaxTransportationOrders != nil{
            dictionary["location.location_order_config.max_transportation_orders"] = locationlocationOrderConfigmaxTransportationOrders
        }
        if locationlocationOrderConfigupdatedBy != nil{
            dictionary["location.location_order_config.updated_by"] = locationlocationOrderConfigupdatedBy
        }
        if locationlocationOrderConfigupdatedAt != nil{
            dictionary["location.location_order_config.updatedAt"] = locationlocationOrderConfigupdatedAt
        }
        if locationlongitude != nil{
            dictionary["location.longitude"] = locationlongitude
        }
        if locationname != nil{
            dictionary["location.name"] = locationname
        }
        if locationphone != nil{
            dictionary["location.phone"] = locationphone
        }
        if locationpostalCode != nil{
            dictionary["location.postal_code"] = locationpostalCode
        }
        if locationprovince != nil{
            dictionary["location.province"] = locationprovince
        }
        if locationstatus != nil{
            dictionary["location.status"] = locationstatus
        }
        if locationtax != nil{
            dictionary["location.tax"] = locationtax
        }
        if locationtimezone != nil{
            dictionary["location.timezone"] = locationtimezone
        }
        if locationupdatedAt != nil{
            dictionary["location.updatedAt"] = locationupdatedAt
        }
        if price != nil{
            dictionary["price"] = price
        }
        if sort != nil{
            dictionary["sort"] = sort
        }
        if status != nil{
            dictionary["status"] = status
        }
        if title != nil{
            dictionary["title"] = title
        }
        if updatedBy != nil{
            dictionary["updated_by"] = updatedBy
        }
        if updatedAt != nil{
            dictionary["updatedAt"] = updatedAt
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        createdBy = aDecoder.decodeObject(forKey: "created_by") as? Int ?? 00
        createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String ?? ""
        daysToComplete = aDecoder.decodeObject(forKey: "days_to_complete") as? Int ?? 00
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String ?? ""
        id = aDecoder.decodeObject(forKey: "id") as? Int ?? 00
        itemId = aDecoder.decodeObject(forKey: "item_id") as? Int ?? 00
        itemcategoryId = aDecoder.decodeObject(forKey: "item.category_id") as? Int ?? 00
        itemcategorycreatedAt = aDecoder.decodeObject(forKey: "item.category.createdAt") as? String ?? ""
        itemcategorydescription = aDecoder.decodeObject(forKey: "item.category.description") as? String ?? ""
        itemcategoryid = aDecoder.decodeObject(forKey: "item.category.id") as? Int ?? 00
        itemcategoryimage = aDecoder.decodeObject(forKey: "item.category.image") as? String ?? ""
        itemcategoryname = aDecoder.decodeObject(forKey: "item.category.name") as? String ?? ""
        itemcategorysort = aDecoder.decodeObject(forKey: "item.category.sort") as? Int ?? 00
        itemcategoryupdatedAt = aDecoder.decodeObject(forKey: "item.category.updatedAt") as? String ?? ""
        itemcreatedBy = aDecoder.decodeObject(forKey: "item.created_by") as? String ?? ""
        itemcreatedAt = aDecoder.decodeObject(forKey: "item.createdAt") as? String ?? ""
        itemid = aDecoder.decodeObject(forKey: "item.id") as? Int ?? 00
        itemname = aDecoder.decodeObject(forKey: "item.name") as? String ?? ""
        itemsort = aDecoder.decodeObject(forKey: "item.sort") as? Int ?? 00
        itemupdatedBy = aDecoder.decodeObject(forKey: "item.updated_by") as? String ?? ""
        itemupdatedAt = aDecoder.decodeObject(forKey: "item.updatedAt") as? String ?? ""
        locationId = aDecoder.decodeObject(forKey: "location_id") as? Int ?? 00
        locationaddress = aDecoder.decodeObject(forKey: "location.address") as? String ?? ""
        locationadminId = aDecoder.decodeObject(forKey: "location.admin_id") as? Int ?? 00
        locationavgRating = aDecoder.decodeObject(forKey: "location.avg_rating") as? Int ?? 00
        locationcity = aDecoder.decodeObject(forKey: "location.city") as? String ?? ""
        locationcreatedAt = aDecoder.decodeObject(forKey: "location.createdAt") as? String ?? ""
        locationid = aDecoder.decodeObject(forKey: "location.id") as? Int ?? 00
        locationisMaintenance = aDecoder.decodeObject(forKey: "location.is_maintenance") as? Int ?? 00
        locationisStorage = aDecoder.decodeObject(forKey: "location.is_storage") as? Int ?? 00
        locationisTransport = aDecoder.decodeObject(forKey: "location.is_transport") as? Int ?? 00
        locationlatitude = aDecoder.decodeObject(forKey: "location.latitude") as? Double ?? 0.0
        locationlicenseNumber = aDecoder.decodeObject(forKey: "location.license_number") as? String ?? ""
        locationlocationOrderConfigcreatedBy = aDecoder.decodeObject(forKey: "location.location_order_config.created_by") as? String ?? ""
        locationlocationOrderConfigcreatedAt = aDecoder.decodeObject(forKey: "location.location_order_config.createdAt") as? String ?? ""
        locationlocationOrderConfigid = aDecoder.decodeObject(forKey: "location.location_order_config.id") as? Int ?? 00
        locationlocationOrderConfiglocationId = aDecoder.decodeObject(forKey: "location.location_order_config.location_id") as? Int ?? 00
        locationlocationOrderConfigmaxMaintenanceOrders = aDecoder.decodeObject(forKey: "location.location_order_config.max_maintenance_orders") as? Int ?? 00
        locationlocationOrderConfigmaxStorageOrders = aDecoder.decodeObject(forKey: "location.location_order_config.max_storage_orders") as? Int ?? 00
        locationlocationOrderConfigmaxTransportationOrders = aDecoder.decodeObject(forKey: "location.location_order_config.max_transportation_orders") as? Int ?? 00
        locationlocationOrderConfigupdatedBy = aDecoder.decodeObject(forKey: "location.location_order_config.updated_by") as? String ?? ""
        locationlocationOrderConfigupdatedAt = aDecoder.decodeObject(forKey: "location.location_order_config.updatedAt") as? String ?? ""
        locationlongitude = aDecoder.decodeObject(forKey: "location.longitude") as? Double ?? 0.0
        locationname = aDecoder.decodeObject(forKey: "location.name") as? String ?? ""
        locationphone = aDecoder.decodeObject(forKey: "location.phone") as? String ?? ""
        locationpostalCode = aDecoder.decodeObject(forKey: "location.postal_code") as? String ?? ""
        locationprovince = aDecoder.decodeObject(forKey: "location.province") as? String ?? ""
        locationstatus = aDecoder.decodeObject(forKey: "location.status") as? Int ?? 00
        locationtax = aDecoder.decodeObject(forKey: "location.tax") as? Double ?? 0.0
        locationtimezone = aDecoder.decodeObject(forKey: "location.timezone") as? String ?? ""
        locationupdatedAt = aDecoder.decodeObject(forKey: "location.updatedAt") as? String ?? ""
        price = aDecoder.decodeObject(forKey: "price") as? Double ?? 0.0
        sort = aDecoder.decodeObject(forKey: "sort") as? Int ?? 00
        status = aDecoder.decodeObject(forKey: "status") as? String ?? ""
        title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? Int ?? 00
        updatedAt = aDecoder.decodeObject(forKey: "updatedAt") as? String ?? ""
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if createdBy != nil{
            aCoder.encode(createdBy, forKey: "created_by")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "createdAt")
        }
        if daysToComplete != nil{
            aCoder.encode(daysToComplete, forKey: "days_to_complete")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if itemId != nil{
            aCoder.encode(itemId, forKey: "item_id")
        }
        if itemcategoryId != nil{
            aCoder.encode(itemcategoryId, forKey: "item.category_id")
        }
        if itemcategorycreatedAt != nil{
            aCoder.encode(itemcategorycreatedAt, forKey: "item.category.createdAt")
        }
        if itemcategorydescription != nil{
            aCoder.encode(itemcategorydescription, forKey: "item.category.description")
        }
        if itemcategoryid != nil{
            aCoder.encode(itemcategoryid, forKey: "item.category.id")
        }
        if itemcategoryimage != nil{
            aCoder.encode(itemcategoryimage, forKey: "item.category.image")
        }
        if itemcategoryname != nil{
            aCoder.encode(itemcategoryname, forKey: "item.category.name")
        }
        if itemcategorysort != nil{
            aCoder.encode(itemcategorysort, forKey: "item.category.sort")
        }
        if itemcategoryupdatedAt != nil{
            aCoder.encode(itemcategoryupdatedAt, forKey: "item.category.updatedAt")
        }
        if itemcreatedBy != nil{
            aCoder.encode(itemcreatedBy, forKey: "item.created_by")
        }
        if itemcreatedAt != nil{
            aCoder.encode(itemcreatedAt, forKey: "item.createdAt")
        }
        if itemid != nil{
            aCoder.encode(itemid, forKey: "item.id")
        }
        if itemname != nil{
            aCoder.encode(itemname, forKey: "item.name")
        }
        if itemsort != nil{
            aCoder.encode(itemsort, forKey: "item.sort")
        }
        if itemupdatedBy != nil{
            aCoder.encode(itemupdatedBy, forKey: "item.updated_by")
        }
        if itemupdatedAt != nil{
            aCoder.encode(itemupdatedAt, forKey: "item.updatedAt")
        }
        if locationId != nil{
            aCoder.encode(locationId, forKey: "location_id")
        }
        if locationaddress != nil{
            aCoder.encode(locationaddress, forKey: "location.address")
        }
        if locationadminId != nil{
            aCoder.encode(locationadminId, forKey: "location.admin_id")
        }
        if locationavgRating != nil{
            aCoder.encode(locationavgRating, forKey: "location.avg_rating")
        }
        if locationcity != nil{
            aCoder.encode(locationcity, forKey: "location.city")
        }
        if locationcreatedAt != nil{
            aCoder.encode(locationcreatedAt, forKey: "location.createdAt")
        }
        if locationid != nil{
            aCoder.encode(locationid, forKey: "location.id")
        }
        if locationisMaintenance != nil{
            aCoder.encode(locationisMaintenance, forKey: "location.is_maintenance")
        }
        if locationisStorage != nil{
            aCoder.encode(locationisStorage, forKey: "location.is_storage")
        }
        if locationisTransport != nil{
            aCoder.encode(locationisTransport, forKey: "location.is_transport")
        }
        if locationlatitude != nil{
            aCoder.encode(locationlatitude, forKey: "location.latitude")
        }
        if locationlicenseNumber != nil{
            aCoder.encode(locationlicenseNumber, forKey: "location.license_number")
        }
        if locationlocationOrderConfigcreatedBy != nil{
            aCoder.encode(locationlocationOrderConfigcreatedBy, forKey: "location.location_order_config.created_by")
        }
        if locationlocationOrderConfigcreatedAt != nil{
            aCoder.encode(locationlocationOrderConfigcreatedAt, forKey: "location.location_order_config.createdAt")
        }
        if locationlocationOrderConfigid != nil{
            aCoder.encode(locationlocationOrderConfigid, forKey: "location.location_order_config.id")
        }
        if locationlocationOrderConfiglocationId != nil{
            aCoder.encode(locationlocationOrderConfiglocationId, forKey: "location.location_order_config.location_id")
        }
        if locationlocationOrderConfigmaxMaintenanceOrders != nil{
            aCoder.encode(locationlocationOrderConfigmaxMaintenanceOrders, forKey: "location.location_order_config.max_maintenance_orders")
        }
        if locationlocationOrderConfigmaxStorageOrders != nil{
            aCoder.encode(locationlocationOrderConfigmaxStorageOrders, forKey: "location.location_order_config.max_storage_orders")
        }
        if locationlocationOrderConfigmaxTransportationOrders != nil{
            aCoder.encode(locationlocationOrderConfigmaxTransportationOrders, forKey: "location.location_order_config.max_transportation_orders")
        }
        if locationlocationOrderConfigupdatedBy != nil{
            aCoder.encode(locationlocationOrderConfigupdatedBy, forKey: "location.location_order_config.updated_by")
        }
        if locationlocationOrderConfigupdatedAt != nil{
            aCoder.encode(locationlocationOrderConfigupdatedAt, forKey: "location.location_order_config.updatedAt")
        }
        if locationlongitude != nil{
            aCoder.encode(locationlongitude, forKey: "location.longitude")
        }
        if locationname != nil{
            aCoder.encode(locationname, forKey: "location.name")
        }
        if locationphone != nil{
            aCoder.encode(locationphone, forKey: "location.phone")
        }
        if locationpostalCode != nil{
            aCoder.encode(locationpostalCode, forKey: "location.postal_code")
        }
        if locationprovince != nil{
            aCoder.encode(locationprovince, forKey: "location.province")
        }
        if locationstatus != nil{
            aCoder.encode(locationstatus, forKey: "location.status")
        }
        if locationtax != nil{
            aCoder.encode(locationtax, forKey: "location.tax")
        }
        if locationtimezone != nil{
            aCoder.encode(locationtimezone, forKey: "location.timezone")
        }
        if locationupdatedAt != nil{
            aCoder.encode(locationupdatedAt, forKey: "location.updatedAt")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if sort != nil{
            aCoder.encode(sort, forKey: "sort")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if updatedBy != nil{
            aCoder.encode(updatedBy, forKey: "updated_by")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updatedAt")
        }
    }
}
