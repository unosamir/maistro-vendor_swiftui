//
//  MaintenanceServiceModel.swift
//  MaiStro-Vendor
//
//  Created by samir on 21/10/19.
//  Copyright © 2019 samir. All rights reserved.
//

import UIKit

class MaintenanceCategoryModel: NSObject {

    var category_id : Int?
    var category_name : String?
    var arrItems : [MaintenanceItemModel] = []
}

class MaintenanceItemModel: NSObject {

    var item_id : Int?
    var item_name : String?
    var arrService : [ClsService] = []
    var isExpand = false
  
    //temp
    var completion : (()->())?
}
