//
//  StorageServiceModel.swift
//  MaiStro-Vendor
//
//  Created by Samir on 27/04/20.
//  Copyright © 2020 samir. All rights reserved.
//

import UIKit

class StorageServiceModel: NSObject {

    var category_id : Int?
    var category_name : String?
    var arrServices : [ClsService] = []
    
    var isSelected = false
}
