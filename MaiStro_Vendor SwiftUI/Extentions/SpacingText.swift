//
//  SpacingText.swift
//
//  Created by Meghdoot Dhameliya on 19/12/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

extension UILabel {
    func addCharactersSpacing(spacing:CGFloat, text:String) {
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSMakeRange(0, text.count))
        self.attributedText = attributedString
    }
}
