//
//  Utilities.swift


import UIKit
import MapKit

class Utilities: NSObject {
    
    //MARK:- Check Internet Connenction If you are using alamofire then only it will work
    static func checkInternetConnection() -> Bool {
        let networkReachabilityManager = NetworkReachabilityManager()
        networkReachabilityManager?.startListening()
        if networkReachabilityManager?.isReachable == false{
            return false
        }else{            
            return true
        }
    }
    
    //MARK: JSON
    static func jsonToString(json: Any) -> String?{
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            return convertedString
        } catch let myJSONError {
            print(myJSONError)
        }
        
        return nil
    }
    
    static func objectFromJsonString(str:String) -> Any?{
        
        let jsonString = str
        let jsonData = jsonString.data(using: .utf8)
        let dict = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)
        return dict
    }
    
    //MARK: Dollar
    class func convertToDollar(number : Double) -> String {
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.currency
        currencyFormatter.currencyCode = "USD"
        currencyFormatter.maximumFractionDigits = 0
        currencyFormatter.minimumFractionDigits = 0
        currencyFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        return currencyFormatter.string(from: NSNumber(value: number))!
    }
    
    class func convertToINR(number : Double) -> String {

        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.currency
        currencyFormatter.currencyCode = "INR"
        currencyFormatter.maximumFractionDigits = 0
        currencyFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        return currencyFormatter.string(from: NSNumber(value: number))!
    }
  
    //MARK: Text Height
    
    class func getLabelHeight(constraintedWidth width: CGFloat, font: UIFont,text:String) -> CGFloat {
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.font = font
        label.sizeToFit()
        
        return label.frame.height
    }
    
    class func getLabelWidth(constraintedheight: CGFloat, font: UIFont,text:String) -> CGFloat {
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: .greatestFiniteMagnitude, height: constraintedheight))
        label.numberOfLines = 1
        label.text = text
        label.font = font
        label.sizeToFit()
        
        return label.frame.width
    }
    
    class func getDocumentsDirectory() -> URL{
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    class func loadImage(_ imageName: String) -> UIImage {
        
        return UIImage(contentsOfFile: imageName) ?? #imageLiteral(resourceName: "image_Chat")
    }
    
    //MARK:- Show Alert View
    
    class func showAlertView(title: String?, message: String?) {
      
        switch UIApplication.shared.applicationState {
            case .active:

//                let alert = UIAlertController(title: title ?? kAppName, message: message, preferredStyle: UIAlertController.Style.alert)
                let alert = SmartAlertController(title: title ?? kAppName, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: StringConstants.ButtonConstant.kOk, style: .default, handler: nil))
                alert.presentInOwnWindow(animated: true, completion: nil)
            default:
                break
        }
    }
    
    class func showAlertWithButtonAction(title: String,message:String,buttonTitle:String,onOKClick: @escaping () -> ()){
      
        switch UIApplication.shared.applicationState {
            case .active:

//                let alert = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertController.Style.alert)
                let alert = SmartAlertController(title: kAppName, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { action in
                      onOKClick()
                }))
                
                alert.presentInOwnWindow(animated: true, completion: nil)
            default:
                break
        }
        
    }
   
    class func showAlertWithTwoButtonAction(title: String,message:String,attributedMessage: NSAttributedString?,buttonTitle1:String,buttonTitle2:String,onButton1Click: @escaping () -> (),onButton2Click: @escaping () -> ()){
        
        
        switch UIApplication.shared.applicationState {
            case .active:

                var strTitle = title
                if strTitle.count == 0 {
                    strTitle = kAppName
                }
                
//                let alert = UIAlertController(title: strTitle, message: message, preferredStyle: UIAlertController.Style.alert)
                let alert = SmartAlertController(title: strTitle, message: message, preferredStyle: .alert)
                
                if attributedMessage != nil {
                    alert.setValue(attributedMessage, forKey: "attributedTitle")
                }
                alert.addAction(UIAlertAction(title: buttonTitle1, style: .default, handler: { action in
                    onButton1Click()
                }))
                
                alert.addAction(UIAlertAction(title: buttonTitle2, style: .default, handler: { action in
                    onButton2Click()
                }))
                
                alert.presentInOwnWindow(animated: true, completion: nil)
            
            default:
                break
        }
        
    }
    
    //MARK: Call
    class func call(phoneNumber: String)
    {
        let trimPhone = phoneNumber.replacingOccurrences(of: " ", with: "")
        if  let url = URL(string: "tel://\(trimPhone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK: Open mail
    class func openEmail(email: String)
    {
        if let url = URL(string: "mailto:\(email)")  {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    class func openWebView(link: String)
    {
        let vc = WebViewVC(nibName: "WebViewVC", bundle: nil)
        vc.link = link
        kAppDelegate.topViewController()?.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Open URL in Safari
    
    class func openUrl(strUrl:String)
    {
        if let url = URL(string: strUrl){
            
            if UIApplication.shared.canOpenURL(url)
            {
                UIApplication.shared.open(url, options:[:], completionHandler: nil)
            }
        }else{
            
            Utilities.showAlertView(title: kAppName, message: "No link found")
        }
        
    }
    
    //MARK: Change Color
    class func changeStringColor(string : String, array : [String], colorArray : [UIColor],arrFont:[UIFont]) ->  NSAttributedString {
        
        let attrStr = NSMutableAttributedString(string: string)
        let inputLength = attrStr.string.count
        let searchString = array

        for i in 0...searchString.count-1
        {
            
            let string  = searchString[i]
            let searchLength = string.count
            var range = NSRange(location: 0, length: attrStr.length)
            
            while (range.location != NSNotFound) {
                range = (attrStr.string as NSString).range(of: string, options: [], range: range)
                if (range.location != NSNotFound) {
                    
                    if colorArray.count > 0{
                        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: colorArray[0], range: NSRange(location: range.location, length: searchLength))
                    }
                    
                    if arrFont.count > 0{
                        attrStr.addAttribute(NSAttributedString.Key.font, value: arrFont[0], range: NSRange(location: range.location, length: searchLength))
                    }
                    
                    range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
                    return attrStr
                }
            }
        }
        
        return NSAttributedString()
    }
    

    static func openGoogleMapWithDirection(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)){
        
            //open app
            let url = URL(string: "comgooglemaps://?saddr=\(source.latitude),\(source.longitude)&daddr=\(destination.latitude),\(destination.longitude)&directionsmode=driving")
            
            if  let url = url, UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else{
            
            //open in browser
            let url = URL(string: "https://www.google.com/maps/?saddr=\(source.latitude),\(source.longitude)&daddr=\(destination.latitude),\(destination.longitude)&directionsmode=driving")
            
            if  let url = url, UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    static func openAppleMapWithDirection(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        
        let source1 = MKMapItem(placemark: MKPlacemark(coordinate: source))
        source1.name = "Source"
        
        let destination1 = MKMapItem(placemark: MKPlacemark(coordinate: destination))
        destination1.name = "Destination"
        
        MKMapItem.openMaps(with: [source1, destination1], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
}
