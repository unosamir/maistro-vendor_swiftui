//
//  CommonMethods.swift
//
//  Created by Apple on 23/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit


public enum PayloadType : Int {
    case DICTIONARY
    case ARRAY
    case NONE
}

class CommonMethods: NSObject
{
//
//    class func showAlertMessage(_ alertMesasge: String) {
//        let alert = UIAlertController(title: kAppName, message: trimString(value: alertMesasge), preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
//        })
//        alert.presentInOwnWindow(animated: true, completion: nil)
////        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
//    }
    
    // MARK: - Rechability Functions
    
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    // MARK: - Other Functions
    
    class func convertToDictionary(text: String) -> [String: AnyObject]? {
        guard let data = text.data(using: .utf8) else {
            return ["code":400 as AnyObject]
        }
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
        } catch {
        }
        return ["code":400 as AnyObject]
    }
    
    class func trimString(value:String) -> String {
        let str=value.trimmingCharacters(in: .whitespaces)
        return str
    }
    
    
    class func isEmail(_ email: String) -> Bool {
        let emailRegEx: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let regExPredicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return regExPredicate.evaluate(with: email.lowercased())
    }
    
    class func isPhoneNumber(_ phone : String) -> Bool {
        let allowedCharacters = CharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phone.components(separatedBy: allowedCharacters)
        let filtered = inputString.joined(separator: "")
        return phone == filtered
    }
    
    class func isValidPassword(_ password: String) -> Bool {
        let passwordRegEx: String = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
        
        let regExPredicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return regExPredicate.evaluate(with: password)
    }
    
    class func sizeOfImage(image:UIImage ,inAspectFitImageView imageview:UIImageView,view : UIView ) -> CGSize {
        
        //imageview.contentMode = UIViewContentMode.scaleAspectFit
        let imageViewWidth = imageview.bounds.size.width //view.frame.size.width
        let imageViewHeight = imageview.bounds.size.height
        
        
        let imageWidth = image.size.width
        let imageHeight = image.size.height
        
        let scaleFactor = max(imageViewWidth/imageWidth,imageViewHeight/imageHeight)
        
        return CGSize(width: image.size.width * scaleFactor, height: image.size.height * scaleFactor)
    }
    
    class func sizeOfImageWithHeightWidth(height:CGFloat,width : CGFloat ,inAspectFitImageView imageview:UIImageView , isWidthFixed : Bool = true) -> CGSize {
        
        imageview.contentMode = UIView.ContentMode.scaleAspectFit
        let imageViewWidth = isWidthFixed ?  imageview.bounds.size.width : UIScreen.main.bounds.size.width
        let imageViewHeight = imageview.bounds.size.height
        
        let imageWidth = width
        let imageHeight = height
        
        let scaleFactor = isWidthFixed ? max(imageViewWidth/imageWidth,imageViewHeight/imageHeight) : min(imageViewWidth/imageWidth,imageViewHeight/imageHeight)
        
        let s = CGSize(width: width * scaleFactor, height: height * scaleFactor)
        
        return s
    }
    
    //MARK: - Date Helper
    class func convertMessageDateFormater(_ strDate: String, requiredTodayText : Bool = false) -> String
    {
        if strDate == "" { return "" }
        let dateFormatter = DateFormatter()
        
        dateFormatter.setLocale()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let date = dateFormatter.date(from: strDate)
        if date?.isToday() ?? false {
            dateFormatter.dateFormat =  requiredTodayText ? "'Today' h:mm a" : "h:mm a"
        }else if( date?.isYesterday() ?? false){
            dateFormatter.dateFormat = "'Yesterday' h:mm a"
        }else{
            dateFormatter.dateFormat = "MMM d h:mm a"
        }
        dateFormatter.timeZone = TimeZone.current
        return  dateFormatter.string(from: date!)
    }
    
    class func readSingleNotification(notificationUUID:String)
    {
        if let objUser : UserDetailModel  = ApplicationData.user
        {
            NotificationManager.readSingleNotification(userID: "\(objUser.id!)", notificationID: notificationUUID) { (result, custError) in
                if result == true{
                    print("Notification Read Done")
                }
            }
        }
    }
    
    class func getUnreadNotificationCount() -> Promise<Int>{
        
        return Promise<Int> { resolver in
            if let objUser : UserDetailModel  = ApplicationData.user
            {
                //,params: ["type":"push"]
                NotificationManager.getUnreadCount(userID: "\(objUser.id!)") { (unreadCount, customError) in
                    if customError == nil{
                        resolver.fulfill(unreadCount)
                    }
                    else{
                        let errMsg = (customError?.failureReason) ?? ""
                        resolver.reject(CustomError.init(title: errMsg, description: errMsg, code: 500))
                    }
                }
            }else{
                resolver.reject(CustomError.init(title: "Invalid user data", description: "Unable to get user information", code: 500))
            }
        }
        
    }
    
    // MARK: - iPhoneX Extra Bottom Height
    
    class func isIphoneXHeight() -> CGFloat
    {
        var height : CGFloat = 0
        
        if UIScreen.main.nativeBounds.size.height == 2436.0 {
            height = 34
        }
        
        return height
    }
    
    //MARK: Others
    class func getJsonString(_ resDix:[String:AnyObject]) -> String{
        let jsonData = try? JSONSerialization.data(withJSONObject: resDix, options: [])
        return String(data: jsonData!, encoding: .utf8)!
    }
    
    class func getPrice(_ price:Float) -> String {
        return String(format:"%.2f",price)
    }
    
    class func imageWithImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
        let oldWidth = sourceImage.size.width
        let scaleFactor = scaledToWidth / oldWidth
        
        let newHeight = sourceImage.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    class func createGradientLayer (view : UIView, color1:UIColor, color2 : UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [color1.cgColor, color2.cgColor]
        view.layer.addSublayer(gradientLayer)
    }
    
    //MARK:- Get clean number
    class func getCleanNumber( phone : String)-> String {
        var phone = phone
        phone = phone.replacingOccurrences(of: "-", with: "")
        phone = phone.replacingOccurrences(of: " ", with: "")
        phone = phone.replacingOccurrences(of: "(", with: "")
        phone = phone.replacingOccurrences(of: ")", with: "")
        return phone
    }

    class func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: 0, y: KStatusBarHeight + 54, width: kAppDelegate.window!.frame.size.width, height: 35))
        toastLabel.backgroundColor = UIColor.black
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 14.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0
        
        kAppDelegate.window!.addSubview(toastLabel)
        //        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
        //            toastLabel.alpha = 0.0
        //        }, completion: {(isCompleted) in
        //            toastLabel.removeFromSuperview()
        //        })
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            toastLabel.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, delay: 1.5, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0.0
            }, completion: {_ in
                toastLabel.removeFromSuperview()
            })
        })
    }
    
    class func configureTextFieldColorFont(arrTextFields:[TKFormTextField]){
        
        for textField in arrTextFields{
            
            textField.textColor = ColorConstant.TextFieldTextColor
            textField.selectedTitleColor = ColorConstant.TextFieldTextColor
            textField.placeholderColor = ColorConstant.TextFieldTextColor
            textField.titleColor = ColorConstant.TextFieldTextColor
            textField.errorColor = ColorConstant.TextFieldErrorColor
            textField.placeholderFont = FontScheme.kProDisplayRegularFont(size: 16)
            textField.titleLabel.font = FontScheme.kProTextRegularFont(size: 12)
            textField.backgroundColor = ColorConstant.DisableAndTextFieldBGColor
            textField.lineView.isHidden = true
            textField.cornerRadius = 2
        }
    }
    
    class func getCellModel(type : CellType) -> CellModel {
        
        let model = CellModel()
        model.cellType = type
        return model
    }
    
    class func getCellModel(placeholder:String,cellType:CellType = .none) -> CellModel {
        
        let model = CellModel()
        model.cellType = cellType
        model.placeholder = placeholder
        return model
    }
    
    class func showSuccessPopup(title:String,subTitle:String,textMsg:String,completion:@escaping (()->())){
        
        let objView : SuccessPopupView = SuccessPopupView(frame: UIScreen.main.bounds, title: title, subTitle: subTitle, textMsg: textMsg)
        let pop = FFPopup.init(contentView: objView)
        pop.dimmedMaskAlpha = 0.75
        pop.showInDuration = 0.5
        pop.dismissOutDuration = 0.3
        pop.shouldDismissOnContentTouch = false
        pop.shouldDismissOnBackgroundTouch = true
        pop.show()
        objView.handler = {
            pop.dismiss(animated: true)
            
            completion()
        }
    }
    
    class func registerTableViewCustomCell(_ arrayNib: [String], tableView: UITableView) {
        for strCellName: String in arrayNib {
            tableView.register(UINib(nibName: strCellName, bundle: nil), forCellReuseIdentifier: strCellName)
        }
    }
    
    class func registerCollectionViewCustomCell(_ arrayNib: [String], collectionView: UICollectionView) {
        for strCellName: String in arrayNib {
            collectionView.register(UINib(nibName: strCellName, bundle: nil), forCellWithReuseIdentifier: strCellName)
        }
    }
    
    class func getHeaderView(labelText:String) -> UIView{
        let lbl = UILabel(frame: CGRect(x: 20, y: -3, width: UIScreen.main.bounds.size.width - 32, height: 13))
        lbl.text = labelText
        lbl.font = FontScheme.kProDisplayRegularFont(size: 15)
        lbl.textColor = ColorConstant.TextMainColor
        lbl.sizeToFit()
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        vw.backgroundColor = .white
        vw.addSubview(lbl)
        return vw
    }
    
    class func changeButtonColor(btn:UIButton, color:UIColor, image:String) {
        let origImage = UIImage(named: image)
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        btn.setImage(tintedImage, for: .normal)
        btn.tintColor = color
    }
    
    
    //MARK:- New Notifications methods
    
    class func createUserForSDKNotification(_ objUserPayload : UserDetailModel, isPassNotificationEnableFlag:Bool){
        
        guard UserDefaultsMethods.getString(key: KFireBaseToken) != nil else {
            return
        }
        
        var params : [String : String] = [
            "ref_id":"\(objUserPayload.id!)",
            "first_name":objUserPayload.first_name!,
            "last_name":objUserPayload.last_name!,
            "email":objUserPayload.email! ,
            "phone":objUserPayload.phone != nil ? objUserPayload.phone! : "" ,
            "notification_token":UserDefaultsMethods.getString(key: KFireBaseToken)!,
            "device_id": NotificationManager.getUUID(),
            "platform":"ios",
            "platform_version": APP_VERSION
        ]
        
        if isPassNotificationEnableFlag {
            params["notification_enabled"] = "true"
        }
        
        NotificationManager.createUser(params: params) { (isDone, custerror) in
            if custerror == nil{
                print("Yes Register To Notification")

            }else{
                print("Failed to Register")
            }
        }
        
    }
    
    class func logoutFromNotificationSdk(){
        if let objUserPayload = UserDefaultsMethods.getCustomObject(key: AppConstants.UserDefaultKeys.UserInfoModelKey) as? UserDetailModel{
            NotificationManager.setDeviceStatus(isLoggedIn: false, userID: "\(objUserPayload.id!)", deviceID: NotificationManager.getUUID()) { (isDone, custerror) in
                print("Logout from Success")
            }
        }
    }
    
}

