
import UIKit

//MARK:  Font Constant

struct FontConstant {
 
    static let kProDisplayRegularFont              = "SFProDisplay-Regular"
    static let kProDisplayBoldFont                 = "SFProDisplay-Bold"
    static let kProDisplaySemiBoldFont             = "SFProDisplay-Semibold"
    static let kProDisplayMediumFont               = "SFProDisplay-Medium"
    
    static let kProTextRegularFont                  = "SFProText-Regular"
    static let kProTextLightFont                    = "SFProText-Light"
    static let kProTextBoldFont                     = "SFProText-Bold"
    static let kProTextSemiBoldFont                 = "SFProText-Semibold"
    static let kProTextMediumFont                   = "SFProText-Medium"    
    
}

//MARK: Display Font
class FontScheme: NSObject {

    //Regular
    static func kProDisplayRegularFont(size : CGFloat) -> UIFont  {
        
        return UIFont(name: FontConstant.kProDisplayRegularFont, size: size)!
    }
    
    //Medium
    static func kProDisplayBoldFont(size : CGFloat) -> UIFont  {
        
        return UIFont(name: FontConstant.kProDisplayBoldFont, size: size)!
    }
    
    //Bold
    static func kProDisplaySemiBoldFont(size : CGFloat) -> UIFont  {
        
        return UIFont(name: FontConstant.kProDisplaySemiBoldFont, size: size)!
    }
    
    static func kProDisplayMediumFont(size : CGFloat) -> UIFont  {
        
        return UIFont(name: FontConstant.kProDisplayMediumFont, size: size)!
    }
}

//MARK: Text Font
extension FontScheme {
    
    //Regular
    static func kProTextRegularFont(size : CGFloat) -> UIFont  {
        
        return UIFont(name: FontConstant.kProTextRegularFont, size: size)!
    }
    
    //Medium
    static func kProTextLightFont(size : CGFloat) -> UIFont  {
        
        return UIFont(name: FontConstant.kProTextLightFont, size: size)!
    }
    
    //Bold
    static func kProTextBoldFont(size : CGFloat) -> UIFont  {
        
        return UIFont(name: FontConstant.kProTextBoldFont, size: size)!
    }
    
    static func kProTextSemiBoldFont(size : CGFloat) -> UIFont  {
        
        return UIFont(name: FontConstant.kProTextSemiBoldFont, size: size)!
    }
    
    static func kProTextMediumFont(size : CGFloat) -> UIFont  {
        
        return UIFont(name: FontConstant.kProTextMediumFont, size: size)!
    }
}
