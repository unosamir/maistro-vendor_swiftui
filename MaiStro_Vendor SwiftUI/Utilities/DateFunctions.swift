//
//  DateFunctions.swift
//
//  Created by Apple on 04/10/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit


class DateFunctions: NSObject
{
    
    static var localTimeZoneIdentifier: String { return TimeZone.current.identifier }

    struct DateFormat {
        
//        static let ISOFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        static let ISOFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        static let YYYYMMDD = "yyyy-MM-dd"
        static let OrderDetailFormat = "MMM d yyyy"
        static let TransportDate = "MMM d yyyy, hh:mm a"
        static let TimeFormat = "hh:mm a"
        static let OrderReceiptFormat = "hh:mm a, MMM dd yyyy"
        static let ItemDetailFormat = "MMM d, yyyy"
        static let TimeFormatHH = "HH:mm"
        static let HHmmss = "HH:mm:ss"
        static let UpcommingOrder = "EEE, MMM d"
        static let CSVDate = "MMM d yyyy hh:mm a"
        static let CSVFileDate = "yyyyMMdd_HHMMSS"
        static let EarningReportReqDate = "yyyy-MM-dd HH:mm:ss"
    }
    
//    class func stringToDate(string:String) -> Date?
//    {
//        return nil
//    }
//
//    class func dateToString() -> String?
//    {
//
//        return nil
//    }
    
    class func date(from dateString: String, format: String, isCurrentTimeZone: Bool) -> Date? {
        let dateFormat = DateFormatter()
        dateFormat.setLocale()
        dateFormat.dateFormat = format
        if isCurrentTimeZone {
            
            dateFormat.timeZone = .current
        }else{
           
            dateFormat.timeZone = TimeZone(abbreviation: "UTC")
        }
        let date: Date? = dateFormat.date(from: dateString)
        return date
    }
    
    class func string(from date: Date, format: String, isCurrentTimeZone: Bool) -> String {
        let formatter = DateFormatter()
        formatter.setLocale()
        if isCurrentTimeZone {
            
            formatter.timeZone = NSTimeZone.local
        }else{
           
            formatter.timeZone = TimeZone(abbreviation: "UTC")
        }
        formatter.dateFormat = format
        let dateString: String = formatter.string(from: date)
        return dateString
    }
    
    class func stringfromDate(from_date date: Date?, formate: String, isCurrentTimeZone:Bool) -> String? {

        if let dt = date{

            let formatter = DateFormatter()
            formatter.setLocale()
            formatter.dateFormat = formate
            if isCurrentTimeZone {
                formatter.timeZone = TimeZone.current
            }else{
                formatter.timeZone = TimeZone(abbreviation: "UTC")
            }
            return formatter.string(from: dt)
        }

        return nil
    }
    
    class func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat

        return dateFormatter.string(from: dt!)
    }
}
//    class func stringUTC(from date: Date, format: String) -> String {
//        let formatter = DateFormatter()
//        formatter.setLocale()
//        formatter.timeZone = TimeZone(abbreviation: "UTC")
//        formatter.dateFormat = format
//        let dateString: String = formatter.string(from: date)
//        return dateString
//    }
    
//    class func datefromUTC(from dateString: String, format: String, isCurrentTimeZone locationtime: Bool) -> Date {
//        let dateFormat = DateFormatter()
//        dateFormat.setLocale()
//        dateFormat.dateFormat = format
//        dateFormat.timeZone = TimeZone(abbreviation: "UTC")
//        let date: Date? = dateFormat.date(from: dateString)
//        return date ?? Date()
//    }
//
//
//    class func stringFromtimeconfirmLag(for totalSeconds: Int) -> String {
//        let seconds: Int = totalSeconds % 60
//        let minutes: Int = (totalSeconds / 60) % 60
//        let hours: Int = totalSeconds / 3600
//        return String (format:"%dh%dm%ds",hours,minutes,seconds)
//    }
//
//    class func convertDateFormater(_ date: String) -> String
//    {
//        let dateFormatter = DateFormatter()
//
//        dateFormatter.setLocale()
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.calendar = NSCalendar.current
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//
//        let date = dateFormatter.date(from: date)
//
//        dateFormatter.dateFormat = "MMMM d yyyy, h:mm a"
//        dateFormatter.timeZone = TimeZone.current
//        return  dateFormatter.string(from: date!)
//
//    }
//
//    class func stringfromLocationTime(from strDate: String, format: String, timezone:String) -> String {
//        let formatter = DateFormatter()
//        formatter.setLocale()
//        formatter.timeZone = NSTimeZone(name:timezone)! as TimeZone
//        formatter.dateFormat = format
//        let dateLocation: Date = formatter.date(from: strDate)!
//        formatter.timeZone = TimeZone(abbreviation: "UTC")
//        formatter.dateFormat = format
//        let dateString: String = formatter.string(from: dateLocation)
//        return dateString
//    }
//
//    class func getTime(_ hourOfOperation: String, formatter dateFormat: String) -> String {
//        let dfoperationhours = DateFormatter()
//        dfoperationhours.setLocale()
//        dfoperationhours.dateStyle = .medium
//        dfoperationhours.dateFormat = "HH:mm:ss"
//        let date1: Date? = dfoperationhours.date(from: hourOfOperation)
//        let df24horformatter = DateFormatter()
//        df24horformatter.setLocale()
//        df24horformatter.dateFormat = dateFormat
//        return df24horformatter.string(from: date1!)
//    }
//
//    class func getDateFrom(_ strDate: String) -> Date {
//        let dateFormatter = DateFormatter()
//        dateFormatter.setLocale()
//        dateFormatter.dateFormat = "dd/MM/yyyy"
//        let yourDate: Date = dateFormatter.date(from: strDate)!
//        return yourDate
//    }
    
//    class func timeAgoSince(_ date: Date) -> String {
//
//        let calendar = Calendar.current
//        let now = Date()
//        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
//        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
//
//        if let year = components.year, year >= 2 {
//            return "\(year) years ago"
//        }
//
//        if let year = components.year, year >= 1 {
//            return "Last year"
//        }
//
//        if let month = components.month, month >= 2 {
//            return "\(month) months ago"
//        }
//
//        if let month = components.month, month >= 1 {
//            return "Last month"
//        }
//
//        if let week = components.weekOfYear, week >= 2 {
//            return "\(week) weeks ago"
//        }
//
//        if let week = components.weekOfYear, week >= 1 {
//            return "Last week"
//        }
//
//        if let day = components.day, day >= 2 {
//            return "\(day) days ago"
//        }
//
//        if let day = components.day, day >= 1 {
//            return "Yesterday"
//        }
//
//        if let hour = components.hour, hour >= 2 {
//            return "\(hour) hours ago"
//        }
//
//        if let hour = components.hour, hour >= 1 {
//            return "An hour ago"
//        }
//
//        if let minute = components.minute, minute >= 2 {
//            return "\(minute) minutes ago"
//        }
//
//        if let minute = components.minute, minute >= 1 {
//            return "A minute ago"
//        }
//
//        if let second = components.second, second >= 3 {
//            return "\(second) seconds ago"
//        }
//
//        return "Just now"
//
//    }
//
////    class func date(fromLocationString dateString: String, format: String, isCurrentTimeZone locationtime: String) -> String {
////        let dateFormatter = DateFormatter()
////        dateFormatter.setLocale()
////        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
////        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
////        let yourDate: Date? = dateFormatter.date(from: dateString)
////        let sourceDate: Date? = yourDate
////        let sourceTimeZone = TimeZone(abbreviation: "UTC")
////        let destinationTimeZone = NSTimeZone(name: locationtime)
////        let sourceGMTOffset: Int? = sourceTimeZone?.secondsFromGMT(for: sourceDate!)
////        let destinationGMTOffset: Int? = destinationTimeZone?.secondsFromGMT(for: sourceDate!)
////        let interval = Double (destinationGMTOffset! - sourceGMTOffset!)
////        let destinationDate = Date(timeInterval: interval, since: sourceDate!)
////        dateFormatter.dateFormat = format
////        let orderDateString: String = dateFormatter.string(from: destinationDate)
////        return orderDateString
////    }
////
////
////    class func SetTimeAsServer(_ hourOfOperation: String) -> String {
////        let dfoperationhours = DateFormatter()
////        dfoperationhours.setLocale()
////        dfoperationhours.dateStyle = .medium
////        dfoperationhours.dateFormat = "h:mm a"
////        let date1: Date? = dfoperationhours.date(from: hourOfOperation)
////        let df24horformatter = DateFormatter()
////        df24horformatter.dateFormat = "HH:mm:ss"
////        return df24horformatter.string(from: date1!)
////    }
////    class func DateinStringFromDate(_ date:Date) -> String{
////        let formatter = DateFormatter()
////        formatter.setLocale()
////        formatter.dateFormat = "MMM d, yyyy"
////        return formatter.string(from: date)
////    }
////
////    class func DateFromString(_ strdate:String) -> Date{
////        let formatter = DateFormatter()
////        formatter.setLocale()
////        formatter.dateFormat = "MMM d, yyyy"
////        return formatter.date(from: strdate)!
////    }
////
////    class func offsetFrom(date : Date,currantDate: Date) -> NSMutableAttributedString {
////
////
////        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
////
////        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to:currantDate);
////
////        let attributes = [NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "B72537"),NSAttributedString.Key.font:UIFont(name: "SFCompactDisplay-Regular", size: 15.0)!]
////
////        let minutes = NSMutableAttributedString(string:String(format:"%d" + " ",difference.minute!), attributes:attributes)
////        let minutes2 = NSMutableAttributedString(string:"MINS" + " ")
////        minutes.append(minutes2)
////
////        let seconds = NSMutableAttributedString(string:String(format:"%d" + " ",difference.second!), attributes:attributes)
////        let seconds2 = NSMutableAttributedString(string:"SECS" + " ")
////        seconds.append(seconds2)
////
////
////
////        let hours = NSMutableAttributedString(string:String(format:"%d" + " ",difference.hour!), attributes:attributes)
////        let hours2 = NSMutableAttributedString(string:"HRS" + " ")
////        hours.append(hours2)
////        hours.append(minutes)
////
////        if let hour = difference.hour, hour       > 0 { return hours}
////        if let minute = difference.minute, minute > 0 { return minutes}
////        if let second = difference.second, second > 0 { return seconds}
////
////        return NSMutableAttributedString(string:"")
////    }
////
////    class func daysBetweenDates(startDate: Date, endDate: Date) -> Int
////    {
////        let calendar = NSCalendar.current
////        let date1 = calendar.startOfDay(for: startDate)
////        let date2 = calendar.startOfDay(for: endDate)
////        return calendar.dateComponents([.day], from: date1, to: date2).day!
////    }
////
////
////    class func getTimeString(_ time: String) -> NSMutableAttributedString {
////
////        let strTime: NSMutableAttributedString = NSMutableAttributedString(string:"")
////        let arrTime: [String] = time.components(separatedBy: ":")
////        let Hourtime = Int(arrTime[0])!
////        let MinTime = Int(arrTime[1])!
////        let SecTime = Int(arrTime[2])!
////
////
////        let fontAttributes1 = [NSAttributedString.Key.font:UIFont(name: "SFCompactDisplay-Regular", size: 28.0)!]
////
////        let fontAttributes2 = [NSAttributedString.Key.font:UIFont(name: "SFCompactDisplay-Regular", size: 16.0)!]
////
////        if Hourtime > 0 {
////
////            let hour = NSMutableAttributedString(string:String(format:"%d" + " ",Hourtime), attributes:fontAttributes1)
////            let hour1 = NSMutableAttributedString(string:Hourtime <= 1 ? String(format:"HR") : String(format:"HRS"), attributes:fontAttributes2)
////            hour.append(hour1)
////            strTime.append(hour)
////        }
////
////        if MinTime > 0 {
////
////            let min = NSMutableAttributedString(string:String(format:" " + "%d" + " ",MinTime), attributes:fontAttributes1)
////            let min1 = NSMutableAttributedString(string:MinTime <= 1 ? String(format:"MIN") : String(format:"MINS"), attributes:fontAttributes2)
////            min.append(min1)
////            strTime.append(min)
////        }
////        if SecTime > 0 {
////
////            let sec = NSMutableAttributedString(string:String(format:" " + "%d" + " ",SecTime), attributes:fontAttributes1)
////            let sec1 = NSMutableAttributedString(string:SecTime <= 1 ? String(format:"SEC") : String(format:"SECS"), attributes:fontAttributes2)
////            sec.append(sec1)
////            strTime.append(sec)
////        }
////        if Hourtime == 0 && MinTime == 0 && SecTime == 0 {
////
////            let sec = NSMutableAttributedString(string:String(format:"0" + " "), attributes:fontAttributes1)
////            let sec1 = NSMutableAttributedString(string:String(format:"SEC"), attributes:fontAttributes2)
////            sec.append(sec1)
////            strTime.append(sec)
////        }
////        return strTime
////    }
//
//    class func intToHoursMinutes (minutes : Int) -> (Int, Int) {
//        return (minutes / 60, (minutes % 60))
//    }
//
//    class func HoursMinutesToMinuteInt(hour : Int, minute : Int) -> Int {
//        return (hour * 60) + minute
//    }
//
//    class func intToMinutesSeconds (seconds : Int) -> (Int, Int) {
//        return (seconds / 60, (seconds % 60))
//    }
//
//    class func MinutesSecondsToSecondInt(Mins : Int, seconds : Int) -> Int {
//        return (Mins * 60) + seconds
//    }
//}
//
//MARK: New Functions
extension DateFunctions{

    class func getCurrentDateInDashboardFormat() -> String{
        let formatter = DateFormatter()
        formatter.setLocale()
        formatter.dateFormat = "EEEE\nMMM d, yyyy"
        return formatter.string(from: Date())
    }
}


//extension Date {
//    static var yesterday: Date { return Date().dayBefore }
//    static var tomorrow:  Date { return Date().dayAfter }
//
//    var dayBefore: Date {
//        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
//    }
//    var dayAfter: Date {
//        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
//    }
//    var noon: Date {
//        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
//    }
//    var month: Int {
//        return Calendar.current.component(.month,  from: self)
//    }
//    var isLastDayOfMonth: Bool {
//        return dayAfter.month != month
//    }
//}

//
extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}
//
//public extension Date {
//    // MARK: Components
//    private static func componentFlags() -> Set<Calendar.Component> { return [Calendar.Component.year, Calendar.Component.month, Calendar.Component.day, Calendar.Component.weekOfYear, Calendar.Component.hour, Calendar.Component.minute, Calendar.Component.second, Calendar.Component.weekday, Calendar.Component.weekdayOrdinal, Calendar.Component.weekOfYear] }
//
//    private static func components(_ fromDate: Date) -> DateComponents! {
//        return Calendar.current.dateComponents(Date.componentFlags(), from: fromDate)
//    }
//
//    private func components() -> DateComponents  {
//        return Date.components(self)!
//    }
//
//    func isEqualToDateIgnoringTime(_ date: Date) -> Bool
//    {
//        let comp1 = Date.components(self)!
//        let comp2 = Date.components(date)!
//
//        return ((comp1.year == comp2.year) && (comp1.month == comp2.month) && (comp1.day == comp2.day))
//    }
//
//    /**
//     Returns Returns true if date is today.
//     */
//    func isToday() -> Bool
//    {
//        return self.isEqualToDateIgnoringTime(Date())
//    }
//
//    /**
//     Returns true if date is tomorrow.
//     */
//    func isTomorrow() -> Bool
//    {
//        return self.isEqualToDateIgnoringTime(self.dateByAddingDays(1))
//    }
//
//    /**
//     Returns true if date is yesterday.
//     */
//    func isYesterday() -> Bool
//    {
//        return self.isEqualToDateIgnoringTime(Date().dateBySubtractingDays(1))
//    }
//
//    /**
//     Creates a new date by a adding days.
//
//     - Parameter days: The number of days to add.
//     - Returns A new date object.
//     */
//    func dateByAddingDays(_ days: Int) -> Date
//    {
//        var dateComp = DateComponents()
//        dateComp.day = days
//        return Calendar.current.date(byAdding: dateComp, to: self)!
//    }
//
//    /**
//     Creates a new date by a substracting days.
//
//     - Parameter days: The number of days to substract.
//     - Returns A new date object.
//     */
//    func dateBySubtractingDays(_ days: Int) -> Date
//    {
//        var dateComp = DateComponents()
//        dateComp.day = (days * -1)
//        return Calendar.current.date(byAdding: dateComp, to: self)!
//    }
//
//}
//
//extension Date {
//
//    // Convert local time to UTC (or GMT)
//    func toGlobalTime() -> Date {
//        let timezone = TimeZone.current
//        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
//        return Date(timeInterval: seconds, since: self)
//    }
//
//    // Convert UTC (or GMT) to local time
//    func toLocalTime() -> Date {
//        let timezone = TimeZone.current
//        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
//        return Date(timeInterval: seconds, since: self)
//    }
//
//}

extension String {
    func ISOToLocalDateStr(outputFormat:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFunctions.DateFormat.ISOFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//        var formattedString = self.replacingOccurrences(of: "Z", with: "")
//        if let lowerBound = formattedString.range(of: ".")?.lowerBound {
//            formattedString = "\(formattedString[..<lowerBound])"
//        }

        guard let date = dateFormatter.date(from: self) else {
            return self
        }

        dateFormatter.dateFormat = outputFormat
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: date)
    }
}
