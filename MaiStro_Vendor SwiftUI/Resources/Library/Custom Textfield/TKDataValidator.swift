//
//  TKDataValidator.swift
//  TKFormTextFieldDemo
//
//  Created by Thongchai Kolyutsakul on 1/1/17.
//  Copyright © 2017 Viki. All rights reserved.
//

import Foundation

// Stores functions that validates input text.
// Each function takes a text.
// Returns an error string if that text is invalid, or nil if valid.
class TKDataValidator {
    
    class func isEmpty(text: String?,msg:String) -> String? {
        
        guard let text = text?.trim(), text.count > 0 else {
            return msg
        }
        
        return nil
    }
    
    class func email(text: String?) -> String? {
        guard let text = text?.trim(), !text.isEmpty else {
            return "Please enter an email address"
        }
        guard text.tk_isValidEmail() else {
            return "Please enter a valid email address"
        }
        return nil
    }
    
    class func newAndOldPassword(old: String?, new : String?) -> String? {
        if let oldPass = old, let newPass = new {
            if oldPass == newPass {
                return "Old and new password must be different"
            }
        }
        return nil
    }
    
    class func oldPassword(text: String?) -> String? {
        guard let text = text?.trim(), !text.isEmpty else {
            return "Please enter an old password"
        }
        if !text.isValidPassword {
            return PASSWORD_FORMATE_MESSAGE
        }
        return nil
    }
    
    class func newPassword(text: String?,textOldPswd:String?) -> String? {
        guard let text = text?.trim(), !text.isEmpty else {
            return "Please enter a new password"
        }
        if !text.isValidPassword {
            return PASSWORD_FORMATE_MESSAGE
        }
        
        if text == textOldPswd
        {
            return "New password should not be same as old password."
        }
        
        return nil
    }
    
    class func password(text: String?) -> String? {
        guard let text = text?.trim(), text.count > 0 else {
            return "Please enter a password"
        }
        
        if !text.isValidPassword {
            return PASSWORD_FORMATE_MESSAGE
        }
        return nil
    }
    
    class func confirmPassword(password: String?, confirmPassword : String?) -> String? {
        
        guard let text = confirmPassword, text.count > 0 else {
            return "Please enter a confirm password"
        }
        
        if password != confirmPassword {
            return "Password and Confirm password not matched"
        }
        return nil
    }
    
    class func oldnewPassword(oldPassword: String?, newPassword : String?) -> String? {
        
        guard let text = newPassword, text.count > 0 else {
            return "Please enter a new password"
        }
        
        if oldPassword == newPassword {
            return "new password should not be same as old password"
        }
        return nil
    }
    
    class func firstName(text: String?) -> String? {
        guard let text = text?.trim(), !text.isEmpty else {
            return "Please enter a first name"
        }
        return nil
    }
    
    class func lastName(text: String?) -> String? {
        guard let text = text?.trim(), !text.isEmpty else {
            return "Please enter a last name"
        }
        return nil
    }
    
    class func userName(text: String?) -> String? {
        guard let text = text?.trim(), !text.isEmpty else {
            return "Please enter a user name"
        }
        return nil
    }
    
    class func phoneNumber(text: String?, isMedantory : Bool = false) -> String? {
        
        if isMedantory{
            
            guard let text = text?.trim(), text.count > 0 else {
                return "Please enter a phone number"
            }
        }
        
        if !text!.isValidPhone {
            return "Please enter a valid phone number"
        }
        
        return nil
    }
    class func loginPassword(text: String?) -> String? {
        
        guard let text = text?.trim(), text.count > 0 else {
            return "Please enter a password"
        }
        
        return nil
    }
    
    //MARK: Business Profile
    class func businessName(text: String?) -> String? {
        
        guard let text = text?.trim(), text.count > 0 else {
            return "Please enter a business name"
        }
        
        return nil
    }
    
    class func licenseNumber(text: String?) -> String? {
        
        guard let text = text?.trim(), text.count > 0 else {
            return "Please enter a business license number"
        }
        
        if text.count < 5{
            
            return "Please enter a valid business license number"
        }
        
        return nil
    }
    
    class func address(text: String?) -> String? {
        
        guard let text = text?.trim(), text.count > 0 else {
            return "Please enter a street address"
        }
        
        return nil
    }
    
    class func selectCity(text: String?) -> String? {
        
        guard let text = text?.trim(), text.count > 0 else {
            return "Please enter a city"
        }
        
        return nil
    }
    
    class func selectProvince(text: String?) -> String? {
        
        guard let text = text?.trim(), text.count > 0 else {
            return "Please enter a province"
        }
        
        return nil
    }
    
    class func enterPostalCode(text: String?) -> String? {
        
        guard let text = text?.trim(), text.count > 0 else {
            return "Please enter a postal code"
        }
        
        return nil
    }
    
    class func enterTax(text: String?) -> String? {
        
        guard let text = text?.trim(), text.count > 0 else {
            return "Please enter a tax"
        }
        
        if Double(text) == nil{
            
            return "Please enter a valid tax value"
        }
        
        return nil
    }
}

extension String {
    func tk_isValidEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: self)
    }
    
    public var isValidPassword : Bool
    {
        //let passwordRegEx: String = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
        
        //Validation for one capital, one number , one small and length of 8 character
        //let passwordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d !\"\\\\#$%&'\\(\\)\\*+,\\-\\./:;<=>?@\\[\\]^_`\\{|\\}~]{8,}"
        
        //one capital one special one digit and 8 char long
        let passwordRegEx = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9]).{8,}$"
        let regExPredicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return regExPredicate.evaluate(with: self)
    }
    
    public var isValidPhone : Bool{
        
        let phoneRegex = "^([0-9]).{9}$"
        let regExPredicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return regExPredicate.evaluate(with: self)
    }
    
//    public var isValidLicenseNumber : Bool{
//
//        let license = "^([0-9a-zA-Z]).{5,}$"
//        let regExPredicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", license)
//        return regExPredicate.evaluate(with: self)
//    }
}
