//
//  CalendarViewTheme.swift
//  CalendarView
//
//  Created by Wito Chandra on 06/04/16.
//  Copyright © 2016 Wito Chandra. All rights reserved.
//

import UIKit

open class CalendarViewTheme {

    public init() {
    }
    
    open var bgColorForMonthContainer = UIColor.white
    open var bgColorForDaysOfWeekContainer = UIColor(hexString: "FBFBFB")
    open var bgColorForCurrentMonth = UIColor.white
    open var bgColorForOtherMonth = UIColor.white
    open var colorForDivider = UIColor.clear
    open var colorForSelectedDate = ColorConstant.ThemeColor
    open var colorForDatesRange = ColorConstant.ThemeColor
    open var textColorForTitle = ColorConstant.TextMainColor
    open var textColorForDayOfWeek = UIColor(hexString: "4A4A4A")
    open var textColorForNormalDay = ColorConstant.TextMainColor
    open var textColorForDisabledDay = UIColor(hexString: "D4D4D4")
    open var textColorForSelectedDay = UIColor.white
    open var textColorForRangeDay = UIColor.white
}

extension UIColor {
    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        let red = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((hex & 0xFF00) >> 8) / 255.0
        let blue = CGFloat((hex & 0xFF)) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
