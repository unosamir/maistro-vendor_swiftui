//
//  SplashView.swift
//  MaiStro_Vendor SwiftUI
//
//  Created by Sam on 23/11/20.
//

import SwiftUI

struct SplashView: View {
    
    var body: some View {
        NavigationView {
            Color.init(hex: "2C9FD0").edgesIgnoringSafeArea(.all)
                .overlay(
                    VStack(alignment: .center, spacing: 0, content: {
                        
                        Text("MAISTRO").font(.custom(FontConstant.kProDisplayBoldFont, size: 44)).foregroundColor(.white)
                        Image.init("VENDOR")
                        NavigationLink(destination: SelectSignupView()) {
                            //Text("Show Detail View")
                        }
                    })
                )
        }.navigationBarHidden(true)
        //        .onAppear(perform: initialConfig)
        
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
