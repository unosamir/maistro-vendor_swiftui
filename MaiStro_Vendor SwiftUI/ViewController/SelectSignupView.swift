//
//  LandingView.swift
//  MaiStro_Vendor SwiftUI
//
//  Created by Sam on 23/11/20.
//

import SwiftUI

struct SelectSignupView: View {
    
    var body: some View {
        NavigationView {
            Color.init(hex: "2C9FD0").edgesIgnoringSafeArea(.all)
                .overlay(
                VStack(alignment: .center, spacing: 0, content: {
                        
                        Image.init("VENDOR")
                    })
                )
        }.navigationBarHidden(true)
        
    }
}

struct SelectSignupView_Previews: PreviewProvider {
    static var previews: some View {
        SelectSignupView()
    }
}
